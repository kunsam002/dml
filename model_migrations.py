__author__ = 'kunsam002'
"""
@Author: Olukunle Ogunmokun
"""

import os
import json



def populate_contents():
	from dml.core import udacity, coursera, rss_feeder
	from dml import db, models, logger
	from dml.models import Content

	udacity.populate_courses()
	coursera.populate_courses()
	rss_feeder.fetch_contents()
