#! /usr/bin/env python
import flask
from flask_script import Manager
import os
from flask_assets import ManageAssets
from flask_script.cli import prompt, prompt_pass, prompt_bool

from factories import create_app, initialize_api, initialize_blueprints
from flask_migrate import MigrateCommand
from model_migrations import *
from xlrd import open_workbook
from multiprocessing import Process

app = create_app('dml', 'config.SiteDevConfig')

logger = app.logger

# Initializing script manager
manager = Manager(app)
# add assets command to it
manager.add_command("assets", ManageAssets(app.assets))

manager.add_command('db', MigrateCommand)

SETUP_DIR = app.config.get("SETUP_DIR")


@manager.command
def runserver():
    """ Start the server"""
    # with app2.app_context():
    # from dml.views.public import www
    from dml import api, principal
    from dml.resources import assets, accounts, operations
    from dml.subscribers import accounts, operations

    # Initialize the app blueprints
    # initialize_blueprints(app, www)
    initialize_api(app, api)

    port = int(os.environ.get('PORT', 5500))
    app.run(host='0.0.0.0', port=port)


@manager.command
def runadmin():
    """ Start the backend server server"""
    # with app2.app_context():
    # from dml.views.admin import control
    from dml import api, principal
    from dml.resources.admin import admin
    from dml.resources import assets, accounts, operations

    # Initialize the app blueprints
    # initialize_blueprints(app, control)
    initialize_api(app, api)

    port = int(os.environ.get('PORT', 5001))
    app.run(host='0.0.0.0', port=port)


@manager.command
def rununi():
    """ Start the server"""
    # with app2.app_context():
    from dml import api, principal
    # from dml.resources.universities import university

    # Initialize the app blueprints
    initialize_api(app, api)

    port = int(os.environ.get('PORT', 5600))
    app.run(host='0.0.0.0', port=port)


@manager.command
def syncdb(refresh=False):
    """
    Synchronizes (or initializes) the database
    :param refresh: drop and recreate the database
    """
    # Apparently, we need to import the models file before this can work right.. smh @flask-sqlalchemy
    from dml import db, models
    if refresh:
        logger.info("Dropping database tables")
        db.drop_all()
    logger.info("Creating database tables")
    db.create_all()
    db.session.flush()


@manager.command
def refresh_db():
    syncdb(refresh=True)

@manager.command
def create_admin():
    """ Admin setup to create """
    app = flask.current_app
    with app.app_context():

        from dml.services.accounts import UserService

        print "Enter the following parameters to created a new user account"

        username = prompt("Username(*)")
        email = prompt("Email(*)")
        password = prompt_pass("Password(*)")
        retype_password = prompt_pass("Retype Password(*)")
        first_name = prompt("First Name")
        last_name = prompt("Last Name")

        if username and email and password and retype_password and (password == retype_password):
            user = UserService.create(include_address=False, username=username, email=email, first_name=first_name,
                                      last_name=last_name, is_verified=True, is_staff=True,
                                      password=password, is_mtn=True, active=True, is_super_admin=True)

            if user:
                return user.id

        else:
            print "Account could not be created. Please supply all required (*) parameters"


@manager.command
def install_assets():
    """ Installs all required assets to begin with """
    from startup import start
    start()


@manager.command
def load_contents():
    from model_migrations import populate_contents

    populate_contents()

@manager.command
def setup_app():
    syncdb()
    install_assets()
    load_contents()


def runInParallel(*fns):
    proc = []
    for fn in fns:
        p = Process(target=fn)
        p.start()
        proc.append(p)
    for p in proc:
        p.join()


@manager.command
def start_apps():
    runInParallel(runserver)


@manager.command
def db_upgrade():
    coms = ["upgrade", "migrate", "upgrade"]
    for i in coms:
        manager.add_command('db', i)


if __name__ == "__main__":
    manager.run()
