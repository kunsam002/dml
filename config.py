# Configuration module

import os
# import Crypto.PublicKey.RSA as RSA


class Config(object):
    """
	Base configuration class. Subclasses should include configurations for
	testing, development and production environments

	"""

    DEBUG = True
    SECRET_KEY = '\x91c~\xc0-\xe3\'f\xe19PE\x93\xe8\x91`uu"\xd0\xb6\x01/\x0c\xed\\\xbd]H\x99k\xf8'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_POOL_RECYCLE = 1 * 60 * 60

    ADMIN_EMAILS = ["kunsam002@gmail.com"]

    EMAIL_DEV_ONLY = True
    DEV_MODE = True

    # File uploads
    UPLOADS_DEFAULT_DEST = os.path.join(os.path.dirname(os.path.abspath(__name__)), "uploads")

    LOGS_DEFAULT_DEST = os.path.join(os.path.dirname(os.path.abspath(__name__)), "logs")

    FLASK_ASSETS_USE_S3 = False
    USE_S3 = False
    USE_S3_DEBUG = DEBUG
    ASSETS_DEBUG = True
    S3_USE_HTTPS = False

    LOGFILE_NAME = "dml"

    ADMIN_USERNAME = "dml"
    ADMIN_PASSWORD = "dml"
    ADMIN_EMAIL = "admin@dml.com"
    ADMIN_FULL_NAME = "dml"

    # Flask Mail
    MAIL_SERVER = "smtp.gmail.com"
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = "mtnfdml@gmail.com"
    MAIL_PASSWORD = "androidios"

    CLOUDINARY_CONFIG = {
        'cloud_name': 'dfakx3h87',
        'api_key': '836291492579271',
        'api_secret': 'xO681IPTlTxiO70JJTTjN7TbFm4',
    }

    CACHE_TYPE = 'simple'

    # Facebook, Twitter and Google Plus handles
    SOCIAL_LINKS = {"facebook": "", "twitter": "", "google": "",
                    "instagram": "", "pinterest": ""}

    # ReCaptcha Keys
    RECAPTCHA_PUB_KEY = "6LeC-OgSAAAAAOjhuihbl6ks-NxZ9jzcv7X4kG9M"
    RECAPTCHA_PRIV_KEY = "6LeC-OgSAAAAANbUdjXj_YTCHbocDQ48-bRRFYTr"

    PROTOCOL = "http://"
    DOMAIN = "dml:5500"
    API_PATH = "/v1/api"

    CANONICAL_URL = "%s%s" % (PROTOCOL, DOMAIN)
    CANONICAL_API_URL = "%s%s%s" % (PROTOCOL, DOMAIN, API_PATH)

    ACCEPTABLE_EMAIL_EXTENSION = ["softcom.ng", "mtnnigeria.net", "etranzact.com", "atele.org", "yahoo.com"]

    FIREBASE_PROJECT_ID = "mtnf-demochat"
    FIREBASE_PUSH_NOTI_URL = "https://fcm.googleapis.com/fcm/send"
    FIREBASE_DATABASE_URI = "https://mtnf-demochat.firebaseio.com/"
    FIREBASE_AUTH_HEADER_KEY = "key=AIzaSyBYe9OUgszZ4hNHL0g4EOcPnO6xuL6HIS0"
    FIREBASE_SERVICE_ACCOUNT_EMAIL = "mtn-nodejs-service-account@mtnf-demochat.iam.gserviceaccount.com"
    FIREBASE_SERVICE_ACCOUNT_FILE_PATH = os.path.join(os.path.dirname(os.path.abspath(__name__)), "serviceAccountKey.json")
    FIREBASE_PRIVATE_KEY = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCl+PzAFOZo2i9T\n44rn4tH9/4qAXqEhBq8ZY17v94MahFvB/COHw98/Ad9jKHvNccdKY6d3CeLjnWiz\nGWVLwbFnYJ/nk/c/a2kvd2nKg9fgvjIvPKAaFr1Wvsrp+Ek4zC2AJ31UC5EKUVIk\nR7QQJjaYcz8mr8p+N9usnJF8T5n/Qd/NykXIW9ut0a22dj1A3U+ZQrGBPNK0p7cZ\n1yF5sqSezh8YOxm8XDPtylMVXQezzk5dCTfB6kIkZBAXUaqLqI8UUdQkbHQYfALT\nC8noRqfVHBqdRgo66fZ+/YzO28dmvI821bEImYWQ2YzJ4ejubTKsVCMe5ihnPXrH\nmA5F+CgfAgMBAAECggEAG0KF4NaN8XLEiza4PdXXP6yVBviD5nS+laDomulAMz7C\nA15yR344UJTyJ8sial/hp6ouezK4fUIBTElDhenORXhtSw/WcCgoIo+uZIC7GEKk\n43VFpoNNnc3AbiVFgXqnjoDt6LvtmpA4uZS9Ulm0Zd2n5abBz3FFN1iP3KcGWBIp\n+sfZjYZl97yvjMyC4Y+QMDmhM2bgzCXIWd+KJSpmROXKRr0AI+ylvd0vQVqrv3/z\nGHmDo3QmLyOt19tfLgSDDnb5lbk+MhaR4af8Im+fjbawbKhKxgVf3qPbzpKHKYcI\ne1zgG5weblNkPjTCZSjxAcAyB85fRqnotKmub/KUQQKBgQDVqts66NlmSYgDVxSX\nGkCWS74z0SUIWbLWUB3+LQPvzD/QPolbA4mqkpypxc4uioGl1GV6ssYvREpINC+Z\nzwhQK+kir9HV5jodMKXB48oEgV9gKJ8W+ro7LH28YrhLPzCCpGNQeX6YhA+gLlFC\nf7JWuf+y1CjsWTcib4P6bpd4yQKBgQDG2xDObn55nzTZPMCAz1Fu0c2Es/3Qj+I4\naaR+a01OAGh3FB9dGDiSz0PM7MPn2ur9wwk92EE9guwYGDASmz3y6/nEp9AVgfWy\ncrFtp+3kjkj/QGhd7wkQLfmetKsybQlKQXUNsAPzXZuIrmCYHfqLyBALQwYgodTk\nONGBrhD1pwKBgD7891QOgMhetPIHIdLJsnCcM54i8b/ccZQsWissKhTvKYDCbsud\n51+ZiQWjgnzF6CJy0texDCuy/hL7D21QUysPRtMq8bkU/dzNEpBuDof1e6BlWUD0\n+SGopAh9Mcc9AZgBziVTK7bad6DJBruL2Y8nlfGqCcZjJpXIOigBojXBAoGATLMm\nUlkwEz0jJz9ViNluMW5e9PZvycgVm2iz98bvdEyX/oYWatPyzj1/i0Uvcvhvdx2J\nLP+CenVLFygNMsAKLu5x0jZf/ku4NvIiGYeSLHvtD/cJymFuf30cMOjCt5py4N9K\nhNMFUxMh8KBBxaqaftvVxGI/tMp20OhRdtFq3TUCgYBsSYXLbEhYSV7gw5+XrPp5\nehyN28grifrlcJ6BhhW2upTI2qhZFjfo62fcvFaG8gGP2O3S7sOu3SBYX79J0IhP\ntn4bgHKlr3E6TdrAVTH8pme5i94DfnSY7bEDXFOtCFa5Ug3bu1JZSKswU113HICV\nkmvqo5m7tBtJZRhTUsM0Qg==\n-----END PRIVATE KEY-----\n"



class SiteDevConfig(Config):
    """ Configuration class for site development environment """

    DEBUG = True

    SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://dml:dml@localhost/dml'

    DATABASE = SQLALCHEMY_DATABASE_URI
    SETUP_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), 'setup')
    MAX_RETRY_COUNT = 3


    LOGIN_VIEW = '.login'


class TestProdConfig(SiteDevConfig):
    DEV_MODE = False

    PROTOCOL = "http://"
    DOMAIN = "mtnfdml-api.herokuapp.com"
    API_PATH = "/v1/api"

    SQLALCHEMY_DATABASE_URI = "postgres://ujsnogaegrvmfo:eCAdOqH0qebrusGFH1769-1LQ7@ec2-54-75-228-86.eu-west-1.compute.amazonaws.com/daut6njc7h48e9"
