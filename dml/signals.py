__author__ = 'kunsam002'

"""
signals.py

All signals required within the application.
These signals will be used to interconnect different parts of the application.
It will also be used by 3rd party applications as a connection mechanism
"""

from blinker import Namespace

app_signals = Namespace()

# Send this signal when a student account is created
student_created = app_signals.signal('student_created')
auto_signup_to_verify = app_signals.signal('auto_signup_to_verify')
# Send this signal when a student account is updated
student_updated = app_signals.signal('student_updated')

# Send this signal when a university staff account is created
uni_staff_created = app_signals.signal('uni_staff_created')

# Send this signal when a university staff account is updated
uni_staff_updated = app_signals.signal('uni_staff_updated')

# Send this signal when a mtn staff account is created
mtn_staff_created = app_signals.signal('mtn_staff_created')

# Send this signal when a mtn staff account is updated
mtn_staff_updated = app_signals.signal('mtn_staff_updated')

# Send this signal when a content is liked
content_liked = app_signals.signal('content_liked')

# Send this signal when a content is unliked
content_unliked = app_signals.signal('content_unliked')

# Send this signal when a content is shared
content_shared = app_signals.signal('content_shared')

# Send this signal when a content is saved
content_saved = app_signals.signal('content_saved')

# Send this signal when a content is unsaved
content_unsaved = app_signals.signal('content_unsaved')

# Send this signal when a content is commented on
content_commented = app_signals.signal('content_commented')

# Send this signal when a user collection is created
collection_created = app_signals.signal('collection_created')

# Send this signal when a user collection is updated
collection_updated = app_signals.signal('collection_updated')

# Send this signal when a user adds content to collection
content_added_to_collection = app_signals.signal('content_added_to_collection')

# Send this signal when a user is followed
user_followed = app_signals.signal('user_followed')

# Send this signal when a user updates password
password_updated = app_signals.signal('password_updated')

# Send this signal when a user activity has been created
activity_created = app_signals.signal('activity_created')
user_mention = app_signals.signal('user_mention')
