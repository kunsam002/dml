from sqlalchemy.sql.expression import select

__author__ = 'kunsam002'

# Models

from dml import db, bcrypt, app, logger
from dml.core.utils import slugify, id_generator
from flask_login import UserMixin
from sqlalchemy import or_
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method, Comparator
from sqlalchemy.inspection import inspect
from datetime import datetime, timedelta
import hashlib
from sqlalchemy import func, asc, desc
from sqlalchemy import inspect, UniqueConstraint, desc
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy.orm import dynamic
from flask import url_for
import inspect as pyinspect
from socket import gethostname, gethostbyname
import string
import random


# SQLAlchemy Continuum
# import sqlalchemy as sa
# from sqlalchemy_continuum.ext.flask import FlaskVersioningManager
# from sqlalchemy_continuum import make_versioned

# make_versioned(manager=FlaskVersioningManager())


def get_model_from_table_name(tablename):
    """ return the Model class for a given __tablename__ """

    _models = [args[1] for args in globals().items() if pyinspect.isclass(args[
        1]) and issubclass(args[1], db.Model)]

    for _m in _models:
        try:
            if _m.__tablename__ == tablename:
                return _m
        except Exception, e:
            logger.info(e)
            raise

    return None


def short_token_generator(size=6, chars=string.digits):
    """
    utility function to generate random identification numbers
    """
    code = ''.join(random.choice(chars) for x in range(size))
    return check_short_token_gen(code)


def check_short_token_gen(code):
    token = ConfirmationToken.query.filter(ConfirmationToken.code==code).first()
    if token:
        return short_token_generator()
    else:
        return code


def slugify_from_name(context):
    """
    An sqlalchemy processor that works with default and onupdate
    field parameters to automatically slugify the name parameters in the model
    """
    return slugify(context.current_parameters['name'])


def generate_token_code(context):
    return hashlib.md5("%s:%s:%s" % (context.current_parameters["shop_id"], context.current_parameters["email"], str(datetime.now()))).hexdigest()


def generate_user_token_code(context):
    return hashlib.md5("%s:%s:%s" % (context.current_parameters["user_id"], context.current_parameters["email"], str(datetime.now()))).hexdigest()


class AppMixin(object):
    """ Mixin class for general attributes and functions """

    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    # __versioned__ = {} # SQLAlchemy Continuum

    @declared_attr
    def is_deleted(cls):
        return db.Column(db.Boolean, default=False)

    @declared_attr
    def date_created(cls):
        return db.Column(db.DateTime, default=datetime.utcnow)

    @declared_attr
    def last_updated(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def as_full_dict(self):
        """ Retrieve all values of this model as a dictionary """
        data = inspect(self)

        return dict([(k, getattr(self, k)) for k in data.attrs.keys()])

    def as_dict(self, include_only=None, exclude=[], extras=[]):
        """ Retrieve all values of this model as a dictionary """

        data = inspect(self)
        if include_only is None:
            include_only = data.attrs.keys()

        return dict([(k, getattr(self, k)) for k in data.attrs.keys() + extras
                     if k in include_only + extras and isinstance(getattr(self, k), (db.Model, db.Query, InstrumentedList, dynamic.AppenderMixin)) is False
                     and k not in exclude])


    def as_dict_inner(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[]):
        """ Retrieve all values of this model as a dictionary """
        data = inspect(self)

        if include_only is None:
            include_only = data.attrs.keys() + extras

        else:
            include_only = include_only + extras

        _dict = dict([(k, getattr(self, k)) for k in include_only if isinstance(getattr(self, k),
                                                                                (hybrid_property, InstrumentedAttribute,
                                                                                 InstrumentedList,
                                                                                 dynamic.AppenderMixin)) is False and k not in exclude])

        for key, obj in _dict.items():
            if isinstance(obj, db.Model):
                _dict[key] = obj.as_dict()

            if isinstance(obj, (list, tuple)):
                items = []
                for item in obj:
                    inspect_item = inspect(item)
                    items.append(
                        dict([(k, getattr(item, k)) for k in inspect_item.attrs.keys() + extras if
                              k not in exclude and hasattr(item, k)]))

                for item in items:
                    obj = item.get(child)
                    if obj:
                        item[child] = obj.as_dict(extras=child_include)
        return _dict


class PlatformMixin(AppMixin):
    """ Mixin class for Platform related attributes and functions """

    @declared_attr
    def platform_id(cls):
        return db.Column(db.Integer, db.ForeignKey('platform.id'), nullable=False)


class UniversityMixin(AppMixin):
    """ Mixin class for University related attributes and functions """

    @declared_attr
    def platform_id(cls):
        return db.Column(db.Integer, db.ForeignKey('university.id'), nullable=False)



class UserMixin(AppMixin, UserMixin):
    """ Mixin class for User related attributes and functions """

    @declared_attr
    def user_id(cls):
        return db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    @property
    def user(self):
        if self.user_id:
            return User.query.get(self.user_id)
        else:
            return None


class Banner(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), index=True)
    url = db.Column(db.String(200), index=True)
    cover_image_url = db.Column(db.String(200), index=True)
    is_for_you = db.Column(db.Boolean, default=False, index=True)
    is_discover = db.Column(db.Boolean, default=False, index=True)
    has_file = db.Column(db.Boolean, default=False, index=True)
    file_type = db.Column(db.String(200), nullable=True, index=True)
    file_path = db.Column(db.Text, nullable=True, index=True)


class City(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    code = db.Column(db.String(200), index=True)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False, index=True)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)


class State(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    code = db.Column(db.String(200), index=True)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)
    cities = db.relationship('City', backref='state', lazy='dynamic')


class Country(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    code = db.Column(db.String(200), index=True)
    enabled = db.Column(db.Boolean, default=False, index=True)
    states = db.relationship('State', backref='country', lazy='dynamic')
    cities = db.relationship('City', backref='country', lazy='dynamic')


class Timezone(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    code = db.Column(db.String(200), index=True)
    offset = db.Column(db.String(200), index=True)  # UTC time


class PasswordResetToken(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    university_id = db.Column(db.Integer, nullable=True, index=True)
    user_id = db.Column(db.Integer, nullable=True, index=True)
    email = db.Column(db.String(200), nullable=False, index=True)
    code = db.Column(db.String(200), unique=False, default=generate_token_code, index=True)
    is_expired = db.Column(db.Boolean, default=False, index=True)


class ConfirmationToken(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False, unique=True, index=True)
    email = db.Column(db.String(200), nullable=False, index=True)
    phone = db.Column(db.String(200), nullable=True, index=True)
    code = db.Column(db.String(200), unique=True,
                     default=short_token_generator, index=True)
    is_expired = db.Column(db.Boolean, default=False, index=True)

    @property
    def url(self):
        return "%s%s/v1/api/account_verification/%s" % (app.config.get("PROTOCOL"), app.config.get("DOMAIN"), self.code)


class User(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), index=True)
    email = db.Column(db.String(200), unique=True, index=True)
    first_name = db.Column(db.String(200), unique=False, index=True)
    last_name = db.Column(db.String(200), unique=False, index=True)
    full_name = db.Column(db.String(200), unique=False, index=True)
    bio = db.Column(db.Text, unique=False, index=True)
    matric = db.Column(db.String(200), unique=False, index=True)
    phone = db.Column(db.String(200), index=True)
    profile_pic = db.Column(db.Text, index=True)
    password = db.Column(db.Text, unique=False, index=True)
    active = db.Column(db.Boolean, default=False, index=True)
    is_verified = db.Column(db.Boolean, default=False, index=True)
    is_onboarded = db.Column(db.Boolean, default=False, index=True)
    uni_verified = db.Column(db.Boolean, default=False, index=True)
    is_staff = db.Column(db.Boolean, default=False, index=True)
    is_admin = db.Column(db.Boolean, default=False, index=True)
    is_mtn = db.Column(db.Boolean, default=False, index=True)
    is_global = db.Column(db.Boolean, default=False, index=True)
    is_super_admin = db.Column(db.Boolean, default=False, index=True)
    deactivate = db.Column(db.Boolean, default=False, index=True)
    login_count = db.Column(db.Integer, default=0, index=True)
    last_login_at = db.Column(db.DateTime, index=True)
    current_login_at = db.Column(db.DateTime, index=True)
    last_login_ip = db.Column(db.String(200), index=True)
    current_login_ip = db.Column(db.String(200), index=True)
    gender = db.Column(db.String(200), index=True)
    location = db.Column(db.String(200), index=True)
    dob = db.Column(db.DateTime, index=True)
    allow_notifications = db.Column(db.Boolean, default=True, index=True)
    university_id = db.Column(db.Integer, db.ForeignKey('university.id'), nullable=True, index=True)
    collections = db.relationship('UserCollection', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    shares = db.relationship('Share', backref='user', lazy='dynamic')
    likes = db.relationship(
        'Like', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    saved_contents = db.relationship(
        'SavedContent', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    interests = db.relationship(
        'Interest', secondary="user_interests", backref=db.backref('user', lazy='dynamic'))
    user_activities = db.relationship(
        'UserActivity', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    accounts = db.relationship(
        'PlatformAccount', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    groups = db.relationship(
        'Group', secondary="user_groups", backref=db.backref('user', lazy='dynamic'))
    group_messages = db.relationship(
        'GroupMessage', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    # followers = db.relationship(
    #     'Follower', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    follows = db.relationship(
        'Follow', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    comments = db.relationship(
        'Comment', backref='user', lazy='dynamic', cascade="all,delete-orphan")
    # roles = db.relationship('Role', secondary="user_roles",
    #   backref = db.backref('users', lazy = 'dynamic'))


    @hybrid_property
    def roles(self):
        """ Fetch user roles as a list of roles """
        roles = []
        for access_group in self.access_groups:
            for role in access_group.roles:
                roles.append(role)

        return list(set(roles))

    def __repr__(self):
        return '<User %r>' % self.full_name

    def get_auth_token(self):
        """ Returns the user's authentication token """
        return hashlib.md5("%s:%s:%s" % (str(self.id), self.email, self.password)).hexdigest()

    def is_active(self):
        """ Returns if the user is active or not. Overriden from UserMixin """
        return self.active

    def update_last_login(self):
        if self.current_login_at is None and self.last_login_at is None:
            self.current_login_at = self.last_login_at = datetime.now()
            self.current_login_ip = self.last_login_ip = gethostbyname(gethostname())

        if self.current_login_at != self.last_login_at:
            self.last_login_at = self.current_login_at
            self.last_login_ip = self.current_login_ip
            self.current_login_at = datetime.now()
            self.current_login_ip = gethostbyname(gethostname())

        if self.last_login_at == self.current_login_at:
            self.current_login_at = datetime.now()
            self.current_login_ip = gethostbyname(gethostname())

        self.login_count += 1
        db.session.add(self)
        db.session.commit()


    def generate_password(self, password):
        """
        Generates a password from the plain string

        :param password: plain password string
        """

        self.password = bcrypt.generate_password_hash(password)
        db.session.add(self)
        db.session.commit()

    def check_password(self, password):
        """
        Checks the given password against the saved password

        :param password: password to check
        :type password: string

        :returns True or False
        :rtype: bool

        """
        return bcrypt.check_password_hash(self.password, password)

    def set_password(self, new_password):
        """
        Sets a new password for the user

        :param new_password: the new password
        :type new_password: string
        """

        self.password = bcrypt.generate_password_hash(new_password)
        db.session.add(self)
        db.session.commit()

    def add_role(self, role):
        """
        Adds a the specified role to the user
        :param role: Role ID or Role name or Role object to be added to the user

        """
        try:

            if isinstance(role, Role) and hasattr(role, "id"):
                role_obj = role
            elif type(role) is int:
                role_obj = Role.query.get(role)
            elif type(role) is str:
                role_obj = Role.query.filter(Role.name == role.lower()).one()

            if not role_obj:
                raise Exception("Specified role could not be found")

            if self.has_role(role_obj) is False:
                self.roles.append(role_obj)
                db.session.add(self)
                db.session.commit()
                return self
        except:
            db.session.rollback()
            raise

    def has_role(self, role):
        """
        Returns true if a user identifies with a specific role

        :param role: A role name or `Role` instance
        """
        return role in self.roles

    def add_access_group(self, access_group):
        """
        Adds a the specified access group to the user
        :param group: AccessGroup ID or AccessGroup name or AccessGroup object to be added to the user

        """
        try:

            if isinstance(access_group, AccessGroup) and hasattr(access_group, "id"):
                group_obj = access_group
            elif type(access_group) is int:
                group_obj = AccessGroup.query.get(access_group)
            elif type(access_group) is str:
                group_obj = AccessGroup.query.filter(
                    AccessGroup.name == access_group.lower()).one()

            if not group_obj:
                raise Exception("Specified access_group could not be found")

            if self.has_access_group(group_obj) is False:
                self.access_groups.append(group_obj)
                db.session.add(self)
                db.session.commit()
                return self
        except:
            db.session.rollback()
            raise

    def has_access_group(self, access_group):
        """
        Returns true if a user identifies with a specific access_group

        :param access_group: A access_group name or `AccessGroup` instance
        """
        return access_group in self.access_groups

    @property
    def follower_count(self):
        return Follow.query.filter(Follow.person_id==self.id).count()

    @property
    def following_count(self):
        return self.follows.count()

    def check_is_liked(self, content_id):
        liked_content_ids = [i.content_id for i in self.likes]
        if content_id in liked_content_ids:
            return True
        else:
            return False

    def check_is_saved(self, content_id):
        saved_content_ids = [i.content_id for i in self.saved_contents]
        if content_id in saved_content_ids:
            return True
        else:
            return False

    def check_is_shared(self, content_id):
        shared_content_ids = [i.content_id for i in self.shares]
        if content_id in shared_content_ids:
            return True
        else:
            return False

    def check_following(self, person_id):
        f = Follow.query.filter(Follow.user_id==self.id, Follow.person_id==person_id).first()
        if f:
            return True
        else:
            return False

    def check_follower(self, person_id):
        f = Follow.query.filter(Follow.user_id==person_id, Follow.person_id==self.id).first()
        if f:
            return True
        else:
            return False


class UnverifiedUser(db.Model, UserMixin, AppMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(200), unique=True, index=True)
    first_name = db.Column(db.String(200), unique=False, index=True)
    last_name = db.Column(db.String(200), unique=False, index=True)
    full_name = db.Column(db.String(200), unique=False, index=True)
    bio = db.Column(db.Text, unique=False, index=True)
    matric = db.Column(db.String(200), unique=False, index=True)
    phone = db.Column(db.String(200), index=True)
    university_id = db.Column(db.Integer, db.ForeignKey('university.id'), nullable=True, index=True)
    is_verified = db.Column(db.Boolean, default=False, index=True)
    # program_id = db.Column(db.Integer, db.ForeignKey('program.id'), nullable=True, index=True)


class VerificationToken(db.Model, AppMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False, index=True)
    email = db.Column(db.String(200), nullable=True, index=True)
    phone = db.Column(db.String(200), nullable=True, index=True)
    code = db.Column(db.String(200), unique=True,
                     default=generate_user_token_code, index=True)
    is_expired = db.Column(db.Boolean, default=False, index=True)
    expired_date = db.Column(db.DateTime, default=datetime.now()+timedelta(minutes=10), index=True)

    @property
    def url(self):
        return "%s%s/profile/confirm_email/%s" % (app.config.get("PROTOCOL"), app.config.get("DOMAIN"), self.code)


class UserActivity(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(200), nullable=False, index=True)
    content = db.Column(db.Text, index=True)
    is_read = db.Column(db.Boolean, default=False, index=True)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=True, index=True)
    university_id = db.Column(db.Integer, db.ForeignKey('university.id'), nullable=True, index=True)
    activity_type_id = db.Column(db.Integer, db.ForeignKey('activity_type.id'), nullable=True, index=True)
    actor_id = db.Column(db.Integer, index=True)
    action_id = db.Column(db.Integer, index=True)
    actor_type = db.Column(db.String(200), index=True)
    group_id = db.Column(db.String(200), index=True)
    group_key = db.Column(db.String(200), index=True)
    group_name = db.Column(db.String(200), index=True)
    object_id = db.Column(db.Integer, index=True)
    object_type = db.Column(db.String(200), index=True)

    @property
    def actor(self):
        if self.actor_id and self.actor_type=="user":
            return User.query.get(self.actor_id)
        elif self.actor_id and self.actor_type=="university":
            return University.query.get(self.actor_id)

    @property
    def actor_name(self):
        if self.actor_type=="user":
            return self.actor.full_name if self.actor else None
        elif self.actor_type=="university":
            return self.actor.name if self.actor else None

    @property
    def object(self):
        if self.object_id and self.object_type=="user":
            return User.query.get(self.object_id)
        elif self.object_id and self.object_type=="university":
            return University.query.get(self.object_id)
        elif self.object_id and self.object_type=="content":
            return self.content_object

    @property
    def content_object(self):
        if self.content_id:
            return Content.query.get(self.content_id)
        else:
            return None

    @property
    def image(self):
        if self.content_id:
            cont=Content.query.get(self.content_id)
            return cont.image if cont else None
        elif self.object_id:
            return self.object.profile_pic if self.object and self.object.profile_pic else None
        elif self.actor_id:
            return self.actor.profile_pic if self.actor and self.actor.profile_pic else None
        else:
            return None


class ActivityType(db.Model, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, index=True)
    description = db.Column(db.String(200), nullable=True, index=True)
    code = db.Column(db.String(200), nullable=False, index=True)
    user_activities = db.relationship('UserActivity', backref='activity_type', lazy='dynamic')


# class Follow(db.Model, UserMixin, AppMixin):

#     id = db.Column(db.Integer, primary_key=True)
#     fan_id = db.Column(db.Integer, nullable=False)

#     @property
#     def fan(self):
#         return User.query.get(self.fan_id)


class Follow(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    person_id = db.Column(db.Integer, nullable=False, index=True)

    @property
    def person(self):
        return User.query.get(self.person_id)


class Like(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=False, index=True)

    @property
    def name(self):
        return self.content.name

    @property
    def title(self):
        return self.content.title

    @property
    def image(self):
        return self.content.image

    @property
    def interests_slug(self):
        return self.content.interests_slug

    @property
    def platform_name(self):
        return self.content.platform_name


class Share(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=False, index=True)
    group_id = db.Column(db.Integer, db.ForeignKey('group.id'), nullable=True, index=True)
    friend_id = db.Column(db.Integer, nullable=True, index=True)

    @property
    def friend(self):
        if self.friend_id:
            return User.query.get(self.friend_id)
        else:
            return None



class SavedContent(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=False, index=True)


class UserCollection(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, index=True)
    description = db.Column(db.Text, unique=False, index=True)
    slug = db.Column(db.String(200), default=slugify_from_name, onupdate=slugify_from_name, index=True)
    value = db.Column(db.String(200), nullable=True, index=True)
    items = db.relationship('CollectionItem', backref='user_collection', lazy='dynamic', cascade='all,delete-orphan')

    @property
    def items_count(self):
        return self.items.count()


class CollectionItem(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    collection_id = db.Column(db.Integer, db.ForeignKey('user_collection.id'), nullable=False, index=True)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=False, index=True)
    content = db.relationship('Content', foreign_keys='CollectionItem.content_id')

    # @property
    # def content(self):
    #     return Content.query.get(self.content_id)

    @property
    def content_to_collection_count(self):
        return CollectionItem.query.filter(CollectionItem.content_id==self.content_id).count()

    @property
    def like_count(self):
        return self.content.like_count

    @property
    def is_rss_feed(self):
        return self.content.is_rss_feed

    @property
    def description(self):
        return self.content.description

    @property
    def share_count(self):
        return self.content.share_count

    @property
    def saved_count(self):
        return self.content.saved_count

    @property
    def comment_count(self):
        return self.content.comment_count

    @property
    def url(self):
        return self.content.url

    @property
    def caption(self):
        return self.content.caption

    @property
    def summary(self):
        return self.content.summary

    @property
    def title(self):
        return self.content.title

    @property
    def subtitle(self):
        return self.content.subtitle

    @property
    def check_is_saved(self):
        return self.content.check_is_saved(self.user_collection.user_id)

    @property
    def check_is_liked(self):
        return self.content.check_is_liked(self.user_collection.user_id)

    @property
    def check_is_shared(self):
        return self.content.check_is_shared(self.user_collection.user_id)

    @property
    def file_path(self):
        return self.content.file_path

    @property
    def file_type(self):
        return self.content.file_type

    @property
    def has_file(self):
        return self.content.has_file

    @property
    def interests_slug(self):
        return self.content.interests_slug

    @property
    def platform_name(self):
        return self.content.platform_name


class Interest(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, unique=True, index=True)
    description = db.Column(db.Text, unique=False, index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, index=True)
    value = db.Column(db.String(200), nullable=True, index=True)
    is_academic = db.Column(db.Boolean, default=False, index=True)
    users = db.relationship(
        'User', secondary="user_interests", backref=db.backref('interest', lazy='dynamic'))
    contents = db.relationship(
        'Content', secondary="interest_contents", backref=db.backref('interest', lazy='dynamic'))
    platforms = db.relationship(
        'Platform', secondary="interest_platforms", backref=db.backref('interest', lazy='dynamic'))


user_interests = db.Table('user_interests',
  db.Column('user_id', db.Integer,
            db.ForeignKey('user.id')),
  db.Column('interest_id', db.Integer,
            db.ForeignKey('interest.id'))
  )


class University(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False, nullable=False, index=True)
    handle = db.Column(db.String(200), unique=True, nullable=False, index=True)
    email_extension = db.Column(db.String(200), unique=True, nullable=False, index=True)
    description = db.Column(db.Text, index=True)
    about_information = db.Column(db.Text, index=True)  # information for your about page
    thumbnail = db.Column(db.Text, index=True)  # storefront thumbnail url
    logo = db.Column(db.Text, index=True)  # storefront thumbnail url
    account_email = db.Column(db.String(200), nullable=False, unique=True, index=True)
    email = db.Column(db.String(200), nullable=False, unique=True, index=True)
    phone = db.Column(db.String(200), index=True)
    street = db.Column(db.String(200), index=True)
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'), nullable=True, index=True)
    is_enabled = db.Column(db.Boolean, default=False, index=True)
    is_premium = db.Column(db.Boolean, default=False, index=True)
    twitter_handle = db.Column(db.String(200), index=True)
    facebook_handle = db.Column(db.String(200), index=True)
    google_handle = db.Column(db.String(200), index=True)
    instagram_handle = db.Column(db.String(200), index=True)
    owner_id = db.Column(db.Integer, nullable=True, index=True)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=True, index=True)
    country_id = db.Column(
        db.Integer, db.ForeignKey('country.id'), nullable=True, index=True)
    timezone_id = db.Column(
        db.Integer, db.ForeignKey('timezone.id'), nullable=True, index=True)
    users = db.relationship('User', backref='university', lazy='dynamic')
    contents = db.relationship(
        'Content', backref='university', lazy='dynamic', cascade="all,delete-orphan")


    def add_user(self, username, email, password, full_name=None, is_staff=True, is_verified=True, is_mtn=False, is_admin=False, is_global=False, is_super_admin=False, active=False):
        """
        Adds a user to a university

        :param username: username (should be unique)
        :type username: string
        :param email: email address (should be unique)
        :type email: string
        :param password: user's password. (this is automatically encrypted)
        :type email: string
        :param full_name: user's full name (optional)
        :type full_name: string
        :param active

        :returns: a User object or None
        :rtype: dml.models.User

        """
        try:
            user = User(username=username, email=email,
                        full_name=full_name, active=active, is_staff=True, is_verified=True, is_mtn=False, is_global=False, is_super_admin=False)
            user.generate_password(password)
            self.users.append(user)
            db.session.commit()
            if is_admin:
                # If the user is the account owner, set the user on the shop
                self.owner_id = user.id
                db.session.add(self)
                db.session.commit()
            return user
        except:
            db.session.rollback()
            raise


    @property
    def owner(self):
        if self.owner_id:
            return User.query.get(self.owner_id)
        else:
            return None


class PlatformAccount(db.Model, UserMixin, PlatformMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), index=True)
    email = db.Column(db.String(200), nullable=True, index=True)
    auth_token = db.Column(db.Text, index=True)



class Platform(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False, nullable=False, index=True)
    handle = db.Column(db.String(200), unique=True, nullable=False, index=True)
    url = db.Column(db.String(200), nullable=True, index=True)
    description = db.Column(db.Text, index=True)
    about_information = db.Column(db.Text, index=True)  # information for your about page
    thumbnail = db.Column(db.Text, index=True)
    logo = db.Column(db.Text, index=True)
    email = db.Column(db.String(200), nullable=True, unique=True, index=True)
    phone = db.Column(db.String(200), index=True)
    street = db.Column(db.String(200), index=True)
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'), nullable=True, index=True)
    is_enabled = db.Column(db.Boolean, default=False, index=True)
    is_rss_feed = db.Column(db.Boolean, default=False, index=True)
    rss_url = db.Column(db.Text, unique=False, index=True)
    twitter_handle = db.Column(db.String(200), index=True)
    facebook_handle = db.Column(db.String(200), index=True)
    google_handle = db.Column(db.String(200), index=True)
    instagram_handle = db.Column(db.String(200), index=True)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=True, index=True)
    interests = db.relationship(
        'Interest', secondary="interest_platforms", backref=db.backref('platform', lazy='dynamic'))
    country_id = db.Column(
        db.Integer, db.ForeignKey('country.id'), nullable=True, index=True)
    timezone_id = db.Column(
        db.Integer, db.ForeignKey('timezone.id'), nullable=True)
    contents = db.relationship(
        'Content', backref='platform', lazy='dynamic', cascade="all,delete-orphan")
    accounts = db.relationship(
        'PlatformAccount', backref='platform', lazy='dynamic', cascade="all,delete-orphan")


interest_platforms = db.Table('interest_platforms',
  db.Column('interest_id', db.Integer,
            db.ForeignKey('interest.id')),
  db.Column('platform_id', db.Integer,
            db.ForeignKey('platform.id'))
  )

class Content(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False, index=True)
    subtitle = db.Column(db.String(200), unique=False, index=True)
    author = db.Column(db.String(200), unique=False, index=True)
    short_summary = db.Column(db.Text, unique=False, index=True)
    description = db.Column(db.Text, index=True)
    summary = db.Column(db.Text, unique=False, index=True)
    image = db.Column(db.Text, unique=False, index=True)
    partner_logo = db.Column(db.Text, unique=False, index=True)
    title = db.Column(db.String(200), unique=False, index=True)
    caption = db.Column(db.Text, unique=False, index=True)
    level = db.Column(db.String(200), unique=False, index=True)
    key = db.Column(db.String(200), unique=False, index=True)
    slug = db.Column(db.String(200), unique=False, index=True)
    url = db.Column(db.Text, unique=False, index=True)
    wiki_diff = db.Column(db.Text, index=True)
    wiki_history = db.Column(db.Text, unique=False, index=True)
    expected_duration_unit = db.Column(db.String(200), unique=False, index=True)
    expected_duration = db.Column(db.String(200), index=True)
    required_knowledge = db.Column(db.Text, index=True)
    expected_learning = db.Column(db.Text, index=True)
    syllabus = db.Column(db.Text, index=True)
    has_file = db.Column(db.Boolean, default=False, index=True)
    file_type = db.Column(db.String(200), nullable=True, index=True)
    file_path = db.Column(db.Text, nullable=True, index=True)
    last_viewed = db.Column(db.DateTime, index=True)
    available = db.Column(db.Boolean, default=True, index=True)
    full_course_available = db.Column(db.Boolean, default=False, index=True)
    cover_image_id = db.Column(db.Integer, index=True)  # over image among all images
    is_private = db.Column(db.Boolean, default=False, index=True)
    is_rss_feed = db.Column(db.Boolean, default=False, index=True)
    is_featured = db.Column(db.Boolean, default=False, index=True)
    is_mtn = db.Column(db.Boolean, default=False, index=True)
    editors_pick = db.Column(db.Boolean, default=False, index=True)
    publication_date = db.Column(db.DateTime, index=True)
    expiry_date = db.Column(db.DateTime, index=True)
    content_type_id = db.Column(db.Integer, db.ForeignKey('content_type.id'), nullable=False, index=True)
    interests = db.relationship(
        'Interest', secondary="interest_contents", backref=db.backref('content', lazy='dynamic'))
    university_id = db.Column(db.Integer, db.ForeignKey('university.id'), nullable=True, index=True)
    collection_items = db.relationship('CollectionItem', backref='_content', lazy='dynamic')
    platform_id = db.Column(db.Integer, db.ForeignKey('platform.id'), nullable=True, index=True)
    library_id = db.Column(db.Integer, db.ForeignKey('library.id'), nullable=True, index=True)
    view_count = db.Column(db.Integer, default=0, index=True)
    shares = db.relationship('Share', backref='content', lazy='dynamic')
    # user_activities = db.relationship(
    #     'UserActivity', backref='content', lazy='dynamic', cascade="all,delete-orphan")
    likes = db.relationship(
        'Like', backref='content', lazy='dynamic', cascade="all,delete-orphan")
    saved_contents = db.relationship(
        'SavedContent', backref='content', lazy='dynamic', cascade="all,delete-orphan")
    comments = db.relationship(
        'Comment', backref='content', lazy='dynamic', cascade="all,delete-orphan")

    def update_view_count(self):
        self.view_count += 1
        self.last_viewed = datetime.now()
        db.session.add(self)
        db.session.commit()

    @hybrid_property
    def like_count(self):
        # return len(self.likes)
        return self.likes.count()

    @like_count.expression
    def child_count(cls):
        return (select([func.count(Like.content_id)]).
                where(Like.content_id == cls.id).
                label("like_count")
                )
    # @like_count.setter
    # def like_count(self, value):
    #     """ the setter for like_count """
    #     self._like_count = value

    @hybrid_property
    def share_count(self):
        return self.shares.count()

    @hybrid_property
    def saved_count(self):
        return self.saved_contents.count()

    @hybrid_property
    def comment_count(self):
        return self.comments.count()

    def check_is_liked(self, user_id):
        user = User.query.get(user_id)
        if user:
            liked_content_ids = [i.content_id for i in user.likes]
            if self.id in liked_content_ids:
                return True
        else:
            return False

    def check_is_saved(self, user_id):
        user = User.query.get(user_id)
        if user:
            saved_content_ids = [i.content_id for i in user.saved_contents]
            if self.id in saved_content_ids:
                return True
        else:
            return False

    def check_is_shared(self, user_id):
        user = User.query.get(user_id)
        if user:
            shared_content_ids = [i.content_id for i in user.shares]
            if self.id in shared_content_ids:
                return True
        else:
            return False

    @property
    def platform_name(self):
        return self.platform.name if self.platform else None

    @property
    def type_slug(self):
        return self.content_type.slug if self.content_type else None

    @property
    def interests_slug(self):
        if len(self.interests) > 0:
            return [i.slug for i in self.interests]
        else:
            return []

    @property
    def content_to_collection_count(self):
        return CollectionItem.query.filter(CollectionItem.content_id==self.id).count()



interest_contents = db.Table('interest_contents',
  db.Column('interest_id', db.Integer,
            db.ForeignKey('interest.id')),
  db.Column('content_id', db.Integer,
            db.ForeignKey('content.id'))
  )


class Comment(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=False, index=True)
    body = db.Column(db.Text, unique=False, index=True)

    @property
    def content_comment_count(self):
        return Comment.query.filter(Comment.content_id==self.content_id).count()


class ContentType(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, unique=True, index=True)
    description = db.Column(db.Text, unique=False, index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    value = db.Column(db.String(200), nullable=True, index=True)
    is_model = db.Column(db.Boolean, index=True)
    contents = db.relationship("Content", backref='content_type', lazy='dynamic')


class LibraryCategory(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, unique=True, index=True)
    description = db.Column(db.Text, unique=False, index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    libraries = db.relationship("Library", backref='library_category', lazy='dynamic')


class Library(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, unique=True, index=True)
    description = db.Column(db.Text, unique=False, index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    value = db.Column(db.String(200), nullable=True, index=True)
    url = db.Column(db.Text, unique=False, index=True)
    library_category_id = db.Column(db.Integer, db.ForeignKey('library_category.id'), nullable=False, index=True)


user_groups = db.Table('user_groups',
  db.Column('group_id', db.Integer,
            db.ForeignKey('group.id')),
  db.Column('user_id', db.Integer,
            db.ForeignKey('user.id'))
  )

class Group(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False, index=True)
    admin_id = db.Column(db.Integer, index=True)
    shares = db.relationship('Share', backref='group', lazy='dynamic')
    users = db.relationship(
        'User', secondary="user_groups", backref=db.backref('group', lazy='dynamic'))
    group_messages = db.relationship("GroupMessage", backref='group', lazy='dynamic')

    @property
    def admin(self):
        if self.admin_id:
            return User.query.get(self.admin_id)
        else:
            return None


class GroupMessage(db.Model, UserMixin, AppMixin):

    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.Integer, db.ForeignKey('group.id'), nullable=False, index=True)
    body = db.Column(db.Text, unique=False, index=True)
