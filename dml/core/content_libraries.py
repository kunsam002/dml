__author__ = 'kunsam002'

from dml import login_manager, app, logger
from dml.models import *
from dml.services.assets import LibraryCategoryService, LibraryService
import json
import feedparser
import requests
import urllib
from datetime import datetime
from time import mktime


def arxiv_search(search_q=None,start=0,max_results=10,return_results=False):
    base_url = "http://export.arxiv.org/api/query?search_query=all"
    if search_q:
        url = "%s:%s&start=%s&max_results=%s" %(base_url,search_q,start,max_results)
    else:
        url = base_url

    lib = Library.query.filter(Library.value=="arxiv").first()

    data = feedparser.parse(url)
    count = data.headers.get("content-length",0)
    if return_results:
        refine =[]
        data = data.entries
        for i in data:
            _date = i.get("published_parsed", None)

            if _date:
                d_date = datetime.fromtimestamp(mktime(_date)).date().isoformat()
            else:
                d_date = datetime.today().date().isoformat()

            _i_ = {"library_id":lib.id,"is_rss_feed":False, "summary":i.get("summary_detail",{"value":""}).get("value",""),"url":i.get("link",""), "name":i.get("title",""),"title":i.get("title",""),"description":i.get("description",""),"publication_date":d_date}

            refine.append(_i_)

        return refine, int(count)

    else:

        refine = {"search_content_count":int(count)}
        return refine


def ieeexplore_search(search_q=None,start=0,max_results=10,return_results=False):
    base_url = "http://ieeexplore.ieee.org/gateway/ipsSearch.jsp?"
    if search_q:
        url = "%squerytext=%s&sortorder=asc" %(base_url,search_q)
    else:
        return []

    lib = Library.query.filter(Library.value=="ieeexplore").first()
    data = feedparser.parse(url)
    count = data.headers.get("content-length",0)
    if return_results:
        refine =[]
        data = data.entries
        for i in data:
            _date = i.get("published_parsed", None)

            if _date:
                d_date = datetime.fromtimestamp(mktime(_date)).date().isoformat()
            else:
                d_date = datetime.today().date().isoformat()

            _i_ = {"library_id":lib.id,"is_rss_feed":False, "summary":i.get("summary_detail",{"value":""}).get("value",""),"url":i.get("link",""), "name":i.get("title",""),"title":i.get("title",""),"description":i.get("description",""),"publication_date":d_date}

            refine.append(_i_)

        return refine, int(count)

    else:

        refine = {"search_content_count":int(count)}
        return refine


def fetch_search_content(value, search_q=None,start=0,max_results=10,return_results=False):
    """ Generates a canned report for a specified period """
    LIBRARY_TYPES = {
        "arxiv": arxiv_search,
        "ieeexplore": ieeexplore_search
    }

    _func = LIBRARY_TYPES.get(value, None)

    if _func:
        data = _func(search_q=search_q, start=start, max_results=max_results, return_results=return_results)

        return data
