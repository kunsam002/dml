__author__ = 'kunsam002'

from dml import login_manager, app, logger
from dml.models import *
from dml.services.assets import ContentService, ContentTypeService, add_content_interests
from dml.services.accounts import PlatformService
import json
import requests
import urllib
import feedparser
from datetime import datetime
from time import mktime


content_type = ContentType.query.filter(ContentType.slug=="rss-feed").first()
if not content_type:
	data = {"name":"RSS Feed", "slug":"rss-feed"}
	content_type = ContentTypeService.create(**data)


def fetch_contents():
	# feedparser.parse("")

	platforms = Platform.query.filter(Platform.is_rss_feed==True, Platform.is_enabled==True).all()

	# rss_urls =[]
	# for i in platforms:
	# 	rss_urls.append(i.rss_url)

	# feeds = []
	# for url in rss_urls:
	# 	feeds.extend(feedparser.parse(url).entries)
	_i_={}

	for p in platforms:
		logger.info("Loading Content Feeds From %s" %p.name)
		data = feedparser.parse(p.rss_url).entries
		for i in data:
			try:
				key = i.pop("id","")
				_date = i.get("published_parsed", None)

				if _date:
					d_date = datetime.fromtimestamp(mktime(_date)).date().isoformat()

				else:
					d_date = datetime.today().date().isoformat()

				_i_ = {"is_rss_feed":True, "summary":i.get("summary_detail",{"value":""}).get("value",""),"key":key, "platform_id":p.id, "content_type_id": content_type.id, "url":i.get("link",""), "name":i.get("title",""),"title":i.get("title",""),"description":i.get("description",""),"publication_date":d_date}

				cont = Content.query.filter(Content.key==key, Content.platform_id==p.id).first()
				if cont:
					obj = ContentService.update(cont.id, **_i_)
				else:
					obj = ContentService.create(**_i_)
					i_ids = [i.id for i in p.interests]
					add_content_interests(obj.id, i_ids)
				if len(obj.interests) == 0:
					ContentService.add_interests(obj.id)
			except:
				db.session.rollback()
				pass

		logger.info("Completed")
