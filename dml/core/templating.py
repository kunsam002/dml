__author__ = 'kunsam002'
"""
templating.py

This module is responsible for converting templates into string (either html or text)
"""

from dml import app, logger, db
from flask import render_template
from jinja2 import TemplateNotFound


def generate_email_content(template_name, data={}):
	"""
	Generates email content by using the template name and the given data
	email templates are stored as either .html or .txt in the templates email directory.
	When a template name is given, it locates the html version by concatenating the template name with '.html'
	and the text template with '.txt'.

	:param template_name: The template name
	:param data: Template data to merge with

	:returns: a (html, text) tuple with either fields
	"""

	html_template = "emails/%s.html" % template_name
	text_template = "emails/%s.txt" % template_name

	# Fetch html template

	try:
		html = render_template(html_template, **data)
	except TemplateNotFound, e:
		logger.info(e)
		logger.info("html template not found")
		html = None


	try:
		text = render_template(text_template, **data)
	except TemplateNotFound, e:
		logger.info("text template not found")
		text = None


	return html, text
