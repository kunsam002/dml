__author__ = 'kunsam002'

from dml import login_manager, app, logger
from dml.models import *
import json
from firebase import firebase
# from pyfcm import FCMNotification
import pyrebase

db_uri = app.config.get("FIREBASE_DATABASE_URI")
noti_url = app.config.get("FIREBASE_PUSH_NOTI_URL")
# push_service = FCMNotification(api_key=noti_url)

proxy_dict = {
      "http"  : "http://127.0.0.1",
      "https" : "http://127.0.0.1",
    }




firebase_config = {
  "apiKey": app.config.get("FIREBASE_PRIVATE_KEY"),
  "authDomain": "%s.firebaseapp.com"% app.config.get("FIREBASE_PROJECT_ID"),
  "databaseURL": app.config.get("FIREBASE_DATABASE_URI"),
  "storageBucket": "%s.appspot.com"% app.config.get("FIREBASE_PROJECT_ID"),
  "serviceAccount": app.config.get("FIREBASE_SERVICE_ACCOUNT_FILE_PATH")
}

all_nodes = ['reported-groups', 'group-messages', 'users', 'groups', 'notificationRequests']
def fetch_node(code="users"):
    firebase = pyrebase.initialize_app(firebase_config)
    db = firebase.database()
    node=db.child(code)
    all_node_cont=node.get()
    results = [{"key":i.key(),"val":i.val()} for i in all_node_cont.each()]
    return results


def fetch_node_object(code="users", key=None):
    if key:
        firebase = pyrebase.initialize_app(firebase_config)
        db = firebase.database()
        obj=db.child(code).child(key).get().val()

        return obj
    else:
        return {}


# def test_push():
#     push_service = FCMNotification(api_key=noti_url, proxy_dict=proxy_dict)
#
#     # Your api-key can be gotten from:  https://console.firebase.google.com/project/<project-name>/settings/cloudmessaging
#
#     registration_id = "<device registration_id>"
#     message_title = "Uber update"
#     message_body = "Hi john, your customized news for today is ready"
#     result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)
#     return result
