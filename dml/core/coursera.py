__author__ = 'kunsam002'

from dml import login_manager, app, logger
from dml.models import *
from dml.services.assets import ContentService, ContentTypeService
from dml.services.accounts import PlatformService
import json
import requests
import urllib

base_url = "https://api.coursera.org/api/courses.v1"

platform = Platform.query.filter(Platform.handle=="cousera").first()
if not platform:
	data = {"name":"Cousera", "handle":"cousera", "is_enabled":True}
	platform = PlatformService.create(**data)

content_type = ContentType.query.filter(ContentType.slug=="e-learning").first()
if not content_type:
	data = {"name":"E-Learning", "slug":"e-learning"}
	content_type = ContentTypeService.create(**data)


def fetch_courses(**kwargs):
	try:
		courses = []
		start = kwargs.get("start",0)
		limit = kwargs.get("limit",100)

		resp = requests.get(base_url)

		if resp.status_code == 200:
			data = json.loads(resp.content)
			paging = data.get("paging",[])
			pages = int(paging.get("total",0)) / int(paging.get("next",1))

			for i in range(0,pages+1):
				kwargs["start"] = start
				kwargs["limit"] = limit
				kwargs["fields"]= "language,shortDescription,photoUrl,description,workload,startDate,partnerLogo,previewLink,specializations,categories"
				kwargs["includes"]= "instructorIds,partnerIds,specializations,v1Details,v2Details"
				url = "%s%s%s" % (base_url, "?", urllib.urlencode(kwargs))
				resp = requests.get(url)
				data = json.loads(resp.content)

				courses = courses + data.get("elements",[])

				start = start + 100
				limit = limit + 100

			return courses
		else:
			raise resp.content

	except:
		raise



def populate_courses(**kwargs):
	try:

		courses = fetch_courses()

		for c in courses:
			key  = c.pop("id","")
			c["title"] = c.get("name","")
			c["expected_duration"] = c.get("workload","")
			c["partner_logo"] = c.get("partnerLogo","")
			c["image"]= c.get("photoUrl","")
			c["key"] = key
			c["content_type_id"]=content_type.id
			c["platform_id"]=platform.id

			cont = Content.query.filter(Content.key==key, Content.platform_id==platform.id).first()
			if cont:
				obj = ContentService.update(cont.id, **c)
			else:
				obj = ContentService.create(**c)

			# if len(obj.interests) == 0:
			# 	ContentService.add_interests(obj.id)

		logger.info("All Cousera Courses have been successfully populated")

	except:
		raise
