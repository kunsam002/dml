"""
public.py

@Author: Ogunmokun, Olukunle

The public views required to sign up and get started
"""

from flask import Blueprint, render_template, abort, redirect, \
    flash, url_for, request, session, g, make_response, current_app
from flask_login import logout_user, login_required, login_user, current_user
from dml import db, logger, app
from dml.forms import *
from dml.services import *
from dml.services.accounts import *
from datetime import date, datetime, timedelta
from dml.models import *
from sqlalchemy import asc, desc, or_, and_, func
from dml.forms import *
from dml.core import utils, templating
from dml.core.utils import build_page_url
from dml.signals import *
import time
import json
import urllib
from flask_principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
import base64
import requests
import xmltodict
import os
import sys
import random
import pprint
import cgi


control = Blueprint('control', __name__, template_folder='templates', static_folder='../static/admin', url_prefix='/admin')


@app.errorhandler(404)
def page_not_found(e):
    title = "404- Page Not Found"
    error_number = "404"
    error_title = "Page not found!"
    error_info = "The requested page cannot be found or does not exist. Please contact the Administrator."

    return render_template('admin/error.html', **locals()), 404


@app.errorhandler(500)
def internal_server_error(e):
    title = "500- Internal Server Error"
    error_number = "500"
    error_title = "Server Error!"
    error_info = "There has been an Internal server Error. Please try again later or Contact the Administrator."

    return render_template('admin/error.html', **locals()), 500


def object_length(data):
    return len(data)




@control.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = date.today()
    minimum = min
    string = str
    number_format = utils.number_format
    length = object_length
    join_list = utils.join_list
    slugify = utils.slugify
    paging_url_build = build_page_url
    clean_ascii = utils.clean_ascii
    

    return locals()


@control.route('/login/', methods=["GET", "POST"])
def login():
    page_title = "Login"

    form = LoginForm(csrf_enabled=False)
    if form.validate_on_submit():
        data = form.data
        username = data["username"]
        password = data["password"]

    return render_template("admin/auth/login.html", **locals())


@control.route('/')
def index():
    
    logger.info("yet")
    return render_template('admin/index.html', **locals())


@control.route('/staffs/')
def staffs():
    
    return render_template('admin/index.html', **locals())


@control.route('/instituitions/')
def instituitions():

    page_title="Instituitions"
    page_caption = "List of All Instituitions"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    query = University.query

    if search_q:
        queries = [University.name.ilike("%%%s%%"%search_q), University.handle.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(University.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())


@control.route('/instituitions/create/')
def create_instituition():
    page_title="Create Instituition"
    page_caption = "Registers an Instituition"

    form = UniversityForm()

    if form.validate_on_submit():
        data = form.data
        obj = UniversityService.create(**data)

        if obj:
            flash("Institution Record Registered Successfully")

    return render_template('admin/index.html', **locals())


@control.route('/instituitions/<int:id>/update/')
def update_instituition(id):
    page_title="Update Instituition"
    page_caption = "Update an Instituition Record"

    obj = UniversityService.get(id)
    form = UniversityForm(obj=obj)

    if form.validate_on_submit():
        data = form.data
        obj = UniversityService.update(obj.id, **data)

        if obj:
            flash("Institution Record Updated Successfully")

    return render_template('admin/index.html', **locals())


@control.route('/instituitions/<int:id>/delete/')
def delete_instituition(id):
    page_title="Delete Instituition"
    page_caption = "Delete an Instituition Record"

    obj = UniversityService.get(id)

    if request.method == "POST":
        UniversityService.delete(id)
        flash("Delete Successful")
        return redirect(url_for('.instituitions'))

    return render_template('admin/index.html', **locals())


@control.route('/platforms/')
def platforms():

    page_title="Platforms"
    page_caption = "List of All Platforms"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    query = Platform.query

    if search_q:
        queries = [Platform.name.ilike("%%%s%%"%search_q), Platform.handle.ilike("%%%s%%"%search_q), Platform.url.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(Platform.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())


@control.route('/platforms/create/')
def create_platform():
    page_title="Create Platform"
    page_caption = "Registers an Platform"

    form = PlatformForm()

    if form.validate_on_submit():
        data = form.data
        obj = PlatformService.create(**data)

        if obj:
            flash("Platform Record Registered Successfully")

    return render_template('admin/index.html', **locals())


@control.route('/platforms/<int:id>/update/')
def update_platform(id):
    page_title="Update Platform"
    page_caption = "Update an Platform Record"

    obj = PlatformService.get(id)
    form = PlatformForm(obj=obj)

    if form.validate_on_submit():
        data = form.data
        obj = PlatformService.update(obj.id, **data)

        if obj:
            flash("Platform Record Updated Successfully")

    return render_template('admin/index.html', **locals())


@control.route('/platforms/<int:id>/delete/')
def delete_platform(id):
    page_title="Delete Platform"
    page_caption = "Delete a Platform Record"

    obj = PlatformService.get(id)

    if request.method == "POST":
        PlatformService.delete(id)
        flash("Delete Successful")
        return redirect(url_for('.platforms'))

    return render_template('admin/index.html', **locals())


@control.route('/content_types/')
def content_types():

    page_title="Content Types"
    page_caption = "List of All Content Types"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    query = ContentType.query

    if search_q:
        queries = [ContentType.name.ilike("%%%s%%"%search_q), ContentType.slug.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(ContentType.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())


@control.route('/content_types/create/')
def create_content_type():
    page_title="Create Content Type"
    page_caption = "Registers an Content Type"

    form = ContentTypeForm()

    if form.validate_on_submit():
        data = form.data
        obj = ContentTypeService.create(**data)

        if obj:
            flash("Content Type Record Registered Successfully")

    return render_template('admin/index.html', **locals())


@control.route('/content_types/<int:id>/update/')
def update_content_type(id):
    page_title="Update Content Type"
    page_caption = "Update an Content Type Record"

    obj = ContentTypeService.get(id)
    form = ContentTypeForm(obj=obj)

    if form.validate_on_submit():
        data = form.data
        obj = ContentTypeService.update(obj.id, **data)

        if obj:
            flash("Content Type Record Updated Successfully")

    return render_template('admin/index.html', **locals())


@control.route('/content_types/<int:id>/delete/')
def delete_content_type(id):
    page_title="Delete Content Type"
    page_caption = "Delete a Content Type Record"

    obj = ContentTypeService.get(id)

    if request.method == "POST":
        ContentTypeService.delete(id)
        flash("Delete Successful")
        return redirect(url_for('.content_types'))

    return render_template('admin/index.html', **locals())


@control.route('/contents/')
def contents():

    logger.info("in ")

    page_title="Contents"
    page_caption = "List of All Contents"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        raise

    query = Content.query

    if search_q:
        queries = [Content.name.ilike("%%%s%%"%search_q), Content.title.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(Content.date_created)).paginate(page, 100, False)
    
    return render_template('admin/index.html', **locals())



@control.route('/interests/')
def interests():

    page_title="Interests"
    page_caption = "List of All Interests"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    query = Interest.query

    if search_q:
        queries = [Interest.name.ilike("%%%s%%"%search_q), Interest.slug.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(Interest.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())



@control.route('/libraries/')
def libraries():

    page_title="Libraries"
    page_caption = "List of All Libraries"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    query = Library.query

    if search_q:
        queries = [Library.name.ilike("%%%s%%"%search_q), Library.slug.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(Library.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())


@control.route('/libraries/<int:id>/library_categories/')
def library_categories(id):

    page_title="Library Categories"
    page_caption = "List of All Library Categories"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    library = Library.query.get(id)
    if not library:
        abort(404)

    query = LibraryCategory.query.filter(LibraryCategory.library_id==library.id)

    if search_q:
        queries = [LibraryCategory.name.ilike("%%%s%%"%search_q), LibraryCategory.slug.ilike("%%%s%%"%search_q), LibraryCategory.url.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(LibraryCategory.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())


@control.route('/users/')
def users():

    page_title="Users"
    page_caption = "List of All Users"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    query = User.query

    if search_q:
        queries = [User.full_name.ilike("%%%s%%"%search_q), User.matric.ilike("%%%s%%"%search_q), User.email.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(User.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())


@control.route('/activity_types/')
def activity_types():

    page_title="Activity Types"
    page_caption = "List of All Activity Types"

    try:
        page = int(request.args.get("page",1))
        search_q = request.args.get("q",None)
    except:
        abort(404)

    query = ActivityType.query

    if search_q:
        queries = [ActivityType.name.ilike("%%%s%%"%search_q), ActivityType.code.ilike("%%%s%%"%search_q)]
        query = query.filter(or_(*queries))

    results = query.order_by(desc(ActivityType.date_created)).paginate(page, 20, False)
    
    return render_template('admin/index.html', **locals())

