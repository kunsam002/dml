"""
public.py

@Author: Ogunmokun, Olukunle

The public views required to sign up and get started
"""

from flask import Blueprint, render_template, abort, redirect, \
    flash, url_for, request, session, g, make_response, current_app
from flask_login import logout_user, login_required, login_user, current_user
from dml import db, logger, app, redis, cache
from dml.forms import *
from dml.services import *
from dml.services import accounts
from datetime import date, datetime, timedelta
from dml.models import *
from sqlalchemy import asc, desc, or_, and_, func
from dml.forms import *
from dml.core import utils, templating
from dml.core.utils import build_page_url
from dml.signals import *
import time
import json
import urllib
from flask_principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
from  sqlalchemy.sql.expression import func, select
import base64
import requests
import xmltodict
import os
import sys
import random
import pprint
import cgi

www = Blueprint('public', __name__, template_folder='templates', static_folder='../static/public')


@app.errorhandler(404)
def page_not_found(e):
    title = "404- Page Not Found"
    error_number = "404"
    error_title = "Page not found!"
    error_info = "The requested page cannot be found or does not exist. Please contact the Administrator."

    return render_template('public/error.html', **locals()), 404


@app.errorhandler(500)
def internal_server_error(e):
    title = "500- Internal Server Error"
    error_number = "500"
    error_title = "Server Error!"
    error_info = "There has been an Internal server Error. Please try again later or Contact the Administrator."

    return render_template('public/error.html', **locals()), 500

# Caching function: same way as regular caching but this time need to specify the "key_prefix" argument, otherwise it will use the request.path which can lead to conflicts
# @cache.cached(timeout=10, key_prefix="obj_length")
def object_length(data):
    return len(data)




@www.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = date.today()
    minimum = min
    string = str
    number_format = utils.number_format
    length = object_length
    join_list = utils.join_list
    slugify = utils.slugify
    paging_url_build = build_page_url
    clean_ascii = utils.clean_ascii


    return locals()


# @www.route('/<string:path>/')
# @www.route('/')
# @cache.cached(timeout=300) #cache the view in 5 minutes
# def index(path=None):

#     return render_template('public/index.html', **locals())


# @www.route('/contents/')
# def contents(path=None):

#     try:
#         page = int(request.args.get("page",1))
#         search_q = request.args.get("q",None)
#     except:
#         abort(404)

#     query = Content.query.order_by(func.rand())

#     if search_q:
#         queries = [Content.title.ilike("%%%s%%"%search_q), Content.subtitle.ilike("%%%s%%"%search_q)]
#         query = query.filter(or_(*queries))

#     # url = app.config.get("CANONICAL_API_URL") + "/contents"
#     # logger.info(url)

#     # data = requests.get(url)
#     # logger.info(data)

#     results = query.order_by(desc(Content.date_created)).paginate(page, 30, False)

#     return render_template('public/contents.html', **locals())




def event_stream():
    pubsub = redis.pubsub()
    pubsub.subscribe('notifications')
    for message in pubsub.listen():
        print message
        yield 'data: %s\n\n' % message['data']

@www.route('/push/<string:msg>/post', methods=['POST'])
def push_post(msg):
    if request.methods == "POST":
        redis.publish('notifications', msg)
        return flask.redirect('/')

@www.route('/push/stream')
def push_stream():
    return flask.Response(event_stream(), mimetype="text/event-stream")
