__author__ = 'kunsam002'
"""
All subscriptions having to do with accounts services.
"""

from dml.signals import *
from dml import app, logger
from dml.models import *
from dml.core.messaging import send_email, send_push_notification
from dml.core import templating, utils, firebase_connection
from datetime import date, datetime, timedelta
from dml.services.authentication import ConfirmationTokenService
from dml.services.operations import UserActivityService
import json
from flask import make_response


@activity_created.connect
def _activity_created(obj_id, **kwargs):
    """ Sends out an email when the an student signs up """
    today = date.today()

    activity = UserActivityService.get(obj_id)
    if not activity:
        raise Exception("User Activity does not exist")

    user = activity.user
    user_noti_id=None
    obj = firebase_connection.fetch_node_object(code="users",key=user.id)
    if obj:
        user_noti_id = obj.get("notificationID",None)

    # data ={
    # 	"data":{
    # 		"title":activity.subject,
    # 		"body":activity.content,
    #         "content_image":activity.content_object.image if activity.content_object else None,
    #         "actor_image":activity.actor.profile_pic if activity.actor else None
    # 	},
    # 	"to":user_noti_id
    # }
    if user_noti_id:
        # data={
        # 	"notification":{
        # 		"title":activity.subject,
        # 		"body":activity.content
        # 	},
        # 	"to":user_noti_id
        # }
        data={
        	"data":{
        		"actor_id":activity.actor_id,
        		"action_id":activity.action_id,
                "object_id":activity.object_id,
                "actor_name":activity.actor_name,
                "group_id":activity.group_id,
                "group_key":activity.group_key
        	},
        	"to":user_noti_id
        }
        try:
            send_push_notification(data=data)
        except Exception as e:
            _data_ = {"status":"Failure","message":"Push Notification sending Failed %s"%e}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 403)
            resp.headers['Content-Type'] = "application/json"
            return resp
