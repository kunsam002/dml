__author__ = 'kunsam002'
"""
All subscriptions having to do with accounts services.
"""

from dml.signals import *
from dml import app, logger
from dml.models import *
from dml.core.messaging import send_email
from dml.core import templating, utils
from datetime import date, datetime, timedelta
from dml.services.authentication import ConfirmationTokenService
from dml.services.accounts import UserService


@student_created.connect
def _student_created(obj_id, **kwargs):
    """ Sends out an email when the an student signs up """
    today = date.today()

    logger.info("in subscriber")

    user = User.query.get(obj_id)
    if not user:
        raise Exception("User does not exist")

    token = ConfirmationToken.query.filter(ConfirmationToken.user_id==user.id).first()
    if token:
        _d={"code":short_token_generator(), "is_expired":False}
        token = ConfirmationTokenService.update(token.id, **_d)
    if not token:
        _data_={"user_id":user.id, "email":user.email, "phone":user.phone}
        token=ConfirmationTokenService.create(**_data_)
        # student_created.send(user.id)
        # raise Exception("Confirmation Token not found!")

    html, text = templating.generate_email_content("student_signup", data=locals())

    try:
        send_email(subject="Welcome", recipients=[user.email], html=html, text=text, files=None)
    except:
        _data_ = {"status":"incomplete","message":"Email Notification Sending Failed."}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 403)
        resp.headers['Content-Type'] = "application/json"
        return resp


@auto_signup_to_verify.connect
def _auto_signup_to_verify(email, **kwargs):
    today = date.today()

    data = {"email":email}
    user = User.query.filter(User.email==email).first()
    if not user:
        user = UserService.create(**data)
    token = ConfirmationToken.query.filter(ConfirmationToken.user_id==user.id).first()
    if token:
        _d={"code":short_token_generator(), "is_expired":False}
        token = ConfirmationTokenService.update(token.id, **_d)
    if not token:
        _data_={"user_id":user.id, "email":user.email, "phone":user.phone}
        token= ConfirmationTokenService.create(**_data_)
        # student_created.send(user.id)
        # raise Exception("Confirmation Token not found!")

    html, text = templating.generate_email_content("student_signup", data=locals())

    try:
        send_email(subject="Welcome", recipients=[user.email], html=html, text=text, files=None)
    except:
        _data_ = {"status":"incomplete","message":"Email Notification Sending Failed."}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 403)
        resp.headers['Content-Type'] = "application/json"
        return resp


@content_shared.connect
def _content_shared(obj_id, **kwargs):

    obj = Share.query.get(obj_id)

    html, text = templating.generate_email_content("content_shared", data=locals())
    if obj.friend_id:
        recipients = [obj.friend.email]

    if obj.group_id:
        recipients = [i.email for i in obj.group.users]

    send_email(subject="Content Share", recipients=recipients, html=html, text=text, files=None)
