__author__ = 'kunsam002'

from flask_wtf import Form
from wtforms import Field, TextField, PasswordField, StringField, FieldList, FormField, \
    DateTimeField, DateField, BooleanField, DecimalField, validators, HiddenField, FloatField, \
    IntegerField, TextAreaField, SelectField, RadioField, SelectMultipleField, FileField
from wtforms.validators import DataRequired, Optional, Email, EqualTo, ValidationError
from datetime import datetime, date
from flask_wtf.html5 import EmailField
import re



def check_chars(input):
    """ Checks if there's a special character in the text """

    chars = """ '"!@#$%^&*()+=]}-_[{|\':;?/.>,<\r\n\t """
    return any((c in chars) for c in input)


def check_password(input):
    if len(input) < 6:
        return False
    if not re.search(r'\d', input):
        return False
    elif not re.search(r'[A-Z]', input):
        return False
    elif not re.search(r'[a-z]', input):
        return False
    elif not check_chars(input):
        return False
    else:
        return True


class AutoSignupForm(Form):
    username = StringField('Username or Email Address', validators=[DataRequired()],
                           description="Please enter a registered username or email")
    university_id = IntegerField('University', validators=[Optional()])

class LoginForm(AutoSignupForm):
    password = PasswordField('Password', validators=[DataRequired()], description="Please enter your valid password")


class SignupForm(Form):
    username = StringField('Username', validators=[Optional()])
    full_name = StringField('Your Full Name', validators=[Optional()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[Optional(), Email()])
    matric = StringField('Matric Number', validators=[DataRequired()])
    phone = StringField('Matric Number', validators=[Optional()])
    gender = StringField('Gender', validators=[Optional()])
    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password', validators=[DataRequired(), EqualTo('password')])
    university_id = SelectField('University', coerce=int, validators=[Optional()])

    def validate_matric(form, field):
        email = form.email.data
        phone = form.phone.data
        if email == phone == "":
            raise ValidationError("Please Specify email or Matric Number")


class ProfileUpdateForm(Form):
    # username = StringField('Username', validators=[Optional()])
    first_name = StringField('First Name', validators=[Optional()])
    last_name = StringField('Last Name', validators=[Optional()])
    phone = StringField('Matric Number', validators=[Optional()])
    gender = StringField('Gender', validators=[Optional()])
    bio = StringField('BIO.', validators=[Optional()])

class UserProfileUpdateForm(ProfileUpdateForm):
    password = PasswordField('Password', validators=[DataRequired()])


class UnverifiedUserForm(Form):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired(), Email()])
    matric = StringField('Matric Number', validators=[DataRequired()])
    phone = StringField('Matric Number', validators=[Optional()])
    university_id = SelectField('University', coerce=int, validators=[Optional()])

    def validate_matric(form, field):
        email = form.email.data
        phone = form.phone.data
        if email == phone == "":
            raise ValidationError("Please Specify email or Phone Number")


class FirebaseTokenForm(Form):
    user_id = IntegerField('User', validators=[DataRequired()])


class DeleteAccountForm(Form):
    user_id = IntegerField('User', validators=[DataRequired()])

class ForgotPasswordForm(Form):
    username = TextField('Phone or Email Address', validators = [DataRequired()])


class ResetPasswordForm(Form):
    password = PasswordField('Password', validators = [DataRequired()], description="Your password should be a minimum of 6 characters and contain at least one number, one uppercase, one lower case and one special character")
    verify_password = PasswordField('Verify Password', validators = [DataRequired(),EqualTo("password","Passwords are not equal")], description="Your password should be a minimum of 6 characters and contain at least one number, one uppercase, one lower case and one special character")

    def validate_password(form, field):
        if not check_password(field.data):
            raise ValidationError("Your password should be a minimum of 6 characters and contain at least one number, one uppercase, one lower case and one special character")

class CreateUserPasswordForm(ResetPasswordForm):
    user_id = IntegerField('User', validators=[DataRequired()])


class PrivatizeContentForm(Form):
    content_id = StringField('Content', coerce=int, validators=[DataRequired()])

class NewsletterSubscriberForm(Form):
    email = EmailField('Email Address', validators=[DataRequired(), Email()])


class BannerForm(Form):
    title = TextField('Title', validators = [Optional()])
    url = TextField('Url', validators = [DataRequired()])
    cover_image_url = TextField('Cover Image Url', validators = [DataRequired()])
    is_for_you = BooleanField('Enabled', default=False)
    is_discover = BooleanField('Enabled', default=False)


class CountryForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    code = TextField('Code', validators = [DataRequired()])

class StateForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    code = TextField('Code', validators = [DataRequired()])
    country_id = SelectField('Country', coerce=int, validators=[DataRequired()])

class CityForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    url = StringField('Url', validators=[Optional()])

class TimezoneForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    code = TextField('Code', validators = [DataRequired()])
    offset = TextField('Offset', validators = [DataRequired()])


class UniversityForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    handle = TextField('Handle', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])
    about_information = TextField('About Info.', validators = [Optional()])
    email = EmailField('Email', validators=[DataRequired(), Email()])
    email_extension = TextField('Description', validators = [DataRequired()])
    account_email = EmailField('Email', validators=[DataRequired(), Email()])
    phone = TextField('Phone', validators = [DataRequired()])
    street = TextField('Street', validators = [DataRequired()])
    is_enabled = BooleanField('Enabled', default=False)
    twitter_handle = TextField('Twitter Handle', validators = [Optional()])
    facebook_handle = TextField('Facebook Handle', validators = [Optional()])
    google_handle = TextField('Google Handle', validators = [Optional()])
    instagram_handle = TextField('Instagram Handle', validators = [Optional()])
    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password', validators=[DataRequired(), EqualTo('password')])
    # city_id = SelectField('City', coerce=int, validators=[DataRequired()])
    # state_id = SelectField('State', coerce=int, validators=[DataRequired()])
    # country_id = SelectField('Country', coerce=int, validators=[DataRequired()])
    # timezone_id = SelectField('Timezone', coerce=int, validators=[DataRequired()])


class PlatformForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    handle = TextField('Handle', validators = [DataRequired()])
    url = TextField('Url', validators = [Optional()])
    description = TextField('Description', validators = [Optional()])
    about_information = TextField('About Info.', validators = [Optional()])
    # email = EmailField('Email', validators=[DataRequired(), Email()])
    # phone = TextField('Phone', validators = [DataRequired()])
    is_enabled = BooleanField('Enabled', default=False)
    is_rss_feed = BooleanField('RSS Feed', default=False)
    rss_url = TextField('RSS Url', validators = [Optional()])
    twitter_handle = TextField('Twitter Handle', validators = [Optional()])
    facebook_handle = TextField('Facebook Handle', validators = [Optional()])
    google_handle = TextField('Google Handle', validators = [Optional()])
    instagram_handle = TextField('Instagram Handle', validators = [Optional()])


class ContentForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])
    title = TextField('Title', validators = [Optional()])
    caption = TextField('Caption', validators = [Optional()])
    sku = TextField('SKU', validators = [Optional()])
    url = TextField('Url', validators = [Optional()])
    is_featured = BooleanField('Featured', default=False)
    editors_pick = BooleanField('Editor\'s Pick', default=False)
    is_rss_feed = BooleanField('RSS Feed', default=False)
    visibility = BooleanField('Visibility', default=False)
    is_private = BooleanField('Private', default=False)
    content_type_id = SelectField('Content Type', coerce=int, validators=[DataRequired()])
    university_id = SelectField('University', coerce=int, validators=[Optional()])
    platform_id = SelectField('Platform', coerce=int, validators=[Optional()])


class ContentTypeForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])
    value = TextField('Description', validators = [DataRequired()])
    is_model = BooleanField('Is Model', default=False)


class CommentForm(Form):
    content_id = IntegerField('Content', validators=[DataRequired()])
    body = TextField('Body', validators = [DataRequired()])


class FetchCommentForm(Form):
    content_id = IntegerField('Content', validators=[DataRequired()])
    page = IntegerField('Page', validators=[Optional()], default=1)
    per_page = IntegerField('Page Size', validators=[Optional()], default=20)


class LikeForm(Form):
    content_id = IntegerField('Content', validators=[DataRequired()])
    # user_id = IntegerField('User', validators = [DataRequired()])

class LikeLibraryContentForm(Form):
    library_id = IntegerField('Library', validators=[DataRequired()])
    summary = TextField('Summary', validators = [Optional()])
    url = TextField('Url', validators = [DataRequired()])
    name = TextField('Name', validators = [Optional()])
    title = TextField('Title', validators = [Optional()])
    description = TextField('Description', validators = [Optional()])
    publication_date = TextField('Publication Date', validators = [Optional()])

class PlatformAccountForm(Form):
    platform_id = IntegerField('Platform', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired(), Email()])
    auth_token = TextField('Description', validators = [DataRequired()])


class LibraryCategoryForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])


class LibraryForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])
    value = TextField('Value', validators = [Optional()])
    library_category_id = SelectField('Library Category', coerce=int, validators=[Optional()])
    url = TextField('Url', validators = [Optional()])


class InterestForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])
    value = TextField('Value', validators = [Optional()])


class AddInterestForm(Form):
    interests = SelectMultipleField(coerce=int, validators=[DataRequired()])

class ActivityTypeForm(Form):
    name = TextField('Name', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])
    code = TextField('Code', validators = [Optional()])


class FileUploadForm(Form):
    files = FileField("files", validators=[DataRequired()])
    university_id = IntegerField('University', validators=[Optional()])


class FollowForm(Form):
    # user_id = SelectField('User', coerce=int, validators=[DataRequired()])
    person_id = SelectField('Follow', coerce=int, validators=[DataRequired()])

class SearchFollowingForm(Form):
    username = StringField('Username')


class UserCollectionForm(Form):
    """docstring for UserCollection"""
    # user_id = IntegerField('User', validators=[DataRequired()])
    name = TextField('Name', validators = [DataRequired()])
    description = TextField('Description', validators = [Optional()])
    value = TextField('Value', validators = [Optional()])
    content_id = IntegerField('Content', validators=[Optional()])
    collection_id = IntegerField('Collection', validators=[Optional()])


class CollectionItemForm(Form):
    """docstring for UserCollection"""
    content_id = IntegerField('Content', validators=[DataRequired()])
    collection_id = IntegerField('Collection', validators = [DataRequired()])
    remove_all = BooleanField('Remove From All Collections', default=False)


class UserGroupForm(Form):
    group_id = IntegerField('Group', validators=[DataRequired()])
    name = StringField('Name', validators=[DataRequired()])
    user_ids = SelectMultipleField('Users', coerce=int, validators=[DataRequired()])


class ShareForm(Form):
    content_id = IntegerField(validators=[DataRequired()])
    group_id = SelectMultipleField('Groups', coerce=int, validators=[Optional()])
    friends_id = SelectMultipleField('Friends', coerce=int, validators=[Optional()])

    def validate_group_id(self, field):
        if field.data is None and self.friends_id.data is None:
            raise ValidationError("Specify Group or Friend to Share with")

    # def validate_friend_id(self, field):
    #     if field.data is None and self.group_id.data is None:
    #         raise ValidationError("This username is already taken")


class VerifyStudentForm(Form):
    university_id = IntegerField('University', validators=[DataRequired()])


class EmailVerificationForm(Form):
    email = EmailField('Email', validators=[DataRequired(), Email()])


class AccountVerificationForm(Form):
    code = TextField('Code', validators = [DataRequired()])


class ConfirmationCodeResendForm(Form):
    agent = TextField('Agent', validators = [DataRequired()])


class DeleteForm(Form):
    id = IntegerField(validators=[DataRequired()])

class ViewUserForm(Form):
    user_id = IntegerField(validators=[Optional()])


class SearchForm(Form):
    search_q = TextField(validators=[Optional()])
    user_id = IntegerField(validators=[Optional()])
    is_library = BooleanField('Search In Library', default=False)


class ProfilePicForm(Form):
    profile_pic = FileField('Profile Picture', validators=[DataRequired()])


class BlankForm(Form): pass
