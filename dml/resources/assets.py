from dml.core.exceptions import ValidationFailed
from dml.services.accounts import UserService

__author__ = 'kunsam002'

from dml.resources import BaseResource, ModelListField, ModelField, ListField, require_login
from dml.services.assets import *
from dml.forms import *
from dml import register_api
from flask_restful import fields
from flask import request, g
from dml import logger
from dml.core import utils
import json


class BannerResource(BaseResource):
    method_decorators = []
    resource_name = 'banners'
    service_class = BannerService
    validation_form = BannerForm
    searchable_fields = ['title', 'url']
    resource_fields = {
        "title": fields.String,
        "url": fields.String,
        "cover_image_url": fields.String,
        "is_for_you": fields.Boolean,
        "is_discover": fields.Boolean,
        "has_file": fields.Boolean,
        "file_type": fields.String,
        "file_path": fields.String
    }


class CountryResource(BaseResource):
    method_decorators = []
    resource_name = 'countries'
    service_class = CountryService
    validation_form = CountryForm
    searchable_fields = ['name', 'slug', 'code']
    resource_fields = {
        "name": fields.String,
        "slug": fields.String,
        "code": fields.String,
        "enabled": fields.String,
        "states": ModelListField
    }


class StateResource(BaseResource):
    method_decorators = []
    resource_name = 'states'
    service_class = StateService
    validation_form = StateForm
    searchable_fields = ['name', 'slug', 'code']
    resource_fields = {
        "name": fields.String,
        "slug": fields.String,
        "code": fields.String,
        "country": ModelField,
        "cities": ModelListField
    }


class CityResource(BaseResource):
    method_decorators = []
    resource_name = 'cities'
    service_class = CityService
    validation_form = CityForm
    searchable_fields = ['name', 'code']
    resource_fields = {
        "name": fields.String,
        "code": fields.String,
        "country": ModelField,
        "state": ModelField
    }


class TimezoneResource(BaseResource):
    method_decorators = []
    resource_name = 'timezones'
    service_class = TimezoneService
    validation_form = TimezoneForm
    searchable_fields = ['name', 'code', 'offset']
    resource_fields = {
        "name": fields.String,
        "code": fields.String,
        "offset": fields.String
    }


class ContentTypeResource(BaseResource):

    resource_name = 'content-types'
    service_class = ContentTypeService
    validation_form = ContentTypeForm
    searchable_fields = ['name', 'slug']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "value": fields.String,
        "is_model": fields.Boolean,
        "contents": ModelListField
    }


class ContentResource(BaseResource):

    resource_name = 'contents'
    service_class = ContentService
    validation_form = ContentForm
    searchable_fields = ['name', 'subtitle', 'title']
    resource_fields = {
        "name": fields.String,
        "subtitle": fields.String,
        "short_summary": fields.String,
        "summary": fields.String,
        "image": fields.String,
        "partner_logo": fields.String,
        # "description": fields.String,
        "title": fields.String,
        "caption": fields.String,
        # "expected_duration_unit": fields.String,
        # "expected_duration": fields.String,
        # "required_knowledge": fields.String,
        # "syllabus": fields.String,
        "key": fields.String,
        "level": fields.String,
        "slug": fields.String,
        "url": fields.String,
        "available": fields.Boolean,
        # "full_course_available": fields.Boolean,
        "cover_image_id": fields.Integer,
        "is_private": fields.Boolean,
        # "content_type_id": fields.Integer,
        # "university_id": fields.Integer,
        # "platform_id": fields.Integer,
        # "content_type": ModelField,
        # "university": ModelField,
        # "platform": ModelField,
        # "likes": ModelListField,
        # "saved_contents": ModelListField,
        # "comments": ModelListField,
        "view_count": fields.Integer,
        "like_count": fields.Integer,
        "share_count": fields.Integer,
        "saved_count": fields.Integer,
        "comment_count": fields.Integer,
        "platform_name": fields.String,
        "has_file": fields.Boolean,
        "file_type": fields.String,
        "file_path": fields.String,
        "type_slug": fields.String,
        "interests_slug": ListField,
        "check_is_liked": fields.Boolean,
        "check_is_saved": fields.Boolean,
        "check_is_shared": fields.Boolean,
        "interests":ListField
    }

    def adjust_form_fields(self, form):
        form.university_id.choices = [(c.id, c.name) for c in University.query]
        form.content_type_id.choices = [(c.id, c.name) for c in ContentType.query]
        form.platform_id.choices = [(c.id, c.name) for c in Platform.query]
        return form


class ActivityTypeResource(BaseResource):

    resource_name = 'activity-types'
    service_class = ActivityTypeService
    validation_form = ActivityTypeForm
    searchable_fields = ['name', 'code']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "code": fields.String
    }


class InterestResource(BaseResource):
    method_decorators = []
    resource_name = 'interests'
    service_class = InterestService
    validation_form = InterestForm
    searchable_fields = ['name', 'slug']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "value": fields.String,
        "is_academic": fields.Boolean,
        "is_model": fields.Boolean
    }

    get_contents_service_class = ContentService
    get_contents_resource_fields = {
        "name": fields.String,
        "subtitle": fields.String,
        "summary": fields.String,
        "image": fields.String,
        "title": fields.String,
        "caption": fields.String,
        "url": fields.String,
        "cover_image_id": fields.Integer,
        "is_private": fields.Boolean,
        "view_count": fields.Integer,
        "like_count": fields.Integer,
        "share_count": fields.Integer,
        "saved_count": fields.Integer,
        "comment_count": fields.Integer,
        "platform_name": fields.String,
        "has_file": fields.Boolean,
        "file_type": fields.String,
        "file_path": fields.String,
        "type_slug": fields.String,
        "interests_slug": ListField,
        "check_is_liked": fields.Boolean,
        "check_is_saved": fields.Boolean,
        "check_is_shared": fields.Boolean,
        "interests":ListField,
        "user_collections":ListField,
        "content_to_collection_count": fields.Integer
    }

    # def get_contents_query(self, obj_id):
    #     attrs = {}
    #     user_id = self.get_user_id()
    #     attrs["user_id"] = user_id
    #     return self.service_class.get_contents(obj_id, **attrs)


class AcademicInterestResource(BaseResource):
    method_decorators = []
    resource_name = 'academic_interests'
    service_class = InterestService
    validation_form = InterestForm
    searchable_fields = ['name', 'slug']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "value": fields.String,
        "is_academic": fields.Boolean,
        "is_model": fields.Boolean
        # "contents": ModelListField
    }

    def query(self):
        return self.service_class.query.filter(Interest.is_academic==True)


class NonAcademicInterestResource(BaseResource):
    method_decorators = [require_login]
    resource_name = 'nonacademic_interests'
    service_class = InterestService
    validation_form = InterestForm
    searchable_fields = ['name', 'slug']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "value": fields.String,
        "is_academic": fields.Boolean,
        "selected": fields.Boolean,
        "is_model": fields.Boolean
        # "contents": ModelListField
    }
    nonacademic_interests_interests_resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "value": fields.String,
        "is_academic": fields.Boolean,
        "selected": fields.Boolean,
        "is_model": fields.Boolean
    }

    def query(self):
        obj_id = g.user_id
        return UserService.nonacademic_interest(obj_id)


class LibraryCategoryResource(BaseResource):

    resource_name = 'library_categories'
    service_class = LibraryCategoryService
    validation_form = LibraryCategoryForm
    searchable_fields = ['name', 'slug']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "libraries": ModelListField
    }


class LibraryResource(BaseResource):

    resource_name = 'libraries'
    service_class = LibraryService
    validation_form = LibraryForm
    searchable_fields = ['name', 'slug']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "value": fields.String,
        "url": fields.String,
        "library_category_id": fields.Integer,
        "library_category": ModelField
    }

    def adjust_form_fields(self, form):
        form.library_category_id.choices = [(c.id, c.name) for c in LibraryCategory.query]
        return form


class FirbaseCustomTokenResource(BaseResource):
    resource_name = "firebase_custom_token"
    method_decorators = []

    def prepare_errors(self, errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    def post(self):
        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        form = FirebaseTokenForm(csrf_enabled=False, **_data)
        if form.validate():
            data = form.data
            firebase_custom_token = None

            try:
                firebase_custom_token = utils.create_firebase_custom_token(data)
            except:
                pass

            return {"firebase_custom_token": firebase_custom_token,}, 200
        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))


register_api(BannerResource, '/banners', '/banners/<int:obj_id>', '/banners/<int:obj_id>/<string:resource_name>')
register_api(CountryResource, '/countries', '/countries/<int:obj_id>', '/countries/<int:obj_id>/<string:resource_name>')
register_api(StateResource, '/states', '/states/<int:obj_id>', '/states/<int:obj_id>/<string:resource_name>')
register_api(CityResource, '/cities', '/cities/<int:obj_id>', '/cities/<int:obj_id>/<string:resource_name>')
register_api(TimezoneResource, '/timezones', '/timezones/<int:obj_id>', '/timezones/<int:obj_id>/<string:resource_name>')
register_api(ContentTypeResource, '/content-types', '/content-types/<int:obj_id>', '/content-types/<int:obj_id>/<string:resource_name>')
register_api(ContentResource, '/contents', '/contents/<int:obj_id>', '/contents/<int:obj_id>/<string:resource_name>')
register_api(ActivityTypeResource, '/activity_types', '/activity_types/<int:obj_id>', '/activity_types/<int:obj_id>/<string:resource_name>')
register_api(InterestResource, '/interests', '/interests/<int:obj_id>', '/interests/<int:obj_id>/<string:resource_name>')
register_api(LibraryResource, '/libraries', '/libraries/<int:obj_id>', '/libraries/<int:obj_id>/<string:resource_name>')
register_api(LibraryCategoryResource, '/library_categories', '/library_categories/<int:obj_id>', '/library_categories/<int:obj_id>/<string:resource_name>')
register_api(FirbaseCustomTokenResource, '/firebase_custom_token/generator')
register_api(AcademicInterestResource, '/academic_interests')
register_api(NonAcademicInterestResource, '/nonacademic_interests')
