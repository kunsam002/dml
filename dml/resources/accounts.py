__author__ = 'kunsam002'

import os
from dml.core.exceptions import ValidationFailed, IntegrityException
from dml.resources import BaseResource, ModelListField, ModelField, ListField
from dml.services.accounts import *
from dml.services.authentication import *
from dml.services.operations import SavedContentService, ShareService, FollowService, UserCollectionService
from dml.core import utils
from dml.models import *
from dml.signals import *
from dml.forms import *
from dml import register_api, app, cache, archives
from flask_restful import fields
from flask import redirect, session, request, abort, g, jsonify, make_response
from dml import logger
from flask_login import login_user, logout_user, login_required, current_user
from flask_principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
from dml.services.assets import ContentService
import base64
import requests
import json
import datetime


class LoginResource(BaseResource):
    """
    Resource class to handle User login.
    It returns the username and access token of the user, which can then
    be used in subsequent API requests. Useful for logging in via a rest client

    """

    resource_name = "login"
    method_decorators = []

    def prepare_errors(self, errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    def post(self):

        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        form = LoginForm(csrf_enabled=False, **_data)
        if form.validate():
            data = form.data
            firebase_custom_token = None

            user = authenticate_user(**data)

            if user is None:
                _data_ = {"status":"failure","message":"The provided user credentials are invalid."}
                _data_ = json.dumps(_data_)
                resp = make_response(_data_, 403)
                resp.headers['Content-Type'] = "application/json"
                return resp
            else:
                if user.is_verified != True:
                    _data_ = {"status":"failure","message":"User Verification not complete."}
                    _data_ = json.dumps(_data_)
                    resp = make_response(_data_, 403)
                    resp.headers['Content-Type'] = "application/json"
                    return resp

                login_user(user, remember=True, force=True) # This is necessary to remember the user

                identity_changed.send(app, identity=Identity(user.id))

                # include the username and api_token in the session

                # Transfer auth token to the frontend for use with api requests
                __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                g.user = user
                g.user_id = user.id  # store the user in the special g object
                if user.is_staff==True and user.is_mtn==False and user.university_id!=None:
                    g.university_id = user.university_id

                if user.is_staff and user.is_mtn==True and user.university_id==None:
                    g.is_mtn = True

                try:
                    firebase_custom_token = utils.create_firebase_custom_token(user.id)
                except:
                    pass
                _user_interests=[{"id":i.id,"slug":i.slug} for i in user.interests]
                return {"user_id": user.id, "username": user.username, "first_name": user.first_name,"interests":_user_interests, "is_onboarded":user.is_onboarded, "is_verified":user.is_verified, "last_name": user.last_name, "bio": user.bio, "gender":user.gender, "firebase_custom_token": firebase_custom_token, "auth_token": user.get_auth_token(), "last_login_at":user.last_login_at, "last_login_ip":user.last_login_ip, "current_login_at":user.current_login_at, "current_login_ip":user.current_login_ip}, 200

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))



class LogoutResource(BaseResource):

    resource_name = "logout"

    def post(self):

        logout_user()

        # Remove session keys set by Flask-Principal
        for key in ('identity.name', 'identity.auth_type'):
            session.pop(key, None)

        # Tell Flask-Principal the user is anonymous
        identity_changed.send(app, identity=AnonymousIdentity())

        _data_ = {"status":"Success","message":"You Logged Out Successfully"}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 200)
        resp.headers['Content-Type'] = "application/json"
        return resp



class SignupResource(BaseResource):
    """
    Resource class to handle User login.
    It returns the username and access token of the user, which can then
    be used in subsequent API requests. Useful for logging in via a rest client

    """
    method_decorators = []
    resource_name = "signup"

    def post(self):

        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        form = SignupForm(csrf_enabled=False, **_data)
        form.gender.choices = [("", "---- Select One ----")]+[("Male", "Male")]+[("Female", "Female")]
        form.university_id.choices = [("", "--- Select One ---")]+[(i.id, i.name) for i in University.query.all()]

        if form.validate():
            data = form.data
            ee_mail = data.get("email","")

            # ee_mail = email_handle_verification(ee_mail)
            # if not ee_mail:
            #     _data_ = {"status":"failure","message":"Not a Valid University Email"}
            #     _data_ = json.dumps(_data_)
            #     resp = make_response(_data_, 403)
            #     resp.headers['Content-Type'] = "application/json"
            #     return resp

            u = User.query.filter(User.email==ee_mail).first()
            if u:
                _data_ = {"status":"failure","message":"Account Already exist with that email address"}
                _data_ = json.dumps(_data_)
                resp = make_response(_data_, 403)
                resp.headers['Content-Type'] = "application/json"
                return resp

            firebase_custom_token = None
            v_p= data.pop("verify_password", "")
            user = UserService.create(ignored_args=None, **data)
            user.generate_password(v_p)

            if user is None:
                _data_ = {"status":"failure","message":"Your signup could not be completed."}
                _data_ = json.dumps(_data_)
                resp = make_response(_data_, 403)
                resp.headers['Content-Type'] = "application/json"
                return resp
            else:
                login_user(user, remember=True, force=True) # This is necessary to remember the user

                identity_changed.send(app, identity=Identity(user.id))

                # include the username and api_token in the session

                # Transfer auth token to the frontend for use with api requests
                __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                try:
                    firebase_custom_token = utils.create_firebase_custom_token(user.id)
                except:
                    pass

                _data_={"user_id":user.id, "email":user.email, "phone":user.phone}
                ConfirmationTokenService.create(**_data_)
                student_created.send(user.id)
                return {"user_id": user.id, "username": user.username, "firebase_custom_token":firebase_custom_token, "auth_token": user.get_auth_token(), "last_login_at":user.last_login_at, "last_login_ip":user.last_login_ip, "current_login_at":user.current_login_at, "current_login_ip":user.current_login_ip}, 200

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))


class VerifyEmailResource(BaseResource):

    resource_name = "email_verification"
    method_decorators = []

    def prepare_errors(self, errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    def post(self):

        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        form = EmailVerificationForm(csrf_enabled=False, **_data)
        if form.validate():
            data = form.data

            email = email_handle_verification(data.get("email",""))

            if email:
                _data_ = {"status":"success","message":"Email Verification Successful"}
                _data_ = json.dumps(_data_)
                resp = make_response(_data_, 200)
                resp.headers['Content-Type'] = "application/json"
                return resp
            else:
                _data_ = {"status":"failure","message":"Not a Valid University Email"}
                _data_ = json.dumps(_data_)
                resp = make_response(_data_, 200)
                resp.headers['Content-Type'] = "application/json"
                return resp

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))


class AutoSignupResource(BaseResource):

    resource_name = "auto_signup"
    method_decorators = []

    def prepare_errors(self, errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    def adjust_form_fields(self, form):
        form.university_id.choices = [(c.id, c.name) for c in University.query]
        return form

    def post(self):

        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        is_email = is_matric = False

        form = AutoSignupForm(csrf_enabled=False, **_data)
        if form.validate():
            data = form.data
            username = form.username.data
            firebase_custom_token = None

            user = authenticate_auto_signup_user(**data)
            if user:
                if username == user.email:
                    is_email=True
                elif username == user.matric:
                    is_matric=True
            if is_matric:
                uni_id = data.get("university_id",None)
                if uni_id and uni_id!="" and uni_id>0:
                    if user.university_id != uni_id:
                        _data_ = {"status":"Unauthorized","message":"Specified University doesn't match User's University"}
                        _data_ = json.dumps(_data_)
                        resp = make_response(_data_, 403)
                        resp.headers['Content-Type'] = "application/json"
                        return resp
                else:
                    _data_ = {"status":"Unauthorized","message":"Specified University doesn't match User's University"}
                    _data_ = json.dumps(_data_)
                    resp = make_response(_data_, 403)
                    resp.headers['Content-Type'] = "application/json"
                    return resp

            if user is None:
                email = email_handle_verification(username)
                if email:
                    auto_signup_to_verify.send(email)
                    u = User.query.filter(User.email==email).first()
                    _data_ = {"status":"success","payload":{"user_id":u.id,"email":email,"action":"to_verify_and_provide_details"}}
                    _data_ = json.dumps(_data_)
                    resp = make_response(_data_, 200)
                    resp.headers['Content-Type'] = "application/json"
                    return resp
                else:
                    _data_ = {"status":"failure","message":"Not a Valid University Email. Please Provide an acceptable email"}
                    _data_ = json.dumps(_data_)
                    resp = make_response(_data_, 403)
                    resp.headers['Content-Type'] = "application/json"
                    return resp
            else:
                if user.is_verified != True:
                    auto_signup_to_verify.send(user.email)
                    _data_ = {"status":"success","payload":{"interests_count":len(user.interests),"is_verified":user.is_verified,"is_onboarded":user.is_onboarded,"user_id":user.id,"email":user.email,"bio":user.bio,"first_name":user.first_name,"last_name":user.last_name,"matric":user.matric,"university_id":user.university_id,"university_name":user.university.name if user.university else None,"phone":user.phone,"gender":user.gender,"action":"to_verify_and_provide_details"}}
                    _data_ = json.dumps(_data_)
                    resp = make_response(_data_, 209)
                    resp.headers['Content-Type'] = "application/json"
                    return resp
                user.update_last_login()
                login_user(user, remember=True, force=True) # This is necessary to remember the user

                identity_changed.send(app, identity=Identity(user.id))

                # include the username and api_token in the session

                # Transfer auth token to the frontend for use with api requests
                __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                g.user = user
                g.user_id = user.id  # store the user in the special g object

                # try:
                #     firebase_custom_token = utils.create_firebase_custom_token(user.id)
                # except:
                #     pass

                _data_={"user_id":user.id, "email":user.email, "phone":user.phone}

                try:
                    ConfirmationTokenService.create(**_data_)
                    student_created.send(user.id)
                except:
                    pass

                # return {"user_id": user.id, "is_onboarded":user.is_onboarded, "interests_count":len(user.interests), "email":user.email, "is_verified":user.is_verified, "phone":user.phone, "last_login_at":user.last_login_at, "last_login_ip":user.last_login_ip, "current_login_at":user.current_login_at, "current_login_ip":user.current_login_ip}, 200
                _fdata_ = {"status":"success","payload":{"interests_count":len(user.interests),"is_verified":user.is_verified,"is_onboarded":user.is_onboarded,"user_id":user.id,"email":user.email,"bio":user.bio,"first_name":user.first_name,"last_name":user.last_name,"matric":user.matric,"university_id":user.university_id,"university_name":user.university.name if user.university else None,"phone":user.phone,"gender":user.gender}}
                _fdata_ = json.dumps(_fdata_)
                resp = make_response(_fdata_, 209)
                resp.headers['Content-Type'] = "application/json"
                return resp

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))

    create_password_validation_form = ResetPasswordForm
    service_class = UserService

    def do_create_password(self, obj_id, attrs, files=None):
        return self.service_class.reset_password(obj_id, **attrs)


class PasswordResource(BaseResource):

    resource_name = "user_password"
    service_class = UserService
    method_decorators = []

    def prepare_errors(self, errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    def post(self):

        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        form = CreateUserPasswordForm(csrf_enabled=False, **_data)
        if form.validate():
            data = form.data

            user_id = form.user_id.data

            user = UserService.get(int(user_id))

            if user is None:
                _data_ = {"status":"failure","message":"The provided user credentials are invalid."}
                _data_ = json.dumps(_data_)
                resp = make_response(_data_, 403)
                resp.headers['Content-Type'] = "application/json"
                return resp
            else:

                self.service_class.reset_password(user.id, **data)

                return user

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))


class UserResource(BaseResource):

    resource_name = 'users'
    service_class = UserService
    validation_form = SignupForm
    change_password_validation_form = ResetPasswordForm
    searchable_fields = ['username', 'email', 'full_name']
    resource_fields = {
        "username": fields.String,
        "email": fields.String,
        "full_name": fields.String,
        "first_name": fields.String,
        "last_name": fields.String,
        "phone": fields.String,
        "matric": fields.String,
        # "uni_verified": fields.Boolean,
        "is_verified": fields.Boolean,
        "gender": fields.String,
        "likes": ModelListField,
        "follower_count": fields.Integer,
        "following_count": fields.Integer,
        "university_id": fields.Integer,
        "saved_contents": ModelListField,
        "accounts": ModelListField,
        "interests": ModelListField,
        "user_activities": ModelListField,
        "user_groups": ModelListField,
        "group_messages": ModelListField,
        "follows": ModelListField,
        "collections": ModelListField,
        "bio": fields.String,
        "profile_pic": fields.String
    }

    like_content_validation_form = unlike_content_validation_form = LikeForm
    reset_password_validation_form = ResetPasswordForm
    save_content_validation_form = unsave_content_validation_form = LikeForm
    follow_validation_form = unfollow_validation_form = FollowForm
    update_profile_validation_form = ProfileUpdateForm
    signup_update_profile_validation_form = UserProfileUpdateForm
    verify_student_validation_form = VerifyStudentForm
    add_interests_validation_form = AddInterestForm
    create_collection_validation_form = edit_collection_validation_form = \
        create_content_collection_validation_form = UserCollectionForm
    add_to_collection_validation_form = remove_from_collection_validation_form = remove_content_from_collection_validation_form = CollectionItemForm
    add_comment_validation_form = CommentForm
    fetch_comments_validation_form = FetchCommentForm
    delete_collection_validation_form = DeleteForm
    share_content_validation_form = ShareForm
    view_user_validation_form = ViewUserForm
    search_following_validation_form = SearchFollowingForm

    def delete(self, obj_id=None):
        _data_ = {"status":"failure","message":"Unauthorized Action"}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 401)
        resp.headers['Content-Type'] = "application/json"
        return resp

    def share_content_adjust_form_fields(self, form):
        form.group_id.choices = [(c.id, c.name) for c in Group.query.all()]
        form.friends_id.choices = [(c.id, c.full_name) for c in User.query.all()]
        return form

    get_collections_service_class = UserCollectionService
    # get_collections_resource_fields = {
    #     "name": fields.String,
    #     "description": fields.String
    # }
    get_followers_service_class = get_following_service_class = search_following_service_class = FollowService
    get_following_following_resource_fields = {
        "user_id": fields.Integer,
        "user": ModelField
    }
    search_following_resource_fields={
        "users":ListField
    }
    fetch_comments_resource_fields={
        "page":fields.Integer,
        "per_page": fields.Integer,
        "pages": fields.Integer,
        "comments":ListField
    }
    def do_fetch_comments(self, obj_id, attrs, files=None):
        return self.service_class.fetch_comments(obj_id, **attrs)

    get_followers_followers_user_resource_fields = get_share_list_users_resource_fields = search_following_users_resource_fields={
        "full_name": fields.String,
        "first_name": fields.String,
        "last_name": fields.String,
        "bio":fields.String,
        "id":fields.Integer,
        "profile_pic":fields.String
    }
    get_share_list_groups_resource_fields={
        "key":fields.String,
        "name":fields.String,
        "status":fields.String
    }
    get_followers_multi_fields = ["followers"]
    get_following_multi_fields = ["following"]
    get_followers_followers_resource_fields = {
        "user_id": fields.Integer,
        "user": ModelField,
        "following": fields.Boolean
    }
    get_following_following_person_resource_fields = get_followers_user_resource_fields = {
        "full_name": fields.String,
        "first_name": fields.String,
        "last_name": fields.String,
        "bio":fields.String,
        "id":fields.Integer,
        "profile_pic":fields.String
    }
    add_comment_resource_fields = {
        "status":fields.String,
        "content_id": fields.Integer,
        "content_comment_count": fields.Integer
    }
    follow_resource_fields = unfollow_resource_fields = {
        "status":fields.String
    }
    get_shares_service_class = ShareService
    get_shares_resource_fields = {
        "content_id": fields.Integer,
        "content": ModelField,
        "group_id": fields.Integer,
        "friend_id": fields.Integer,
        "friend": ModelField
    }
    get_saved_contents_service_class = SavedContentService
    get_liked_contents_service_class = LikeService
    get_saved_contents_resource_fields = {
        "saves":ListField,
        "saves_count":fields.Integer
    }
    get_liked_contents_resource_fields ={
        "likes":ListField,
        "likes_count":fields.Integer
    }

    get_saved_contents_saves_resource_fields = get_liked_contents_likes_resource_fields = {
        "content_id":fields.Integer,
        "content":ModelField,
        "id":fields.Integer
    }
    get_liked_contents_likes_count_resource_fields = get_saved_contents_saves_count_resource_fields ={
        "total":fields.Integer
    }
    get_liked_contents_multi_fields=["likes","likes_count"]

    def is_permitted(self, obj_id=None, **kwargs):

        if obj_id:
            if not g.user_id == obj_id:
                return False
        return True

    def adjust_form_fields(self, form):
        form.university_id.choices = [(c.id, c.name) for c in University.query]
        return form

    def add_interests_adjust_form_fields(self, form):
        form.interests.choices = [(c.id, c.name) for c in Interest.query]
        return form

    def follow_adjust_form_fields(self, form):
        form.person_id.choices = [(c.id, c.full_name) for c in User.query]
        return form

    def unfollow_adjust_form_fields(self, form):
        form.person_id.choices = [(c.id, c.full_name) for c in User.query]
        return form

    save_content_resource_fields = like_content_resource_fields = like_library_content_resource_fields = unlike_content_resource_fields = unsave_content_resource_fields = {
        "content_id": fields.Integer,
        "content": ModelField
    }

    # unlike_content_resource_fields = unsave_content_resource_fields={
    #     "content_id": fields.Integer,
    #     "status": fields.String
    # }

    delete_collection_resource_fields = {
        "status": fields.String
    }

    share_content_resource_fields = {
        "share": ListField
    }

    get_collections_collections_resource_fields = create_collection_resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "items_count": fields.Integer
    }
    edit_collection_resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "items_count": fields.Integer,
        "items": ModelListField(relations=['content'])
    }
    get_collections_collections_count_resource_fields = {
        "total":fields.Integer
    }
    get_collections_resource_fields = {"collections":ListField, "collections_count":fields.Integer}
    create_content_collection_resource_fields = remove_from_collection_resource_fields ={
        "name": fields.String,
        "description": fields.String,
        "items_count": fields.Integer,
        "items": ListField
    }

    create_content_collection_items_resource_fields = add_to_collection_items_resource_fields = remove_from_collection_items_resource_fields = {
        "content_id": fields.Integer,
        "content": ListField,
        "content_to_collection_count": fields.Integer
    }

    get_activity_feeds_service_class = get_share_list_service_class = get_interests_service_class = UserService

    get_share_list_resource_fields = {
        "users":ListField,
        "groups":ListField
    }

    get_interests_resource_fields ={
        "interests":ListField
    }
    get_interests_interests_resource_fields ={
        "name":fields.String,
        "id":fields.Integer,
        "slug":fields.String
    }

    get_activity_feeds_for_you_resource_fields = get_activity_feeds_for_general_resource_fields = {
        "actor_id":fields.Integer,
        "action_id":fields.Integer,
        "object_id":fields.Integer,
        "actor_name":fields.String,
        "group_id":fields.String,
        "group_key":fields.String,
        "activity_type":fields.Integer,
        "actor_image":fields.String,
        "object_image":fields.String,
        "group_name":fields.String
    }
    get_activity_feeds_resource_fields ={
        "for_you":ListField,
        "for_general":ListField
    }

    add_to_collection_resource_fields = remove_content_from_collection_resource_fields = {
        "content_id": fields.Integer,
        "content": ModelField,
        "content_to_collection_count": fields.Integer
    }

    add_to_collection_items_content_resource_fields = {
        "user_collections":ListField
    }

    view_user_resource_fields = {
        "username": fields.String,
        "full_name": fields.String,
        "first_name": fields.String,
        "last_name": fields.String,
        "gender": fields.String,
        "likes": ListField,
        "follower_count": fields.Integer,
        "following_count": fields.Integer,
        "university_id": fields.Integer,
        # "saved_contents": ModelListField,
        "bio": fields.String,
        "profile_pic": fields.String,
        "followings": ListField,
        "followers": ListField
    }

    update_profile_service_class = signup_update_profile_service_class = UserService

    def do_toggle_notifications(self, obj_id, attrs, files=None):
        return self.service_class.toggle_notifications(obj_id)

    def do_update_profile(self, obj_id, attrs, files=None):
        return self.service_class.update_profile(obj_id, **attrs)

    def do_signup_update_profile(self, obj_id, attrs, files=None):
        user= self.service_class.update_profile(obj_id, **attrs)
        _data_ = {"user_id":user.id,"email":user.email,"auth_token":user.get_auth_token(),"firebase_custom_token":utils.create_firebase_custom_token(user.id),"first_name":user.first_name,"last_name":user.last_name,"bio":user.bio,"is_verified":user.is_verified,"is_onboarded":user.is_onboarded}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 200)
        resp.headers['Content-Type'] = "application/json"
        return resp

    def do_like_content(self, obj_id, attrs, files=None):
        return self.service_class.like_content(obj_id, **attrs)

    like_library_content_validation_form = LikeLibraryContentForm
    def do_like_library_content(self, obj_id, attrs, files=None):
        return self.service_class.like_library_content(obj_id, **attrs)

    def do_unlike_content(self, obj_id, attrs, files=None):
        return self.service_class.unlike_content(obj_id, **attrs)

    save_profile_pic_validation_form = ProfilePicForm
    def do_save_profile_pic(self, obj_id, attrs, files=None):
        file_path=None
        if files:
            for name in files:
                filename = archives.save(files[name])
                file_path = archives.path(filename)

        logger.info(file_path)
        attrs['filepath'] = file_path
        obj = self.service_class.save_profile_pic(obj_id, **attrs)
        _data_ = {"status":"Success","message":"Your Profile Picture Addedd Successfully", 'profile_pic':obj.profile_pic}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 200)
        resp.headers['Content-Type'] = "application/json"
        return resp

    def do_view_user(self, obj_id, attrs, files=None):
        user_id = attrs.get("user_id",None)
        user = UserService.get(user_id)
        followers = [{"id":i.user_id,"first_name":i.user.first_name,"last_name":i.user.last_name,"profile_pic":i.user.profile_pic,"bio":i.user.bio} for i in Follow.query.filter(Follow.person_id==user_id).all()]
        followings = [{"id":i.person_id,"first_name":i.person.first_name,"last_name":i.person.last_name,"profile_pic":i.person.profile_pic,"bio":i.person.bio} for i in user.follows]
        likes = [{"id":i.id,"content_id":i.content_id,"content":{"name":i.name,"title":i.title,"interests":[{"id":x.id,"name":x.name,"slug":x.slug} for x in i.content.interests],"platform_name":i.platform_name}} for i in user.likes]
        data = {"followers":followers,"followings":followings, "likes":likes}
        data.update(user.as_dict(exclude=['date_created','last_updated','password','email','matric','last_login_at','last_login_ip','current_login_at','current_login_ip']))
        return data

    def get_saved_contents_query(self, obj_id):
        contents = SavedContent.query.filter(SavedContent.user_id==obj_id)
        data = []
        extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name']
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        for i in contents:
            item = {"content_id":i.content_id,"id":i.id,"content":i.content.as_dict(extras=extras,exclude=exclusions)}
            data.append(item)

        return {"saves":data, "saves_count":{"total":contents.count()}}

    def get_liked_contents_query(self, obj_id):
        likes = Like.query.filter(Like.user_id==obj_id)
        data = []
        extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name']
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        for i in likes:
            item = {"content_id":i.content_id,"id":i.id,"content":i.content.as_dict(extras=extras,exclude=exclusions)}
            data.append(item)

        return {"likes":data, "likes_count":{"total":likes.count()}}

    def get_shares_query(self, obj_id):
        return Share.query.filter(Share.user_id==obj_id).order_by(desc(Share.id))

    def get_interests_query(self, obj_id):
        user = self.service_class.get(obj_id)
        return {"interests":[{"name":i.name,"id":i.id,"slug":i.slug} for i in user.interests]}

    def get_share_list_query(self, obj_id):
        return self.service_class.get_share_list(obj_id)

    def get_activity_feeds_query(self, obj_id):
        return self.service_class.get_activity_feeds(obj_id)

    def search_following_query(self, obj_id):
        followings = Follow.query.filter(Follow.user_id==obj_id)
        results = []
        for i in followings:
            item = {"full_name":i.person.full_name,"id":i.person_id,"bio":i.person.bio,"profile_pic":i.person.profile_pic,"first_name":i.person.first_name,"last_name":i.person.last_name}
            results.append(item)
        return {"users":results}

    def do_search_following(self, obj_id, attrs, files=None):
        sub_string = attrs.get('username', " ")
        followings = Follow.query.filter(Follow.user_id==obj_id).all()
        following_ids = [c.person_id for c in followings]
        users = User.query.filter(User.id.in_(following_ids))\
            .filter(or_(User.username.contains(sub_string),
                        User.first_name.contains(sub_string),
                        User.last_name.contains(sub_string)))
        results = []
        for i in users:
            item = {"full_name":i.full_name,"id":i.id}
            results.append(item)
        return {"users":results}

    def do_save_content(self, obj_id, attrs, files=None):
        return self.service_class.save_content(obj_id, **attrs)

    def do_share_content(self, obj_id, attrs, files=None):
        return self.service_class.share_content(obj_id, **attrs)

    def do_unsave_content(self, obj_id, attrs, files=None):
        return self.service_class.unsave_content(obj_id, **attrs)

    def do_add_comment(self, obj_id, attrs, files=None):
        return self.service_class.add_comment(obj_id, **attrs)

    def do_change_password(self, obj_id, attrs, files=None):
        return self.service_class.change_password(obj_id, **attrs)

    def get_followers_query(self, obj_id):
        user = self.service_class.get(obj_id)
        query = Follow.query.filter(Follow.person_id==obj_id).order_by(desc(Follow.id))
        following_ids = [i.person_id for i in user.follows]
        data = []
        exclusions = ["last_updated","is_super_admin","is_staff","university_id","user_id","deactivate","is_mtn","current_login_ip","current_login_at","location","is_verified","email","matric","login_count","phone",
                      "last_login_ip","last_login_at","is_admin","active","uni_verified","password","is_global","is_deleted","dob","gender","date_created"]
        for i in query.all():
            item = {"user_id":i.user_id,"following": True if i.user_id in following_ids else False, "check_following": True if i.user_id in following_ids else False,"user":i.user.as_dict(exclude=exclusions), "id":i.id}
            data.append(item)

        return {"followers":data}


    def get_following_query(self, obj_id):
        user = self.service_class.get(obj_id)
        exclusions = ["last_updated","is_super_admin","is_staff","university_id","user_id","deactivate","is_mtn","current_login_ip","current_login_at","location","is_verified","email","matric","login_count","phone",
                      "last_login_ip","last_login_at","is_admin","active","uni_verified","password","is_global","is_deleted","dob","gender","date_created"]
        return {"following":[{"id":i.id,"user_id":i.person_id,"user":i.person.as_dict(exclude=exclusions)} for i in user.follows.order_by(desc(Follow.id)).all()]}

    def get_suggested_users_query(self, obj_id):
        return self.service_class.suggested_users(obj_id)

    def do_follow(self, obj_id, attrs, files=None):
        obj= self.service_class.follow(obj_id, **attrs)
        self.service_class.update(obj_id)
        # return {"status":"success","id":obj.id if obj else None}
        _data_ = {"status":"Success","message":"You Started following a Friend"}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 200)
        resp.headers['Content-Type'] = "application/json"
        return resp

    def do_unfollow(self, obj_id, attrs, files=None):
        self.service_class.unfollow(obj_id, **attrs)
        _data_ = {"status":"Success","message":"You Request to stop following a Friend was Successful"}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 200)
        resp.headers['Content-Type'] = "application/json"
        return resp

    def do_request_reset_password(self, obj_id, attrs, files=None):
        return self.service_class.request_reset_password(obj_id, **attrs)

    def do_reset_password(self, obj_id, attrs, files=None):
        return self.service_class.reset_password(obj_id, **attrs)

    def do_add_interests(self, obj_id, attrs, files=None):
        self.service_class.add_interests(obj_id, **attrs)
        user = self.service_class.update(obj_id)
        return user

    delete_account_validation_form = DeleteAccountForm
    def do_delete_account(self, obj_id, attrs, files=None):
        user = User.query.get(obj_id)
        user_id=attrs.get("user_id",None)
        if obj_id!=user_id:
            _data_ = {"status":"failure","message":"Unauthorized Action"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 401)
            resp.headers['Content-Type'] = "application/json"
            return resp
        user.interests=[]
        try:
            db.session.add(user)
            db.session.commit()
            db.session.delete(user)
            db.session.commit()
            _data_ = {"status":"success","message":"User Account Deletion Successful"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 200)
            resp.headers['Content-Type'] = "application/json"
            return resp
        except:
            db.session.rollback()
            _data_ = {"status":"failure","message":"User Account Deletion Failed"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 403)
            resp.headers['Content-Type'] = "application/json"
            return resp

    def do_create_collection(self, obj_id, attrs, files=None):
        collection = self.service_class.create_collection(obj_id, **attrs)
        return collection

    def do_edit_collection(self, obj_id, attrs, files=None):
        collection = self.service_class.edit_collection(obj_id, **attrs)
        return collection

    def do_delete_collection(self, obj_id, attrs, files=None):
        user = self.service_class.get(obj_id)
        object_id = attrs.get("id",None)
        UserCollectionService.delete(object_id)
        _data_ = {"status":"Success","message":"Collection Deleted Successfully"}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 200)
        resp.headers['Content-Type'] = "application/json"
        return resp

    def do_create_content_collection(self, obj_id, attrs, files=None):
        collection = self.service_class.create_content_collection(obj_id, **attrs)
        return collection

    @cache.cached(60)
    def get_collections_query(self, obj_id):
        collections = UserCollection.query.filter(UserCollection.user_id==obj_id).order_by(desc(UserCollection.id))
        results =[]
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        for collection in collections:
            # items =[]
            # for i in collection.items:
            #     items.append({"id":i.id,"content_id":i.content_id, "content_to_collection_count":i.content_to_collection_count, "content":i.content.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions)})

            data = {"id":collection.id,"name":collection.name,"description":collection.description,"items_count":collection.items_count}
            results.append(data)
        return {"collections":results, "collections_count":{"total":collections.count()}}

    def do_add_to_collection(self, obj_id, attrs, files=None):
        collection = UserCollection.query.get(attrs.get("collection_id", None))
        if not collection:
            _data_ = {"status":"Collection Does not Exist","message":"User Collection Record does not exist"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 404)
            resp.headers['Content-Type'] = "application/json"
            return resp
        obj = self.service_class.add_to_collection(obj_id, **attrs)
        return obj

    def do_remove_from_collection(self, obj_id, attrs, files=None):
        return self.service_class.remove_from_collection(obj_id, **attrs)

    def do_remove_content_from_collection(self, obj_id, attrs, files=None):
        user = self.service_class.get(obj_id)
        extras = ['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name']
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available",
                      "is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id","file_type", "has_file", "editors_pick", "author", "required_knowledge", "is_rss_feed", "partner_logo",
                       "file_path", "key", "slug", "content_type_id", "url", "title", "summary", "platform_id", "caption"]
        try:
            self.service_class.remove_from_collection(obj_id, **attrs)
            content = Content.query.get(attrs.get("content_id",None))
            user_collections=fetch_content_user_collections(content.id, obj_id)
            _d_ = {"user_collections":user_collections}
            _d_.update(content.as_dict(extras=extras, exclude=exclusions))
            data = {"content_id":content.id, "content_to_collection_count":content.content_to_collection_count, "content":_d_}
            return data
        except Exception as e:
            logger.info(e)
            print "---------Error: %s-----------" % str(e)
            db.session.rollback()
            _data_ = {"status":"Content Not Removed","message":"Content Removal unsuccessful...Try again"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 208)
            resp.headers['Content-Type'] = "application/json"
            return resp

    def do_verify_student(self, obj_id, attrs, files=None):
        uni_id= attrs.get("university_id")
        user = self.service_class.get(obj_id)
        if uni_id!= user.university_id:
            return "error"
        self.service_class.verify_student(obj_id, **attrs)
        user = self.service_class.update(obj_id)

        return user

    get_suggested_users_service_class = UserService
    get_suggested_users_multi_fields = ["by_interest"]
    get_suggested_users_by_interest_users_resource_fields = get_suggested_users_users_resource_fields = for_you_top_users_resource_fields = discover_discover_new_people_resource_fields = discover_top_users_resource_fields = for_you_university_users_resource_fields = discover_university_users_resource_fields = {
        "matric": fields.String,
        "id": fields.Integer,
        "full_name": fields.String,
        "first_name": fields.String,
        "last_name": fields.String,
        "bio": fields.String,
        "profile_pic": fields.String,
        "check_following": fields.Boolean,
        "check_follower": fields.Boolean
    }

    get_suggested_users_by_interest_resource_fields = {
        "interest": fields.String,
        "id": fields.Integer,
        "users": ListField
    }

    def for_you_users_query(self, obj_id):
        return  self.service_class.for_you_users(obj_id)

    # for_you_contents_resource_fields = {
    #     "name": fields.String,
    #     "subtitle": fields.String,
    #     "short_summary": fields.String,
    #     "summary": fields.String,
    #     "image": fields.String,
    #     "partner_logo": fields.String,
    #     "title": fields.String,
    #     "caption": fields.String,
    #     "key": fields.String,
    #     "level": fields.String,
    #     "slug": fields.String,
    #     "url": fields.String,
    #     "available": fields.Boolean,
    #     "cover_image_id": fields.Integer,
    #     "is_private": fields.Boolean,
    #     "view_count": fields.Integer,
    #     "like_count": fields.Integer,
    #     "share_count": fields.Integer,
    #     "saved_count": fields.Integer,
    #     "comment_count": fields.Integer,
    #     "platform_name": fields.String,
    #     "has_file": fields.Boolean,
    #     "file_type": fields.String,
    #     "file_path": fields.String,
    #     "type_slug": fields.String,
    #     "interests_slug": ListField,
    #     "check_is_liked": fields.Boolean,
    #     "check_is_saved": fields.Boolean,
    #     "check_is_shared": fields.Boolean
    #     "interests":ListField
    # }
    for_you_contents_service_class = ContentService

    def for_you_contents_query(self, obj_id):
        return self.service_class.for_you_contents(obj_id)

    for_you_contents_resource_fields = for_you_featured_contents_resource_fields = discover_contents_resource_fields = discover_popular_contents_resource_fields = discover_editors_pick_contents_resource_fields  = {
        "name": fields.String,
        "subtitle": fields.String,
        "summary": fields.String,
        "image": fields.String,
        "title": fields.String,
        "caption": fields.String,
        "url": fields.String,
        "cover_image_id": fields.Integer,
        "is_private": fields.Boolean,
        "view_count": fields.Integer,
        "like_count": fields.Integer,
        "share_count": fields.Integer,
        "saved_count": fields.Integer,
        "comment_count": fields.Integer,
        "platform_name": fields.String,
        "has_file": fields.Boolean,
        "file_type": fields.String,
        "file_path": fields.String,
        "type_slug": fields.String,
        "interests_slug": ListField,
        "check_is_liked": fields.Boolean,
        "check_is_saved": fields.Boolean,
        "check_is_shared": fields.Boolean,
        "interests":ListField,
        "user_collections":ListField,
        "content_to_collection_count": fields.Integer
    }

    for_you_contents_by_interest_resource_fields = {
        "interest": fields.String,
        "contents": ListField
    }

    for_you_service_class = UserService
    for_you_multi_fields = ["contents", "featured_contents"]
    # for_you_contents = []
    # for_you_users = []

    def for_you_query(self, obj_id):
        return self.service_class.for_you(obj_id)

    discover_resource_fields = {
        "user": ModelListField,
        "university": ModelListField
    }

    # discover_top_users_resource_fields = {
    #     "matric": fields.String,
    #     "full_name": fields.String,
    #     "first_name": fields.String,
    #     "last_name": fields.String,
    #     "bio": fields.String,
    #     "profile_pic": fields.String
    # }
    #
    # discover_university_users_resource_fields = {
    #     "matric": fields.String,
    #     "full_name": fields.String,
    #     "first_name": fields.String,
    #     "last_name": fields.String,
    #     "bio": fields.String,
    #     "profile_pic": fields.String
    # }

    # discover_editors_pick_contents_resource_fields = {
    #     "title": fields.String,
    #     "link": fields.String,
    #     "short_summary": fields.String,
    #     "image": fields.String
    # }

    # discover_contents_resource_fields = {
    #      "title": fields.String,
    #     "link": fields.String,
    #     "short_summary": fields.String,
    #     "image": fields.String
    # }
    discover_service_class = UserService
    discover_multi_fields = ["discover_new_people", "editors_pick_contents", "popular_contents"]
    # discover_universitys = []
    # discover_users = []
    def discover_query(self, obj_id):
        return self.service_class.discover(obj_id)


class UniversityResource(BaseResource):

    method_decorators = []
    resource_name = 'universities'
    service_class = UniversityService
    validation_form = UniversityForm
    searchable_fields = ['name', 'handle', 'email_extension', 'google_handle','twitter_handle','facebook_handle','instagram_handle']
    resource_fields = {
        "name": fields.String,
        "handle": fields.String,
        "email_extension": fields.String,
        "description": fields.String,
        "about_information": fields.String,
        "thumbnail": fields.String,
        "logo": fields.String,
        "email": fields.String,
        "phone": fields.String,
        "street": fields.String,
        "is_enabled": fields.Boolean,
        "twitter_handle": fields.String,
        "facebook_handle": fields.String,
        "google_handle": fields.String,
        "instagram_handle": fields.String
        # "city_id": fields.Integer,
        # "state_id": fields.Integer,
        # "country_id": fields.Integer,
        # "timezone_id": fields.Integer,
        # "city": ModelField,
        # "state": ModelField,
        # "country": ModelField,
        # "timezone": ModelField,
        # "users": ModelListField,
        # "contents": ModelListField
    }

    def is_permitted(self, obj_id=None, **kwargs):

        if obj_id:
            if not g.user_id == obj_id:
                return False
        return True

    get_students_resource_fields = {
        "matric": fields.String,
        "full_name": fields.String,
        "first_name": fields.String,
        "last_name": fields.String,
        "bio": fields.String,
        "profile_pic": fields.String
    }
    get_staffs_resource_fields = {
        "full_name": fields.String,
        "first_name": fields.String,
        "last_name": fields.String,
        "email": fields.String,
        "bio": fields.String,
        "profile_pic": fields.String
    }
    get_students_service_class = get_staffs_service_class = UserService
    def get_students_query(self, obj_id):
        return self.service_class.get_students(obj_id)

    def get_staffs_query(self, obj_id):
        return self.service_class.get_staffs(obj_id)

    reset_password_validation_form = ResetPasswordForm

    privatize_content_validation_form = PrivatizeContentForm
    def privatize_content_adjust_form_fields(self, form):
        form.content_id.choices = [(c.id, c.name) for c in Content.query]

    def do_reset_password(self, obj_id, attrs, files=None):
        return self.service_class.reset_password(obj_id, **attrs)

    def do_privatize_content(self, obj_id, attrs, files=None):
        return self.service_class.privatize_content(obj_id, **attrs)




class PlatformResource(BaseResource):

    resource_name = 'platforms'
    service_class = PlatformService
    validation_form = PlatformForm
    searchable_fields = ['name', 'handle', 'google_handle','twitter_handle','facebook_handle','instagram_handle']
    resource_fields = {
        "name": fields.String,
        "handle": fields.String,
        "url": fields.String,
        "description": fields.String,
        "about_information": fields.String,
        "thumbnail": fields.String,
        "logo": fields.String,
        "email": fields.String,
        "phone": fields.String,
        "street": fields.String,
        "is_enabled": fields.Boolean,
        "is_rss_feed": fields.Boolean,
        "rss_url": fields.String,
        "twitter_handle": fields.String,
        "facebook_handle": fields.String,
        "google_handle": fields.String,
        "instagram_handle": fields.String,
        "city_id": fields.Integer,
        "state_id": fields.Integer,
        "country_id": fields.Integer,
        "timezone_id": fields.Integer,
        "city": ModelField,
        "state": ModelField,
        "country": ModelField,
        "timezone": ModelField,
        # "contents": ModelListField,
        "interests": ModelListField,
        "accounts": ModelListField
    }


class UnverifiedUserResource(BaseResource):
    method_decorators = []
    resource_name = 'unverified_users'
    service_class = UnverifiedUserService
    validation_form = UnverifiedUserForm
    verify_user_validation_form = ViewUserForm
    resource_fields = {
        "first_name":fields.String,
        "last_name":fields.String,
        "full_name":fields.String,
        "university_id":fields.Integer,
        "matric":fields.String,
        "phone":fields.String,
        "email":fields.String,
        "is_verified": fields.Boolean
    }

    def adjust_form_fields(self, form):
        form.university_id.choices = [(c.id, c.name) for c in University.query]
        return form

    def do_verify_user(self, obj_id, attrs, files=None):
        return self.service_class.verify_user(obj_id, **attrs)


# Register this resource
register_api(LoginResource, '/login')
register_api(LogoutResource, '/logout')
register_api(SignupResource, '/signup')
register_api(VerifyEmailResource, '/email_verification')
register_api(AutoSignupResource, '/auto_signup')
register_api(PasswordResource, '/user_password')
register_api(UserResource, '/users', '/users/<int:obj_id>', '/users/<int:obj_id>/<string:resource_name>')
register_api(UniversityResource, '/universities', '/universities/<string:resource_name>', '/universities/<int:obj_id>', '/universities/<int:obj_id>/<string:resource_name>')
register_api(PlatformResource, '/platforms', '/platforms/<int:obj_id>', '/platforms/<int:obj_id>/<string:resource_name>')
register_api(UnverifiedUserResource, '/unverified_users', '/unverified_users/<int:obj_id>', '/unverified_users/<int:obj_id>/<string:resource_name>')
