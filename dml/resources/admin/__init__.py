from functools import wraps

from flask.helpers import make_response

from dml.core.exceptions import ValidationFailed, IntegrityException
from dml.resources import BaseResource, ModelListField, ModelField, Timestamp
from dml.services.accounts import *
from dml.services.authentication import *
from dml.core import utils
from dml.models import *
from dml.signals import *
from dml.forms import *
from dml import register_api, app
from flask_restful import fields
from flask import redirect, session, request, abort, g, jsonify
from dml import logger
from flask_login import login_user, logout_user, login_required, current_user
from flask_principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
from dml.services.assets import ContentService
import base64
import requests
import json
import datetime
# from elasticsearch_dsl import Q


class AdminLoginResource(BaseResource):
    """
    Resource class to handle User login.
    It returns the username and access token of the user, which can then
    be used in subsequent API requests. Useful for logging in via a rest client

    """

    resource_name = "admin-login"
    method_decorators = []

    def prepare_errors(self, errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    def post(self):
        logger.info("here")

        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        form = LoginForm(csrf_enabled=False, **_data)
        if form.validate():
            data = form.data
            firebase_custom_token = None

            user = authenticate_admin(**data)

            if user is None:
                abort(403, message="The provided user credentials are invalid.")
            else:
                login_user(user, remember=True, force=True) # This is necessary to remember the user

                identity_changed.send(app, identity=Identity(user.id))

                # include the username and api_token in the session

                # Transfer auth token to the frontend for use with api requests
                __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                g.user = user
                g.user_id = user.id  # store the user in the special g object
                if user.is_staff==True and user.is_mtn==False and user.university_id!=None:
                    g.university_id = user.university_id

                if user.is_staff and user.is_mtn==True and user.university_id==None:
                    g.is_mtn = True

                try:
                    firebase_custom_token = utils.create_firebase_custom_token(user.id)
                except:
                    pass

                return {"user_id": user.id, "username": user.username, "firebase_custom_token": firebase_custom_token, "auth_token": user.get_auth_token(), "last_login_at":user.last_login_at, "last_login_ip":user.last_login_ip, "current_login_at":user.current_login_at, "current_login_ip":user.current_login_ip}, 200

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))



class AdminLogoutResource(BaseResource):

    resource_name = "admin-logout"

    def post(self):

        logout_user()

        # Remove session keys set by Flask-Principal
        for key in ('identity.name', 'identity.auth_type'):
            session.pop(key, None)

        # Tell Flask-Principal the user is anonymous
        identity_changed.send(app, identity=AnonymousIdentity())

        return {"status":"success"}, 200



class AdminSignupResource(BaseResource):
    """
    Resource class to handle User login.
    It returns the username and access token of the user, which can then
    be used in subsequent API requests. Useful for logging in via a rest client

    """
    method_decorators = []
    resource_name = "admin-signup"

    def post(self):

        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        form = SignupForm(csrf_enabled=False, **_data)
        form.gender.choices = [("", "---- Select One ----")]+[("Male", "Male")]+[("Female", "Female")]
        if form.validate():
            data = form.data
            firebase_custom_token = None
            v_p= data.pop("verify_password", "")
            user = UserService.create(ignored_args=None, **data)
            user.generate_password(v_p)

            if user is None:
                abort(403, message="Your signup could not be completed.")
            else:
                login_user(user, remember=True, force=True) # This is necessary to remember the user

                identity_changed.send(app, identity=Identity(user.id))

                # include the username and api_token in the session

                # Transfer auth token to the frontend for use with api requests
                __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                try:
                    firebase_custom_token = utils.create_firebase_custom_token(user.id)
                except:
                    pass

                return {"user_id": user.id, "username": user.username, "firebase_custom_token":firebase_custom_token, "auth_token": user.get_auth_token(), "last_login_at":user.last_login_at, "last_login_ip":user.last_login_ip, "current_login_at":user.current_login_at, "current_login_ip":user.current_login_ip}, 200

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))

def require_admin_login(func):
    """ Authentication decorator that handles user login """

    @wraps(func)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return require_basic_auth()

        # login the user
        user = check_admin_auth(auth.username, auth.password)
        if user is None:
            _data_ = {"status":"Invalid Credentials","message":"The provided user credentials are invalid."}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 403)
            resp.headers['Content-Type'] = "application/json"
            return resp

        g.admin = user
        g.admin_id = user.id  # store the user in the special g object

        return func(*args, **kwargs)

    return decorated


class BaseAdminResource(BaseResource):
    method_decorators = [require_admin_login]

    @property
    def output_fields(self):
        return {
            'id': fields.Integer(default=None),
            'date_created': Timestamp,
            'last_updated': Timestamp,
    }

    @staticmethod
    def get_admin_id():
        _admin_id = getattr(g, "admin_id", None)

        if not _admin_id:
            abort(401, status="Invalid credentials",
                  message="the user credentials provided is invalid for this resource")

        return _admin_id

    @staticmethod
    def get_admin():

        _admin_id = getattr(g, "admin_id", None)

        if not _admin_id:
            abort(401, status="Invalid credentials",
                  message="the user credentials provided is invalid for this resource")

        return User.query.get(_admin_id)

    def adjust_form_data(self, data):
        """ inject courier_id into the data before persisting """

        _admin_id = self.get_admin_id()

        # data["courier_id"] = _courier_id
        data["admin_id"] = _admin_id

        return data

    def search_filters(self):
        """default search filter. should be typically overwritten in subclass"""
        return [Q("term", user_id=self.get_admin_id())]

    def search_filter_queries(self):
        return [Q("term", user_id=self.get_admin_id())]

    def limit_query(self, query):
        """ Limit the query by the courier_id as well as role/user management here"""

        admin_id = self.get_admin_id()
        return query.filter_by(user_id=admin_id)


register_api(AdminLoginResource, '/admin/login')
register_api(AdminLogoutResource, '/admin/logout')
register_api(AdminSignupResource, '/admin/signup')