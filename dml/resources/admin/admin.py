from dml.resources import BaseResource, ModelListField, ModelField, Timestamp, ListField
from dml.resources.admin import BaseAdminResource
from dml.services.accounts import *
from dml.services.admin import AdminService
from dml.models import *
from dml.signals import *
from dml.forms import *
from dml import register_api, app
from flask_restful import fields
from dml import logger
from dml.services.assets import ContentService


class AdminResource(BaseAdminResource):

    service_class = AdminService
    resource_name = "admins"
    validation_form = SignupForm
    change_password_validation_form = ResetPasswordForm
    searchable_fields = ['username', 'email', 'full_name', 'gender']
    resource_fields = {
        "name": fields.String,
        "is_enabled": fields.Boolean,
        "username": fields.String,
        "email": fields.String,
        "full_name": fields.String,
        "phone": fields.String,
        "is_verified": fields.Boolean,
        "gender": fields.String,
      }

    def do_change_password(self, obj_id, attrs, files=None):
        return self.service_class.change_password(obj_id, **attrs)

    def do_add_student_to_institution(self, obj_id, attrs, files=None):
        return self.service_class.add_student_to_institution(**attrs)

    get_students_resource_fields = {
        "matric": fields.String,
        "full_name": fields.String
    }
    get_students_service_class = UserService

    def get_students_query(self, obj_id, attrs):
        return self.service_class.get_students(**attrs)




class AdminContentResource(BaseAdminResource):
    service_class = ContentService
    validation_form = ContentForm
    searchable_fields = ['name', 'subtitle', 'title']
    resource_name = "admin_contents"
    resource_fields = {
        "name": fields.String,
        "subtitle": fields.String,
        "short_summary": fields.String,
        "summary": fields.String,
        "image": fields.String,
        "partner_logo": fields.String,
        # "description": fields.String,
        "title": fields.String,
        "caption": fields.String,
        # "expected_duration_unit": fields.String,
        # "expected_duration": fields.String,
        # "required_knowledge": fields.String,
        # "syllabus": fields.String,
        "key": fields.String,
        "level": fields.String,
        "slug": fields.String,
        "url": fields.String,
        "available": fields.Boolean,
        # "full_course_available": fields.Boolean,
        "cover_image_id": fields.Integer,
        "is_private": fields.Boolean,
        # "content_type_id": fields.Integer,
        # "university_id": fields.Integer,
        # "platform_id": fields.Integer,
        # "content_type": ModelField,
        # "university": ModelField,
        # "platform": ModelField,
        # "likes": ModelListField,
        # "saved_contents": ModelListField,
        # "comments": ModelListField,
        "view_count": fields.Integer,
        "like_count": fields.Integer,
        "share_count": fields.Integer,
        "saved_count": fields.Integer,
        "comment_count": fields.Integer,
        "platform_name": fields.String,
        "has_file": fields.Boolean,
        "file_type": fields.String,
        "file_path": fields.String,
        "type_slug": fields.String

    }

    def do_mark_featured_content(self, obj_id, attrs):
        return self.service_class.mark_featured_content(obj_id, **attrs)

    def do_mark_editors_pick(self, obj_id, attrs):
        return self.service_class.mark_editors_pick(obj_id, **attrs)


register_api(AdminResource, '/admins', '/admins/<int:obj_id>', '/admins/<int:obj_id>/<string:resource_name>')
