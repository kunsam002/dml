__author__ = 'kunsam002'
import os
from dml.resources import BaseResource, ModelListField, ModelField, ListField
from dml.services.operations import *
from dml.forms import *
from dml.signals import *
from dml import register_api, archives
from flask_restful import fields
from flask import render_template, make_response
from dml import logger
from dml.core.utils import excel_to_json, create_firebase_custom_token
from dml.core.content_libraries import fetch_search_content
import json
from dml.services.accounts import UniversityService, UserService, PlatformService, fetch_content_user_collections
from dml.models import ConfirmationToken, Platform


class LikeResource(BaseResource):

    resource_name = 'likes'
    service_class = LikeService
    validation_form = LikeForm
    searchable_fields = ['user_id', 'content_id']
    resource_fields = {
        "user_id": fields.Integer,
        "content_id": fields.Integer,
        "user": ModelField,
        "content": ModelField
    }


class SavedContentResource(BaseResource):

    resource_name = 'saved_contents'
    service_class = SavedContentService
    validation_form = LikeForm
    searchable_fields = ['user_id', 'content_id']
    resource_fields = {
        "user_id": fields.Integer,
        "content_id": fields.Integer,
        "user": ModelField,
        "content": ModelField
    }


class FollowResource(BaseResource):

    resource_name = 'follows'
    service_class = FollowService
    validation_form = FollowForm
    searchable_fields = ['user_id', 'person_id']
    resource_fields = {
        "user_id": fields.Integer,
        "person_id": fields.Integer,
        "user": ModelField,
        "person": ModelField
    }


class UserGroupResource(BaseResource):

    resource_name = 'user_groups'
    service_class = UserGroupService
    validation_form = UserGroupForm
    resource_fields = {
        "user_id": fields.Integer,
        "person_id": fields.Integer
    }


class ShareResource(BaseResource):

    resource_name = 'shares'
    service_class = ShareService
    validation_form = ShareForm
    searchable_fields = ['user_id', 'content_id']
    resource_fields = {
    	"user_id": fields.Integer,
        "content_id": fields.Integer,
        "group_id": fields.Integer,
        "friend_id": fields.Integer,
        "user": ModelField,
        "content": ModelField
    }


class UserCollectionResource(BaseResource):

    resource_name = 'user_collections'
    service_class = UserCollectionService
    validation_form = UserCollectionForm
    searchable_fields = ['name', 'slug']
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "slug": fields.String,
        "value": fields.String,
        "items_count": fields.Integer,
        "user_id": fields.Integer
    }

    exclusions = ["is_deleted","publication_date", "level","university","syllabus","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available",
                  "is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","university_id", "editors_pick", "author", "required_knowledge", "partner_logo",
                    "key", "slug", "content_type_id", "platform_id"]
    extras = ["url",'caption','title','is_rss_feed','like_count','summary', 'share_count', 'saved_count','subtitle', 'comment_count','description', 'interests_slug', 'platform_name', "check_is_liked","check_is_saved","check_is_shared","content_to_collection_count","file_path","file_type","has_file"]

    inject_fields = {
        "items": ModelListField(relations=['content'], exclude=exclusions, extras=extras)
    }

    user_collections_items_resource_fields = {
        "content_id": fields.Integer,
        "content": ListField,
        "content_to_collection_count": fields.Integer
    }

    remove_from_collection_validation_form = CollectionItemForm
    remove_from_collection_resource_fields = {
        "content": ModelField
    }

    def do_remove_from_collection(self, obj_id, attrs, files=None):
        user_id = self.get_user_id()
        attrs["user_id"] = user_id
        return self.service_class.remove_from_collection(obj_id, **attrs)


class CollectionItemResource(BaseResource):

    resource_name = 'collection_items'
    service_class = CollectionItemService
    validation_form = CollectionItemForm
    searchable_fields = ['collection_id', 'content_id']
    resource_fields = {
        "collection_id": fields.Integer,
        "content_id": fields.Integer,
        "collection": ModelField,
        "content": ModelField
    }


class CommentResource(BaseResource):

    resource_name = 'comments'
    service_class = CommentService
    validation_form = CommentForm
    searchable_fields = ['user_id', 'content_id']
    resource_fields = {
    	"user_id": fields.Integer,
        "body": fields.String,
        "content_id": fields.Integer,
        "user": ModelField,
        "content": ModelField
    }


class UniversitySheetResource(BaseResource):
    method_decorators = []
    resource_name = 'university_sheet'
    service_class = UniversityService
    validation_form = FileUploadForm
    searchable_fields = ['name', 'handle', 'google_handle','twitter_handle','facebook_handle','instagram_handle']
    resource_fields = {
        "name": fields.String,
        "handle": fields.String,
        "description": fields.String,
        "about_information": fields.String,
        "thumbnail": fields.String,
        "logo": fields.String,
        "email": fields.String,
        "phone": fields.String,
        "street": fields.String,
        "is_enabled": fields.Boolean,
        "twitter_handle": fields.String,
        "facebook_handle": fields.String,
        "google_handle": fields.String,
        "instagram_handle": fields.String,
        "city_id": fields.Integer,
        "state_id": fields.Integer,
        "country_id": fields.Integer,
        "timezone_id": fields.Integer,
        "city": ModelField,
        "state": ModelField,
        "country": ModelField,
        "timezone": ModelField,
        "users": ModelListField,
        "contents": ModelListField
    }

    def save(self, attrs, files=None):
        print files
        if files:
            for name in files:
                filename = archives.save(files[name])
                file_path = archives.path(filename)
                obj = excel_to_json(file_path)
                obj = json.loads(obj)
                print obj
                self.service_class.create_from_sheet(obj)
                return obj


class StudentSheetResource(BaseResource):
    method_decorators = []
    resource_name = 'student_sheet'
    service_class = UserService
    validation_form = FileUploadForm
    searchable_fields = ['username', 'first_name', 'last_name','full_name','matric','email', 'phone']
    resource_fields = {
        "username": fields.String,
        "email": fields.String,
        "full_name": fields.String,
        "phone": fields.String,
        "matric": fields.String,
        "gender": fields.String,
        "likes": ModelListField,
        "follower_count": fields.Integer,
        "following_count": fields.Integer,
        "university_id": fields.Integer,
        "saved_contents": ModelListField,
        "accounts": ModelListField,
        "interests": ModelListField,
        "user_activities": ModelListField,
        "user_groups": ModelListField,
        "group_messages": ModelListField,
        "follows": ModelListField,
        "collections": ModelListField
    }

    def save(self, attrs, files=None):
        print files
        if files:
            for name in files:
                filename = archives.save(files[name])
                logger.info(filename)
                file_path = archives.path(filename)
                obj = excel_to_json(file_path)
                obj = json.loads(obj)
                self.service_class.create_from_sheet(obj)
                return obj


class AccountVerificationResource(BaseResource):
    method_decorators = []
    resource_name = 'account_verification'
    service_class = UserService
    student_verification_validation_form = validation_form = AccountVerificationForm
    resend_code_validation_form = ConfirmationCodeResendForm

    def get(self, obj_id):
        user = self.service_class.get(int(obj_id))
        UserService.verify_user(int(obj_id))
        return make_response(render_template('resources/account_verification.html'), 200)

    def do_student_verification(self, obj_id, attrs, files=None):
        code = attrs.get("code")
        user = self.service_class.get(int(obj_id))
        if user.is_verified:
            _data_ = {"status":"failure","message":"Unauthorized Verification on an already verified account"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 403)
            resp.headers['Content-Type'] = "application/json"
            return resp

        token = ConfirmationToken.query.filter(ConfirmationToken.code==code, ConfirmationToken.user_id==obj_id).first()

        if not token:
            _data_ = {"status":"failure","message":"Token Code does not exist."}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 403)
            resp.headers['Content-Type'] = "application/json"
            return resp

        if token and token.is_expired:
            _data_ = {"status":"failure","message":"Token Code has expired. Please Request for Another Code."}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 403)
            resp.headers['Content-Type'] = "application/json"
            return resp

        if token:
            user = UserService.verify_user_token(user.id, **attrs)
            firebase_custom_token = None
            try:
                firebase_custom_token = create_firebase_custom_token(user.id)
            except:
                pass
            _data_ = {"status":"success","payload":{"interests_count":len(user.interests),"user_id":user.id, "auth_token": user.get_auth_token(), "firebase_custom_token": firebase_custom_token,"email":user.email,"is_onboarded":user.is_onboarded,"bio":user.bio,"first_name":user.first_name,"last_name":user.last_name,"matric":user.matric,"university_id":user.university_id,"university_name":user.university.name if user.university else None,"phone":user.phone,"gender":user.gender}}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 200)
            resp.headers['Content-Type'] = "application/json"
            return resp

    def do_resend_code(self, obj_id, attrs, files=None):
        user = self.service_class.get(int(obj_id))
        agent = attrs.get("agent","")
        if agent == "sms":
            pass
        else:
            student_created.send(user.id)

        return user



class SearchResource(BaseResource):
    resource_name = 'search'
    validation_form = SearchForm
    service_class = PlatformService
    resource_fields = {
        "users": ListField,
        "news": ListField,
        "libraries": ListField
    }

    search_platform_resource_fields = {
        "platform_name":fields.String,
        "search_content_count": fields.Integer,
        "contents":ListField
    }

    def save(self, attrs, files=None):

        search_q = attrs.get("search_q",None)
        user_id = attrs.get("user_id",None)

        user_query = User.query
        news_query = Content.query.filter(Content.is_rss_feed==True)
        library_query = db.session.query(Content.platform_id, func.count(Content.platform_id)).filter(Content.is_rss_feed!=True)

        if search_q:
            user_queries = [User.first_name.like("%%%s%%" % search_q), User.last_name.like("%%%s%%" % search_q), User.full_name.like("%%%s%%" % search_q)]
            user_query = user_query.filter(or_(*user_queries))

            content_queries = [Content.name.like("%%%s%%" % search_q), Content.title.like("%%%s%%" % search_q)]

            news_query = news_query.filter(or_(*content_queries))
            library_query = library_query.filter(or_(*content_queries)).group_by(Content.platform_id).all()
        else:
            library_query = library_query.group_by(Content.platform_id).all()
        users_data = [{"id":i.id,"full_name":i.full_name,"first_name":i.first_name,"last_name":i.last_name,"profile_pic":i.profile_pic,"bio":i.bio} for i in user_query]
        news_data = [
            {
                "id":i.id,"title":i.title,"content_to_collection_count":i.content_to_collection_count,"name":i.name,"view_count":i.view_count,"comment_count":i.comment_count,"share_count":i.share_count,
                "saved_count":i.saved_count,"like_count":i.like_count,"summary":i.summary,"url":i.url, "subtitle":i.subtitle,"caption":i.caption,"image":i.image,"platform_name":i.platform_name,"interests_slug":i.interests_slug,
                "has_file":i.has_file,"file_path":i.file_path,"file_type":i.file_type,"type_slug":i.type_slug,"check_is_liked":i.check_is_liked(user_id) if user_id else None,"check_is_saved":i.check_is_saved(user_id) if user_id else None,
                "check_is_shared":i.check_is_shared(user_id) if user_id else None,"user_collections":fetch_content_user_collections(i.id,user_id) if user_id else None,"interests":[{"id":x.id,"name":x.name} for x in i.interests]} for i in news_query.limit(20).all()]
        library_data = []
        for i in library_query:
            p = Platform.query.get(i[0])
            library_data.append({"platform_name":p.name,"platform_id":p.id, "library_id":None,"search_content_count":i[1]})

        for i in Library.query.all():
            item = fetch_search_content(i.value, search_q=search_q)
            item.update({"platform_name":i.name,"platform_id":None, "library_id":i.id})
            library_data.append(item)

        # library_data = [{"id":i.id,"title":i.title,"name":i.name,"summary":i.summary,"image":i.image,"platform_name":i.platform_name,"interests_slug":i.interests_slug,"has_file":i.has_file,"file_path":i.file_path,"file_type":i.file_type} for i in library_query]
        return {"users":users_data,"news":news_data,"libraries":library_data}


    search_platform_validation_form = SearchForm
    def do_search_platform(self, obj_id, attrs, files=None):
        search_q = attrs.get("search_q",None)
        is_library = attrs.get("is_library",False)
        if is_library:
            library = Library.query.get(obj_id)
            data, count = fetch_search_content(library.value, search_q=search_q, return_results=True)
            return {"id":library.id,"platform_name":library.name,"search_content_count":count,"contents":data}
        else:
            platform = Platform.query.get(obj_id)

            query = Content.query.filter(Content.platform_id==obj_id, Content.is_rss_feed!=True)

            if search_q:
                content_queries = [Content.name.like("%%%s%%" % search_q), Content.title.like("%%%s%%" % search_q)]
                query = query.filter(or_(*content_queries))

            data = [{"id":i.id,"title":i.title,"name":i.name,"summary":i.summary,"image":i.image,"interests_slug":i.interests_slug,"has_file":i.has_file,"file_path":i.file_path,"file_type":i.file_type} for i in query]
            return {"id":platform.id,"platform_name":platform.name,"search_content_count":query.count(),"contents":data}


register_api(LikeResource, '/likes', '/likes/<int:obj_id>', '/likes/<int:obj_id>/<string:resource_name>')
register_api(SavedContentResource, '/saved_contents', '/saved_contents/<int:obj_id>', '/saved_contents/<int:obj_id>/<string:resource_name>')
register_api(FollowResource, '/follows', '/follows/<int:obj_id>', '/follows/<int:obj_id>/<string:resource_name>')
register_api(ShareResource, '/shares', '/shares/<int:obj_id>', '/shares/<int:obj_id>/<string:resource_name>')
register_api(UserCollectionResource, '/user_collections', '/user_collections/<int:obj_id>', '/user_collections/<int:obj_id>/<string:resource_name>')
register_api(CollectionItemResource, '/collection_items', '/collection_items/<int:obj_id>', '/collection_items/<int:obj_id>/<string:resource_name>')
register_api(CommentResource, '/comments', '/comments/<int:obj_id>', '/comments/<int:obj_id>/<string:resource_name>')
register_api(UniversitySheetResource, '/university_sheet')
register_api(StudentSheetResource, '/student_sheet')
register_api(AccountVerificationResource, '/account_verification/<int:obj_id>', '/account_verification/<int:obj_id>/<string:resource_name>')
register_api(SearchResource, '/search', '/search/<int:obj_id>','/search/<int:obj_id>/<string:resource_name>')
register_api(UserGroupResource, '/user_group', '/user_group/<int:obj_id>','/user_group/<int:obj_id>/<string:resource_name>')
