__author__ = 'kunsam002'

"""
restful.py

@Author: Ogunmokun Olukunle

"""

from dml.models import *
from dml.signals import *
from sqlalchemy import or_, and_
from dml.services import ServiceFactory
from dml.services.accounts import fetch_content_user_collections
from datetime import datetime

BannerService = ServiceFactory.create_service(Banner, db)
CountryService = ServiceFactory.create_service(Country, db)
StateService = ServiceFactory.create_service(State, db)
CityService = ServiceFactory.create_service(City, db)
TimezoneService = ServiceFactory.create_service(Timezone, db)
ContentTypeService = ServiceFactory.create_service(ContentType, db)
BaseContentService = ServiceFactory.create_service(Content, db)
LibraryService = ServiceFactory.create_service(Library, db)
LibraryCategoryService = ServiceFactory.create_service(LibraryCategory, db)
InterestService = ServiceFactory.create_service(Interest, db)
ActivityTypeService = ServiceFactory.create_service(ActivityType, db)


def add_content_interests(obj_id, ids, **kwargs):
    obj = Content.query.get(obj_id)
    interests = Interest.query.filter(Interest.id.in_(ids)).all()
    if interests:
        obj.interests = interests
    else:
        obj.interests=[]

    db.session.add(obj)
    try:
        db.session.commit()
        return obj
    except:
        db.session.rollback()
        raise


def update_content_view_count(obj_id):
    obj = Content.query.get(obj_id)
    obj.view_count += 1
    obj.last_viewed = datetime.now()
    db.session.add(obj)
    try:
        db.session.commit()
        return obj
    except:
        db.session.rollback()
        raise

class ContentService(BaseContentService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):

        obj_ = BaseContentService.create(ignored_args=ignored_args, **kwargs)
        data={}
        if obj_.publication_date:
            data["expiry_date"] = obj_.publication_date+timedelta(days=14)
        else:
            data["publication_date"] = datetime.today()
            data["expiry_date"] = datetime.today()+timedelta(days=14)

        obj = BaseContentService.update(obj_.id, **data)

        return obj


    @classmethod
    def get(cls, obj_id):
        """ Simple query method to get an object by obj_id """
        obj = BaseContentService.get(obj_id)
        obj = update_content_view_count(obj.id)
        return obj

    @classmethod
    def add_interests(cls, obj_id):
        obj = cls.get(obj_id)
        platform = Platform.query.get(obj.platform_id)
        ids = [i.id for i in platform.interests]

        add_content_interests(obj.id, ids)

        return obj

    @classmethod
    def add_content(cls, **kwargs):
        content = BaseContentService.create(**kwargs)
        return content


    @classmethod
    def mark_featured_content(cls, obj_id, **kwargs):
        kwargs['is_featured'] = True
        content = BaseContentService.update(obj_id, **kwargs)
        return content


    @classmethod
    def mark_editors_pick(cls, obj_id, **kwargs):
        kwargs['editors_pick'] = True
        content = BaseContentService.update(obj_id, **kwargs)
        return content

    @classmethod
    def user_collections(cls, obj_id, user_id, **kwargs):
        obj = cls.get(obj_id)
        ids = [i.collection_id for i in obj.collection_items]
        collections = UserCollection.query.filter(UserCollection.id.in_(ids), UserCollection.user_id==user_id).all()
        return collections


# class InterestService(BaseInterestService):

    # @classmethod
    # def get_contents(cls, obj_id, **kwargs):
    #     obj = cls.get(obj_id)
    #     logger.info(obj)
    #     user_id = kwargs.get("user_id", None)
    #     logger.info(user_id)
    #     exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
    #     for i in obj.contents:
    #         logger.info(i)
    #         cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
    #         user_collections=fetch_content_user_collections(i.id, user_id)
    #         d_ = {"check_is_liked":i.check_is_liked(user_id),"check_is_saved":i.check_is_saved(user_id),"check_is_shared":i.check_is_shared(user_id),"interests":cont_interests,"user_collections":user_collections,"content_to_collection_count":i.content_to_collection_count}
    #         d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions))
    #         data.append(d_)
    #     return data
