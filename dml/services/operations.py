__author__ = 'kunsam002'

"""
restful.py

@Author: Ogunmokun Olukunle

"""

from dml.models import *
from dml.signals import *
from dml.core.utils import clean_kwargs, populate_obj, fetch_user_mentions
from sqlalchemy import or_, and_
from dml.services import ServiceFactory

LikeService = ServiceFactory.create_service(Like, db)
SavedContentService = ServiceFactory.create_service(SavedContent, db)
BaseFollowService = ServiceFactory.create_service(Follow, db)
# FollowingService = ServiceFactory.create_service(Following, db)
ShareService = ServiceFactory.create_service(Share, db)
GroupService = ServiceFactory.create_service(Group, db)
BaseUserActivityService = ServiceFactory.create_service(UserActivity, db)
BaseUserCollectionService = ServiceFactory.create_service(UserCollection, db)
CollectionItemService = ServiceFactory.create_service(CollectionItem, db)
BaseCommentService = ServiceFactory.create_service(Comment, db)
PasswordResetTokenService = ServiceFactory.create_service(PasswordResetToken, db)

def update_content(obj_id, **kwargs):
    content = Content.query.get(obj_id)
    data = clean_kwargs(ignored_args, kwargs)
    content = populate_obj(obj, data)
    db.session.add(content)
    try:
        db.session.commit()
    except:
        pass

    return content

class UserCollectionService(BaseUserCollectionService):

    @classmethod
    def remove_from_collection(cls, obj_id, **kwargs):
        collection = cls.get(obj_id)
        user_id = kwargs.get("user_id",None)
        obj = User.query.get(user_id)
        remove_all = kwargs.get("remove_all",None)
        content_id=kwargs.get("content_id",None)
        if obj.id != collection.user_id:
            raise Exception("User not authorized for action on this collection")

        if remove_all:
            col_ids = [i.id for i in obj.collections]
            all_items = CollectionItem.query.filter(CollectionItem.content_id==content_id, CollectionItem.collection_id.in_(col_ids)).all()
            for one_i in all_items:
                db.session.delete(one_i)
                db.session.commit()
        else:
            item = CollectionItem.query.filter(CollectionItem.content_id==kwargs.get("content_id"),
                                               CollectionItem.collection_id==collection.id).first()
            if item:
                CollectionItemService.delete(item.id)

        content = update_content(content_id)
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        data = {"content":content.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions)}
        return data


class CommentService(BaseCommentService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        body=kwargs.get("body","")
        obj = BaseCommentService.create(ignored_args=ignored_args, **kwargs)
        ids = fetch_user_mentions(body)
        activity_type = ActivityType.query.filter(ActivityType.code=="mention").first()
        for i in ids:
            data={"user_id":i,"subject":"Mention","content":"You were mentioned in a comment","activity_type_id":activity_type.id,"actor_id":obj.user_id,"actor_type":"user","object_id":obj.content_id,"object_type":"content","content_id":obj.content_id,"action_id":obj.id}
            UserActivityService.create(**data)
        return obj



class FollowService(BaseCommentService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        obj = BaseFollowService.create(ignored_args=ignored_args, **kwargs)

        activity_type = ActivityType.query.filter(ActivityType.code=="follow").first()
        data=[
            # {"user_id":obj.user_id,"subject":"Following","content":"You Started following %s"%obj.person.first_name,"activity_type_id":activity_type.id,"object_id":obj.person_id,"object_type":"user"},
            {"user_id":obj.person_id,"subject":"Following","content":"%s Started following you"%obj.user.first_name,"activity_type_id":activity_type.id,"actor_id":obj.user_id,"actor_type":"user","action_id":obj.id}
        ]
        for i in data:
            UserActivityService.create(**i)

        return obj

    @classmethod
    def unfollow(cls, obj_id, **kwargs):
        obj = BaseFollowService.get(obj_id)
        activity_type = ActivityType.query.filter(ActivityType.code=="follow").first()
        data=[
            # {"user_id":obj.user_id,"subject":"Un-Follow","content":"You Stopped following %s"%obj.person.first_name,"activity_type_id":activity_type.id,"object_id":obj.person_id,"object_type":"user"},
            {"user_id":obj.person_id,"subject":"Un-Follow","content":"%s Stopped following you"%obj.user.first_name,"activity_type_id":activity_type.id,"actor_id":obj.user_id,"actor_type":"user"}
        ]
        for i in data:
            UserActivityService.create(**i)

        BaseFollowService.delete(obj_id)

        return {"status":"success"}


class UserGroupService(GroupService):

    @classmethod
    def create(cls, **kwargs):
        activity_type = ActivityType.query.filter(ActivityType.code == "added-to-group").first()
        user_ids = kwargs.get('user_ids')
        group_id = kwargs.get('group_id')
        for i in user_ids:
            UserActivityService.create(user_id=i, subject="Added To Group",
                                       activity_type_id=activity_type.id, content='You have been added to a group') # kunle should try in inject group name

        return {'status':'success'}

def check_useractivity(**kwargs):
    actor_id = kwargs.get("actor_id")
    activity_type_id = kwargs.get("activity_type_id")
    action_id = kwargs.get("action_id")
    user_id = kwargs.get("user_id")
    obj = UserActivity.query.filter(UserActivity.user_id==user_id,UserActivity.actor_id==actor_id,UserActivity.activity_type_id==activity_type_id,UserActivity.action_id==action_id).first()
    if obj and obj.activity_type.code!="mention":
        db.session.delete(obj)
        db.session.commit()

    return True

class UserActivityService(BaseUserActivityService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        check_useractivity(**kwargs)
        obj = BaseUserActivityService.create(ignored_args=ignored_args, **kwargs)
        activity_created.send(obj.id)
        return obj
