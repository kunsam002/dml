from dml.models import *
from dml.services.accounts import BaseUserService
from dml.signals import *
from sqlalchemy import or_, and_, func
from dml.services import ServiceFactory
from dml.services.assets import InterestService, ContentService
from dml.services import authentication
from dml import logger


BaseAdminService = ServiceFactory.create_service(User, db)


class AdminService(BaseAdminService):
    """ service to control all administrative activities
    """

    @classmethod
    def create(cls, **kwargs):
        p = kwargs.pop("password")
        kwargs['is_mtn'] = kwargs['is_super_admin'] =\
            kwargs['active'] = kwargs['is_verified'] = kwargs['is_staff'] = True

        obj = BaseUserService.create(**kwargs)
        obj.generate_password(p)
        return obj

    @classmethod
    def change_password(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        p = kwargs.pop("password")

        obj.set_password(p)
        return obj

    @classmethod
    def add_student_to_institution(cls, **kwargs):
        student = BaseUserService.get(kwargs.get('student_id', None))

        student = BaseUserService.update(student.id, university_id=kwargs.get('university_id'))
        return student

    @classmethod
    def get_students(cls, **kwargs):
        university_id = kwargs.get("university_id", None)
        students = User.query.filter(User.matric != None, User.university_id == university_id)

        return students


