import json
import os
import pprint

from flask.helpers import make_response

__author__ = 'kunsam002'

"""
restful.py

@Author: Ogunmokun Olukunle

"""

from dml.models import *
from dml.signals import *
from dml.core.utils import clean_kwargs, populate_obj, id_generator, fetch_user_mentions
from sqlalchemy import or_, and_, func
from dml.services import ServiceFactory
from dml.services.operations import LikeService, SavedContentService, PasswordResetTokenService, UserCollectionService, \
    CollectionItemService, CommentService, FollowService, ShareService, UserActivityService
# from dml.services.assets import ContentService
from dml.services import authentication
from dml import logger, bcrypt, cache, cloudinary_upload
from flask import g
import random
from dml.core import firebase_connection

BaseUserService = ServiceFactory.create_service(User, db)
BaseUniversityService = ServiceFactory.create_service(University, db)
PlatformService = ServiceFactory.create_service(Platform, db)
PlatformAccountService = ServiceFactory.create_service(PlatformAccount, db)
BaseUnverifiedUserService = ServiceFactory.create_service(UnverifiedUser, db)


def update_content(obj_id, **kwargs):
    content = Content.query.get(obj_id)
    ignored_args = ['id', 'date_created', 'last_updated']
    data = clean_kwargs(ignored_args, kwargs)
    content = populate_obj(content, data)
    db.session.add(content)
    try:
        db.session.commit()
    except:
        pass

    return content

def token_expired(obj_id):
    token = ConfirmationToken.query.get(obj_id)
    token.is_expired=True
    db.session.add(token)
    try:
        db.session.commit()
    except:
        db.session.rollback()


def add_user_interests(obj_id, ids, **kwargs):
    obj = User.query.get(obj_id)
    interests = Interest.query.filter(Interest.id.in_(ids)).all()
    if interests:
        obj.interests = interests
    else:
        obj.interests=[]
    if not obj.is_onboarded:
        obj.is_onboarded=True

    db.session.add(obj)
    try:
        db.session.commit()
        return obj
    except:
        db.session.rollback()
        raise

def fetch_content_user_collections(content_id, user_id):
    i=Content.query.get(content_id)
    ids = [_obj.collection_id for _obj in i.collection_items]
    collections = UserCollection.query.filter(UserCollection.id.in_(ids), UserCollection.user_id==user_id).all()
    return [{"name":_i.name, "id":_i.id} for _i in collections]

class UserService(BaseUserService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):

        p = kwargs.pop("password", id_generator())
        kwargs["full_name"] = "%s %s" %(kwargs.get("first_name",""), kwargs.get("last_name",""))

        obj = BaseUserService.create(ignored_args=ignored_args, **kwargs)
        obj.generate_password(p)
        return obj


    @classmethod
    def create_from_sheet(cls, kwargs):

        for data in kwargs:
            try:
                uni_id = data.pop("university_id", None)
                uni_id = int(uni_id)
                data['university_id'] = uni_id
                data["is_verified"] = data["uni_verified"] = True
                data["password"] = data["verify_password"] = id_generator()
                cls.create(**data)
            except:
                pass

        return "Success"


    @classmethod
    def update_profile(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        if kwargs.get("first_name",None) in [None,""]:
            kwargs["first_name"]=obj.first_name
        if kwargs.get("last_name",None) in [None,""]:
            kwargs["last_name"]=obj.last_name
        if kwargs.get("bio",None) in [None,""]:
            kwargs["bio"]=obj.bio
        if kwargs.get("username",None) in [None,""]:
            kwargs["username"]=obj.username
        if kwargs.get("phone",None) in [None,""]:
            kwargs["phone"]=obj.phone
        kwargs["full_name"] = "%s %s" %(kwargs.get("first_name"), kwargs.get("last_name"))
        p = kwargs.pop("password",None)
        obj = BaseUserService.update(obj_id, **kwargs)
        if p:
            obj.generate_password(p)
        return obj


    @classmethod
    def change_password(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        p = kwargs.pop("password")

        obj.set_password(p)
        return obj


    @classmethod
    def fetch_liked_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        content_id = kwargs.get("content_id")

        like = Like.query.filter(Like.content_id==content_id, Like.user_id==obj.id).first()

        if not like:
            like = LikeService.create(ignored_args=None, user_id=obj.id, content_id=content_id)

        return like


    @classmethod
    def fetch_unliked_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        content_id = kwargs.get("content_id")

        like = Like.query.filter(Like.content_id==content_id, Like.user_id==obj.id).first()

        # if like:
        #     # LikeService.delete(like.id)
        #
        # else:
        #     pass
        db.session.delete(like)
        try:
            db.session.commit()
        except:
            pass

        return obj


    @classmethod
    def fetch_saved_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        content_id = kwargs.get("content_id")

        s_content = SavedContent.query.filter(SavedContent.content_id==content_id, SavedContent.user_id==obj.id).first()

        if not s_content:
            s_content = SavedContentService.create(ignored_args=None, user_id=obj.id, content_id=content_id)

        return s_content


    @classmethod
    def fetch_unsaved_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        content_id = kwargs.get("content_id")

        s_content = SavedContent.query.filter(SavedContent.content_id==content_id, SavedContent.user_id==obj.id).first()

        # if s_content:
        #     # SavedContentService.delete(s_content.id)
        #
        # else:
        #     pass
        db.session.delete(s_content)
        try:
            db.session.commit()
        except:
            pass

        return obj

    @classmethod
    def share_content(cls, obj_id, **kwargs):
        content_id = kwargs.get('content_id')
        group_ids = kwargs.pop('group_id', [])
        friend_ids = kwargs.pop('friends_id', [])

        share = []
        for friend_id in friend_ids:
            try:
                obj = ShareService.create(friend_id=friend_id, user_id=obj_id, **kwargs)
                activity_type = ActivityType.query.filter(ActivityType.code=="share").first()
                data=[
                    # {"user_id":obj.user_id,"subject":"Share","content":"You Shared Content with %s"%obj.friend.first_name,"activity_type_id":activity_type.id,"object_id":obj.friend_id,"object_type":"user"},
                    {"user_id":obj.friend_id,"subject":"Share","content":"%s Shared Content with you"%obj.user.first_name,"activity_type_id":activity_type.id,"actor_id":obj.user_id,"actor_type":"user","content_id":obj.content_id,"object_id":obj.content_id,"object_type":"content","action_id":obj.id}
                ]
                for i in data:
                    UserActivityService.create(**i)
                share.append(obj.as_dict())
                content_shared.send(obj.id)
            except Exception as e:
                db.session.rollback()
                logger.info("----Error while sharing content with friends %s----"%e)
                pass

        for group_id in group_ids:
            obj = ShareService.create(group_id=group_id, user_id=obj_id, **kwargs)
            share.append(obj.as_dict())
            content_shared.send(obj.id)

        return {"share": share}


    @classmethod
    def get_share_list(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        results = group_results = []
        for i in obj.follows.all():
            item = {"full_name":i.person.full_name,"id":i.person_id,"bio":i.person.bio,"profile_pic":i.person.profile_pic,"first_name":i.person.first_name,"last_name":i.person.last_name}
            results.append(item)

        fb_user = firebase_connection.fetch_node_object(code="users",key=obj.id)
        fb_user_groups = fb_user.get("groups",[])
        if fb_user_groups:
            for i in fb_user_groups:
                gp = firebase_connection.fetch_node_object(code="groups",key=i)
                item = {"key":i,"name":gp.get("name",""), "status":gp.get("groupStatus",None)}
                group_results.append(item)
        return {"users":results, "groups":group_results}


    @classmethod
    def save_profile_pic(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        profile_pic = kwargs.get("filepath","")
        upload_result = cloudinary_upload(profile_pic, public_id="dml_%s"% obj.first_name)
        os.remove(profile_pic)
        kwargs["profile_pic"] = upload_result["url"]

        obj = cls.update(obj_id, **kwargs)
        return obj

    @classmethod
    def nonacademic_interest(cls, obj_id):
        interests = Interest.query.filter(Interest.is_academic!=True).all()
        print interests, '====general interests====='
        user = cls.get(obj_id)
        user_interestz = [i.id for i in user.interests]
        print user_interestz, '=====user_interstz====='

        interestz = []
        for i in interests:
            interest = i.as_dict()
            if i.id in user_interestz:
                interest.update(selected=True)
            else: interest.update(selected=False)

            interestz.append(interest)

        pprint.pprint(interestz[0])
        print interestz, '======int========='

        return {"interests":interestz}


    @classmethod
    def get_activity_feeds(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        results = gen_results = []
        for i in obj.user_activities.order_by(desc(UserActivity.id)):
            results.append({"id":i.id,"actor_id":i.actor_id,"action_id":i.action_id,"object_id":i.object_id,"actor_name":i.actor_name,"group_id": i.group_id,"group_key":i.group_key,"activity_type":i.activity_type_id,"actor_image":i.actor_image,"object_image":i.object_image,"group_name":i.group_name})
        ids_=[i.person_id for i in obj.follows]
        _ids=[i.user_id for i in Follow.query.filter(Follow.person_id==obj_id).all()]
        _ids_ = _ids+ids_
        at_ids = [i.id for i in ActivityType.query.filter(ActivityType.code.in_(["follow","mention"])).all()]
        query = UserActivity.query.filter(UserActivity.user_id.in_(_ids_), UserActivity.activity_type_id.in_(at_ids)).order_by(desc(UserActivity.id)).limit(20).all()
        for i in query:
            gen_results.append({"id":i.id,"actor_id":i.actor_id,"action_id":i.action_id,"object_id":i.object_id,"actor_name":i.actor_name,"group_id": i.group_id,"group_key":i.group_key,"activity_type":i.activity_type_id,"actor_image":i.actor_image,"object_image":i.object_image,"group_name":i.group_name})
        return {"for_you":results, "for_general":gen_results}


    @classmethod
    def like_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        like = cls.fetch_liked_content(obj_id, **kwargs)

        obj = cls.update(obj.id)

        extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name']
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available",
                      "is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id","file_type", "has_file", "editors_pick", "author", "required_knowledge", "is_rss_feed", "partner_logo",
                       "file_path", "key", "slug", "content_type_id", "url", "title", "summary", "platform_id", "caption"]

        return {"id":like.id,"content_id":like.content_id,"content":like.content.as_dict(extras=extras,exclude=exclusions)}


    @classmethod
    def like_library_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        content_type = ContentType.query.filter(ContentType.slug=="library").first()
        if not content_type:
            data = {"name":"Library", "slug":"library"}
            content_type = ContentType(**data)
            db.session.add(content_type)
            db.session.commit()
        kwargs["content_type_id"]=content_type.id
        cont = Content.query.filter(Content.url==kwargs.get("url"), Content.library_id==kwargs.get("library_id"),Content.content_type_id==content_type.id).first()
        if not cont:
            cont = Content(**kwargs)
            db.session.add(cont)
            try:
                db.session.commit()
                data={"content_id":cont.id}
                resp = cls.like_content(obj_id, **data)
                return resp
            except:
                _data_ = {"status":"Library Content Like Failed","message":"You have already added this content to this collection"}
                _data_ = json.dumps(_data_)
                resp = make_response(_data_, 208)
                resp.headers['Content-Type'] = "application/json"
                return resp
        else:
            data={"content_id":cont.id}
            resp = cls.like_content(obj_id, **data)
            return resp


    @classmethod
    def unlike_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        cls.fetch_unliked_content(obj_id, **kwargs)
        content_id = kwargs.get("content_id")
        content = Content.query.get(content_id)

        extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name']
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available",
                      "is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id","file_type", "has_file", "editors_pick", "author", "required_knowledge", "is_rss_feed", "partner_logo",
                       "file_path", "key", "slug", "content_type_id", "url", "title", "summary", "platform_id", "caption"]

        obj = cls.update(obj.id)

        return {"content_id":content_id, "status":"success","content":content.as_dict(extras=extras,exclude=exclusions)}


    @classmethod
    def save_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        like = cls.fetch_saved_content(obj_id, **kwargs)

        obj = cls.update(obj.id)

        extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name']
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available",
                      "is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id","file_type", "has_file", "editors_pick", "author", "required_knowledge", "is_rss_feed", "partner_logo",
                       "file_path", "key", "slug", "content_type_id", "url", "title", "summary", "platform_id", "caption"]

        return {"id":like.id,"content_id":like.content_id,"content":like.content.as_dict(extras=extras,exclude=exclusions)}


    @classmethod
    def unsave_content(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        cls.fetch_unsaved_content(obj_id, **kwargs)
        content_id = kwargs.get("content_id")
        obj = cls.update(obj.id)

        return {"content_id":content_id, "status":"success"}

    @classmethod
    def fetch_user_platform_account(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        platform_id = kwargs.get("platform_id")

        acc = PlatformAccount.query.filter(PlatformAccount.platform_id==platform_id, PlatformAccount.user_id==obj.id).first()

        if not acc:
            acc = PlatformAccountService.create(ignored_args=None, **kwargs)

        return acc


    @classmethod
    def add_account(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        like = cls.fetch_user_platform_account(obj_id, **kwargs)

        obj = cls.update(obj.id)

        return obj


    @classmethod
    def add_comment(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        kwargs["user_id"] =  obj.id
        comment = CommentService.create(**kwargs)

        return {"id":comment.id,"status":"success","content_id":comment.content_id,"content_comment_count":comment.content_comment_count}


    @classmethod
    def fetch_comments(cls, obj_id, **kwargs):
        try:
            page=kwargs.get("page",1)
            per_page=kwargs.get("per_page",20)
            content_id=kwargs.get("content_id")
        except:
            _data_ = {"status":"failure","message":"Invalid Parameters"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 403)
            resp.headers['Content-Type'] = "application/json"
            return resp

        query = Comment.query.filter(Comment.content_id==content_id).order_by(desc(Comment.id))
        results = query.paginate(page, per_page, False)

        data = {"per_page":per_page,"id":content_id,"pages":results.pages,"comments":[]}
        if results.has_next:
            data["page"] = results.next_num
        if results.has_prev:
            data["page"] = results.prev_num

        for i in results.items:
            item={"user_id":i.user_id,"profile_pic":i.user.profile_pic,"user":i.user.full_name, "id":i.id,"body":i.body, "timestamp": int(i.date_created.strftime('%s')) * 1000, "date_created":i.date_created.strftime('%b %d, %Y %H:%M %p')}
            data["comments"].append(item)

        return data


    @classmethod
    def follow(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        kwargs["user_id"] =  obj.id
        f= Follow.query.filter(Follow.user_id==obj.id, Follow.person_id==kwargs.get("person_id",None)).first()
        if not f:
            follower = FollowService.create(**kwargs)
            obj = cls.update(obj.id)
            return follower

        pass



    @classmethod
    def unfollow(cls, obj_id, **kwargs):
        person_id = kwargs["person_id"]
        obj = Follow.query.filter(Follow.user_id==obj_id, Follow.person_id==person_id).first()
        if obj:
            FollowService.unfollow(obj.id)
        cls.update(obj_id)
        return {"status":"success"}

    @classmethod
    def request_reset_password(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        data ={"user_id": obj.id,"email":obj.email}

        token = PasswordResetTokenService.create(**data)
        return obj

    @classmethod
    def reset_password(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        p=kwargs.get("password", "")
        data={"password":bcrypt.generate_password_hash(p)}
        obj = cls.update(obj.id, **data)
        return obj

    @classmethod
    def add_interests(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        interests = kwargs.get('interests',[])
        obj = add_user_interests(obj.id, interests)
        return obj

    @classmethod
    def get_students(cls, obj_id):

        students = User.query.filter(User.matric is not None, User.university_id == obj_id, User.first_name is not None,
                                     User.last_name is not None)

        return students


    @classmethod
    def get_staffs(cls, obj_id):

        staffs = User.query.filter(User.matric == None, User.university_id == obj_id,
                                   User.is_staff==True, User.first_name is not None, User.last_name is not None)

        return staffs


    @classmethod
    def create_collection(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        kwargs["user_id"] = obj.id

        collection = UserCollectionService.create(**kwargs)
        return collection

    @classmethod
    def edit_collection(cls, obj_id, **kwargs):
        collection_id = kwargs.get('collection_id')
        collection = UserCollectionService.update(collection_id, **kwargs)
        return collection

    @classmethod
    def create_content_collection(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        kwargs["user_id"] = obj.id

        collection = UserCollectionService.create(**kwargs)
        kwargs["collection_id"] = collection.id
        kwargs["is_new_collection"]=True
        return cls.add_to_collection(obj_id, **kwargs)

    @classmethod
    def add_to_collection(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        kwargs["user_id"] = obj.id
        is_new_collection = kwargs.get("is_new_collection",None)
        collection_id = kwargs.get("collection_id",None)
        extras = ['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name']
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available",
                      "is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id","file_type", "has_file", "editors_pick", "author", "required_knowledge", "is_rss_feed", "partner_logo",
                       "file_path", "key", "slug", "content_type_id", "url", "title", "summary", "platform_id", "caption"]
        item = CollectionItem.query.filter_by(content_id=kwargs.get('content_id')).first()
        if item:
            _data_ = {"status":"Content Exists","message":"You have already added this content to this collection"}
            _data_ = json.dumps(_data_)
            resp = make_response(_data_, 208)
            resp.headers['Content-Type'] = "application/json"
            return resp

        item = CollectionItemService.create(**kwargs)
        if is_new_collection:
            collection =UserCollectionService.update(collection_id)
            items =[]
            for i in collection.items:
                user_collections=fetch_content_user_collections(i.content_id, obj_id)
                _d_ = {"user_collections":user_collections}
                _d_.update(i.content.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions))
                items.append({"id":i.id,"content_id":i.content_id, "content_to_collection_count":i.content_to_collection_count, "content":_d_})

            data = {"name":collection.name,"description":collection.description,"items_count":collection.items_count,"items":items, "id":collection.id}
            return data
        else:
            user_collections=fetch_content_user_collections(item.content_id, obj_id)
            _d_ = {"user_collections":user_collections}
            _d_.update(item.content.as_dict(extras=extras, exclude=exclusions))
            data = {"id":item.id,"content_id":item.content_id, "content_to_collection_count":item.content_to_collection_count, "content":_d_}
            return data

    @classmethod
    def remove_from_collection(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)
        collection_id = kwargs.get("collection_id",None)
        remove_all = kwargs.get("remove_all",None)
        content_id=kwargs.get("content_id",None)
        collection = UserCollection.query.get(kwargs.get("collection_id",None))
        if obj.id != collection.user_id:
            raise Exception("User not authorized for action on this collection")

        if remove_all:
            col_ids = [i.id for i in obj.collections]
            all_items = CollectionItem.query.filter(CollectionItem.content_id==content_id, CollectionItem.collection_id.in_(col_ids)).all()
            for one_i in all_items:
                db.session.delete(one_i)
                db.session.commit()
        else:
            item = CollectionItem.query.filter(CollectionItem.content_id==kwargs.get("content_id"),
                                               CollectionItem.collection_id==collection.id).first()
            if item:
                db.session.delete(item)
                db.session.commit()

        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        collection = UserCollectionService.update(collection_id)
        items =[]
        for i in collection.items:
            user_collections=fetch_content_user_collections(i.content_id, obj_id)
            _d_ = {"user_collections":user_collections}
            _d_.update(i.content.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions))
            items.append({"id":i.id,"content_id":i.content_id, "content_to_collection_count":i.content_to_collection_count, "content":_d_})

        data = {"name":collection.name,"description":collection.description,"items_count":collection.items_count,"items":items}
        return data

    @classmethod
    def toggle_notifications(cls, obj_id):
        obj = cls.get(obj_id)
        n = obj.allow_notifications
        if n is None:
            obj.allow_notifications = True
            m = "On"
        if n is True:
            obj.allow_notifications = False
            m = "Off"
        else:
            obj.allow_notifications = True
            m = "On"

        _data_ = {"status": "Success", "message": "your notification preference has been set to %s." % m}
        _data_ = json.dumps(_data_)
        resp = make_response(_data_, 200)
        resp.headers['Content-Type'] = "application/json"
        return resp


    @classmethod
    def for_you_banners(cls, obj_id):
        obj = cls.get(obj_id)
        featured_contents = Content.query.filter(Content.is_featured==True, Content.image != None).all()
        data = []
        for i in featured_contents:
            cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
            user_collections=fetch_content_user_collections(i.id, obj_id)
            d_ = {"content_to_collection_count":i.content_to_collection_count,"check_is_liked":obj.check_is_liked(i.id),
                  "check_is_saved":obj.check_is_saved(i.id),"check_is_shared":obj.check_is_shared(i.id),
                  "interests":cont_interests,"user_collections":user_collections}
            d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug']))
            data.append(d_)
        return data

    @classmethod
    def discover_banners(cls, obj_id):
        obj = cls.get(obj_id)
        editors_pick_contents = Content.query.filter(Content.editors_pick == True, Content.image != None).all()
        data = []
        random.shuffle(editors_pick_contents)
        for i in editors_pick_contents[0:7]:
            cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
            user_collections=fetch_content_user_collections(i.id, obj_id)
            d_ = {"content_to_collection_count":i.content_to_collection_count,"check_is_liked":obj.check_is_liked(i.id),
                  "check_is_saved":obj.check_is_saved(i.id),"check_is_shared":obj.check_is_shared(i.id),
                  "interests":cont_interests,"user_collections":user_collections}
            d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count']))
            data.append(d_)
        return data

    @classmethod
    # @cache.cached(120)
    def for_you_users(cls, obj_id):
        obj = cls.get(obj_id)
        suggested = []
        max_days = datetime.utcnow()-timedelta(days=3)
        top_users = User.query.filter(User.last_login_at>=max_days, User.first_name != None,
                                      User.last_name != None, User.id!=obj.id).order_by(func.rand()).limit(10).all() # add ffs

        # for student in top_users:
        #     d = set(student.interests).intersection(obj.interests)
        #     count = len(d)
        #     if count >= 3:
        #         suggested.append(student.id)
        #
        # suggested = tuple(suggested)
        # suggested = User.query.filter(User.id.in_(suggested)).all()

        top_users_results = []
        for i in top_users:
            d_ = {"check_following":obj.check_following(i.id), "check_follower":obj.check_follower(i.id)}
            d_.update(i.as_dict(exclude=['last_updated','last_name','password','is_super_admin']))
            top_users_results.append(d_)

        # university_users = User.query.filter(User.university_id==obj.university_id, User.first_name != None,
        #                                      User.last_name != None, User.id!=obj.id).order_by(func.rand()).limit(10).all()
        university_users_results = []
        # for i in university_users:
        #     d_ = {"check_following":obj.check_following(i.id), "check_follower":obj.check_follower(i.id)}
        #     d_.update(i.as_dict(exclude=['last_updated','last_name','password','is_super_admin']))
        #     university_users_results.append(d_)

        results = {"top_users":top_users_results, "university_users":university_users_results}
        return results


    @classmethod
    def for_you_top_users(cls, obj_id):
        obj = cls.get(obj_id)
        suggested = []
        max_days = datetime.utcnow()-timedelta(days=3)
        top_users = User.query.filter(User.matric!=None,
                                      User.last_login_at>=max_days, User.first_name != None, User.last_name != None).order_by(desc(User.last_login_at)).limit(100).all() # add ffs

        # for student in top_users:
        #     d = set(student.interests).intersection(obj.interests)
        #     count = len(d)
        #     if count >= 3:
        #         suggested.append(student.id)
        #
        # suggested = tuple(suggested)
        # suggested = User.query.filter(User.id.in_(suggested)).all()

        top_users_results = []
        for i in top_users:
            d_ = {"check_following":obj.check_following(i.id), "check_follower":obj.check_follower(i.id)}
            d_.update(i.as_dict(exclude=['last_updated','last_name','password','is_super_admin']))
            top_users_results.append(d_)

        return top_users_results

    @classmethod
    def for_you_university_users(cls, obj_id):
        obj = cls.get(obj_id)
        university_users = User.query.filter(User.university_id==obj.university_id, User.first_name != None, User.last_name != None).order_by(func.rand()).limit(10).all()
        university_users_results = []
        for i in university_users:
            d_ = {"check_following":obj.check_following(i.id), "check_follower":obj.check_follower(i.id)}
            d_.update(i.as_dict(exclude=['last_updated','last_name','password','is_super_admin']))
            university_users_results.append(d_)

        return university_users_results

    @classmethod
    def for_you_contents(cls, obj_id):
        obj = cls.get(obj_id)
        platform_ids = interests = suggested = []
        content_type = ContentType.query.filter(ContentType.slug=="rss-feed").first()

        for interest in obj.interests:
            interests.append(interest.id)
            # platform_ids = platform_ids+[i.id for i in interest.platforms]

        top_content = Content.query.filter(Content.content_type_id==content_type.id, Content.is_private==False,
                                            Content.image is not None, Content.image!="").order_by(desc(Content.view_count)).order_by(func.rand()).limit(400).all()
        for i in top_content:
            print i.id, '=====top_content'
        for content in top_content:
            c_int_ids=[i.id for i in content.interests]
            print c_int_ids, '====c_int_ids======'
            for i in c_int_ids:
                if i in interests:
                    suggested.append(content.id)

        print suggested, '=====sug tuple===='
        suggested = tuple(suggested)
        suggested = Content.query.filter(Content.id.in_(suggested), Content.image is not None, Content.image!="").order_by(func.rand()).limit(60).all()
        data = []

        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        for i in suggested:
            cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
            user_collections=fetch_content_user_collections(i.id, obj_id)
            d_ = {"check_is_liked":obj.check_is_liked(i.id),"check_is_saved":obj.check_is_saved(i.id),"check_is_shared":obj.check_is_shared(i.id),"interests":cont_interests,"user_collections":user_collections,"content_to_collection_count":i.content_to_collection_count}
            d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions))
            data.append(d_)
        return data

        # exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        #
        # data = []
        # for interest in obj.interests:
        #     x = interest.contents
        #     raw_contents = random.sample(x,len(x))[0:10]
        #     items=[]
        #     for i in raw_contents:
        #         cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
        #         d_ = {"check_is_liked":obj.check_is_liked(i.id),"check_is_saved":obj.check_is_saved(i.id),"check_is_shared":obj.check_is_shared(i.id),"interests":cont_interests}
        #         d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions))
        #         items.append(d_)
        #
        #     data.append({"interest":interest.name,"contents":items, "id":interest.id})


        # return data

    @classmethod
    def popular_contents(cls, obj_id):
        obj = cls.get(obj_id)
        follows = obj.follows
        content_ids = []

        for i in follows:
            person = cls.get(i.person_id)
            for like in person.likes:
                content_ids.append(like.content_id)
            for save in person.saved_contents:
                content_ids.append(save.content_id)
        contents = Content.query.filter(Content.id.in_(content_ids)).all()
        random.shuffle(contents)
        items=[]
        exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        for i in contents[0:7]:
            cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
            d_ = {"check_is_liked":obj.check_is_liked(i.id),"check_is_saved":obj.check_is_saved(i.id),"check_is_shared":obj.check_is_shared(i.id),"interests":cont_interests}
            d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions))
            items.append(d_)

        # exclusions = ["interests_slug","is_deleted","publication_date", "level","university","syllabus","subtitle","expected_learning","expected_duration","full_course_available","is_mtn","wiki_history","wiki_diff","expected_duration_unit","available","is_private","cover_image_id", "last_viewed", "is_featured", "last_updated", "short_summary","date_created","description","university_id"]
        #
        # items=[]
        # for interest in obj.interests:
        #     x = interest.contents
        #     raw_contents = random.sample(x,len(x))[0:7]
        #
        #     for i in raw_contents:
        #         cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
        #         d_ = {"check_is_liked":obj.check_is_liked(i.id),"check_is_saved":obj.check_is_saved(i.id),"check_is_shared":obj.check_is_shared(i.id),"interests":cont_interests}
        #         d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count', 'interests_slug', 'platform_name'], exclude=exclusions))
        #         items.append(d_)
        # random.shuffle(items)
        # return items[0:7]
        return items


    # TODO: aggregated for you content
    @classmethod
    # @cache.cached(60)
    def for_you(cls, obj_id):
        # users = cls.for_you_users(obj_id)
        contents = cls.for_you_contents(obj_id)
        banners = cls.for_you_banners(obj_id)
        yours = {"contents":contents, "featured_contents":banners}
        return yours

    @classmethod
    def discover_users(cls, obj_id):
        obj = cls.get(obj_id)
        max_days = datetime.utcnow() - timedelta(days=3)
        top_users = User.query.filter(User.matric != None, User.first_name != None, User.last_name != None).order_by(func.rand()).limit(10).all()  # add ffs
        return top_users

    @classmethod
    @cache.cached(60)
    def suggested_users(cls, obj_id):
        obj = cls.get(obj_id)
        max_days = datetime.utcnow() - timedelta(days=3)
        # top_users = User.query.filter(User.matric != None).order_by(func.random()).limit(10).all()  # add ffs
        # suggested = []
        # for student in top_users:
        #     d = set(student.interests).intersection(obj.interests)
        #     count = len(d)
        #     if count >= 3:
        #         suggested.append(student.id)
        #
        # suggested = tuple(suggested)
        # suggested = User.query.filter(User.id.in_(suggested)).all()
        #
        # results = []
        # for i in suggested:
        #     d_ = {"check_following":obj.check_following(i.id), "check_follower":obj.check_follower(i.id)}
        #     d_.update(i.as_dict(exclude=['last_updated','last_name','password','is_super_admin']))
        #     results.append(d_)
        exclusions = ["current_login_ip","password","is_staff","deactivate","dob","is_deleted","phone","active","is_verified","last_login_at","is_global","date_created", "university","is_mtn","uni_verified", "location","is_admin","user_id","email","login_count","current_login_at","username","university_id" ,"last_login_ip"]
        results = []
        for interest in obj.interests:
            users_list = []
            users_ = interest.users
            random.shuffle(users_)
            for i in users_:
                if i.is_verified and i.full_name not in (None, ""):
                    d_ = {"check_following":obj.check_following(i.id), "check_follower":obj.check_follower(i.id)}
                    d_.update(i.as_dict(exclude=exclusions))
                    users_list.append(d_)

            item = {"interest":interest.name,"id":interest.id,"users":users_list}
            results.append(item)

        return {"by_interest":results}

    @classmethod
    @cache.cached(60)
    def discover_contents(cls, obj_id):
        obj = cls.get(obj_id)
        suggested = []
        top_content = Content.query.filter(Content.like_count >= 0, Content.is_private == False, Content.image != None
                                           ).order_by(func.rand()).offset(10).limit(20).all()

        data = []
        for i in top_content:
            cont_interests = [{"slug":_i.slug, "id":_i.id} for _i in i.interests]
            user_collections=fetch_content_user_collections(i.id, obj_id)
            d_ = {"content_to_collection_count":i.content_to_collection_count,"check_is_liked":obj.check_is_liked(i.id),"check_is_saved":obj.check_is_saved(i.id),"check_is_shared":obj.check_is_shared(i.id),"interests":cont_interests,"user_collections":user_collections}
            d_.update(i.as_dict(extras=['like_count', 'share_count', 'saved_count', 'comment_count']))
            data.append(d_)

        return data

    @classmethod
    @cache.cached(30)
    def discover(cls, obj_id):
        # contents = cls.discover_contents(obj_id)
        # users = cls.discover_users(obj_id)
        users = cls.for_you_users(obj_id)
        popular_contents = cls.popular_contents(obj_id)
        # top_users = cls.for_you_top_users(obj_id)
        # university_users = cls.for_you_university_users(obj_id)
        banners = cls.discover_banners(obj_id)
        # return {"contents": contents, "top_users":users.get("top_users",[]),
        #          "university_users":users.get("university_users",[]), "editors_pick_contents":banners, "popular_contents":popular_contents}
        return {"discover_new_people":users.get("top_users",[]), "editors_pick_contents":banners, "popular_contents":popular_contents}

    @classmethod
    def verify_student(cls, obj_id):
        obj = cls.get(obj_id)
        data = {"uni_verified":True}
        cls.update(obj.id, **data)
        return obj

    @classmethod
    def verify_user(cls, obj_id):
        obj = cls.get(obj_id)
        data = {"is_verified":True}
        cls.update(obj.id, **data)
        return obj

    @classmethod
    def verify_user_token(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        code = kwargs.get("code","")
        token = ConfirmationToken.query.filter(ConfirmationToken.code==code, ConfirmationToken.user_id==obj_id).first()

        data = {"is_verified":True}
        cls.update(obj.id, **data)
        token_expired(token.id)
        return obj

    @classmethod
    def share_count(cls, obj_id, **kwargs):

        obj = cls.get(obj_id)

        friend_id = kwargs.get("friend_id",None)
        group_id = kwargs.get("group_id",None)
        content_id = kwargs.get("content_id",None)

        return obj


class UniversityService(BaseUniversityService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):

        obj = BaseUniversityService.create(ignored_args=ignored_args, **kwargs)

        email = kwargs.pop("account_email","")

        obj.add_user(username=kwargs.get("handle",""), email=email,password=kwargs.get("password",""), full_name=kwargs.get("name",""), is_admin=True)

        return obj


    @classmethod
    def create_from_sheet(cls, obj):

        for data in obj:
            try:
                cls.create(**data)
            except:
                pass

        return "Success"


    @classmethod
    def reset_password(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)

        data ={"user_id": obj.owner_id,"email":obj.account_email}

        token = PasswordResetTokenService.create(**data)
        return obj

    # @classmethod
    # def get_students(cls, obj_id):
    #
    #     students = User.query.filter(User.matric!=None, User.university_id==obj_id)
    #
    #     return students

    @classmethod
    def privatize_content(cls, obj_id, **kwargs):
        content_id = kwargs.get('content_id', None)
        obj = Content.query.filter(Content.id==content_id, Content.university_id==obj_id, Content.image != None).first()

        if not obj:
            raise("content does not exist or you dont have the permission to alter it")

        _dat_ = {"is_private":True}
        obj = update_content(obj.id, **_dat_)

        return obj



class UnverifiedUserService(BaseUnverifiedUserService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):

        kwargs["full_name"] = "%s %s" %(kwargs.get("first_name",""), kwargs.get("last_name",""))

        obj = BaseUnverifiedUserService.create(ignored_args=ignored_args, **kwargs)
        return obj

    @classmethod
    def verify_user(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        data = {"is_verified":True}
        new_obj = cls.update(obj.id, **data)
        _d_ = {"uni_verified":True}
        _d_.update(obj.as_dict())
        user = UserService.create(**_d_)
        return new_obj
